# Schelling in München (1811-1841). Forschungsdaten

Dieses Repositorium enthält die Forschungsdaten für des DFG-Projekts 
[Schelling in München (1811-1841)](https://schelling-projekt.badw.de).
Insbesondere finden sich hier alle auf https://schelling.badw.de/ 
publizierten Transkriptionen in Form strukturierter Daten (XHTML)
in gebündelter Form.

Die Daten wurden entsprechend dem 
[Heureka-HTML-Ansatz](https://dienst.badw.de/varia/heureka/HeurekaHTML-Poster.pdf)
 direkt in XHTML kodiert. Eine detaillierte Beschreibung ihre Kodierung und Struktur
findet sich in den Auszeichnungsregeln auf: https://schelling.badw.de/auszeichnungsregeln,
die - für Interessierte - auch eine Konkordanz zu TEI-XML enthalten.

Alle Textdaten leigen unterhalb des Verzeichnisses "repros" in jeweils eigenen
Unterverzeichnissen deren Name der ID der Daten in der [Schelling-Datenbank](https://schelling.badw.de/)
entspricht. Um bestimmte Daten ausfindig zu machen kann am Besten die Suchfunktion der Datenbank
verwendet werden. Die Datenbank enthält auch sämtliche Metadaten zu den Texten. Um die
Metdaten ggf. automatisiert verarbeiten zu können, wurde dem Repositorium die gesamte
Datenbank als sql-Dump beigegeben. 

Das Forschungsprojekt, in dessen Rahmen diese Daten erstellt wurden, ist
ein gemeinsames Projekt der [Bayerischen Akademie der Wissenschaften](https://badw.de)
und der [Albert-Ludwigs-Universität Freiburg](https://uni-freiburg.de/). Es wird seit 2022 als 
Langfristvorhaben von der [Deutschen Forschungsgemeinschaft (DFG)](https://www.dfg.de/de) gefördert.

## Lizenz und Zitation

Siehe <LICENSE.md>.


## Schelling in Munich (1811-1841). Research data

This repository contains the research data for the DFG project 
[Schelling in Munich (1811-1841)](https://schelling-projekt.badw.de).
In particular, all transcriptions published on https://schelling.badw.de/ 
in the form of structured data (XHTML) in bundled form.
in bundled form.

The data has been organised according to the 
[Eureka HTML approach](https://dienst.badw.de/varia/heureka/HeurekaHTML-Poster.pdf)
 coded directly in XHTML. A detailed description of its coding and structure
can be found in the markup rules at: https://schelling.badw.de/auszeichnungsregeln,
which - for those interested - also contains a concordance to TEI-XML.

All text data are located below the "repros" directory in their own subdirectories
subdirectories whose name corresponds to the ID of the data in the [Schelling database](https://schelling.badw.de/)
corresponds. The best way to find specific data is to use the search function of the database
can be used. The database also contains all the metadata for the texts. To process the
metadata can be processed automatically, the entire database was added to the repository as an sql dump.
database was added to the repository as an sql dump. 

The research project in the context of which this data was created is
a joint project of the [Bavarian Academy of Sciences and Humanities](https://badw.de)
and the [Albert-Ludwigs-Universität Freiburg](https://uni-freiburg.de/). It has been funded since 2022 as a 
long-term project funded by the [German Research Foundation (DFG)](https://www.dfg.de/de).

## Licence and citation

See <LICENSE.md>.

Translated with DeepL.com (free version)
