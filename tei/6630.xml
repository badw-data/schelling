<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 6630</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/6630">https://schelling.badw.de/text/6630</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/6630">https://schelling.badw.de/doc/6630</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p><placename ref="255">Hanau</placename> am <date when="1816-04-03">3. April 1816</date>.</p>
            </div>
            <div>
                <p>»<bibl n="S. 377" sameas="16982" type="literature">Der Schein muß dem Menschen oft das Seyn zeigen, der Traum den Tag</bibl>«, sagt 
                    <persname ref="2096">Jean Paul</persname>. Ich wünsche nicht, daß diese Regel 
                    auf die Zukunft meiner Verhältnisse in <placename ref="21">M˖<expan>ünchen</expan></placename>, wenigstens was die 
                    Wohnung betrifft, angewendet werden möge. Die lezte Nacht führte mir 
                    nämlich in einem Traumbilde meine häusliche Einrichtung vor, als sei die 
                    schon gegenwärtig. Ich befand mich übel und ohne Behagen, es gebrach an 
                    Raume und zumal an Höhe. – Da erwachte ich. Sehe mich 
                    getröstet durch die Versicherung meiner <persname ref="4582">Frau</persname>, 
                    daß man bösen Träumen eine gute Deutung zu unterlegen pflege, faßte Muth 
                    durch die Aussicht auf die Ausnahmen, die keiner Regel abgehen und eilte 
                    zum Schreibtische um Ihnen, mein Verehrtester, eine alte Schuld abzutragen. 
                    Mich trifft indessen für diesmal der Vorwurf des Zögerns nicht allein. 
                    Ihrer würdigen <persname ref="223">Gattin</persname> waren längst schon einige 
                    Zeilen von meiner Frau zugedacht, allein das Fleisch zeigte Schwäche, so 
                    gut auch der Wille sich anließ. Heute soll nun bestimmt geschrieben werden 
                    und so hoffe ich, daß zu meinem Brieflein sich eine Beilage gesellen werde. 
                    – Jezt vor Allem die Beantwortung Ihrer freundlichen <bibl sameas="750" type="document">
                    Worte</bibl> vom <date when="1816-03-15">15. v˖<expan>origen</expan> M<expan>onats</expan></date>.</p>
                <p>Die Nachricht wegen <placename ref="96">Jena</placename> war mir leider schon früher 
                    als Sie die Güte hatten mir solche zu melden, durch <placename ref="27">
                    Berliner</placename> Briefe zugekommen. Denken Sie sich die Innigkeit meiner 
                    Freude, als ich Ihre beruhigenden Worte las. Treu und ehrlich gestehe ich 
                    Ihnen, daß für mich, hätten <hi rend="u">Sie</hi> <placename ref="21">München</placename> verlassen, 
                    der höchste Reiz geschwunden wäre. – Da wir doch einmal von Anträgen und 
                    Aufforderungen reden, so darf ich Ihnen vertrauen, daß auch mir, 
                    neuerdings wieder etwas der Art von Berlin für die <placename ref="1783">
                    Rhein-Universität</placename> zukam. <pb source="1v"></pb>Mein Kabinet dessen 
                    besondere Vorzüge zum Unterrichte sich freilich nicht wohl erkennen 
                    lassen, scheint die Aufmerksamkeit des <placename ref="1076">Pr˖<expan>eußischen</expan></placename> Gouvernements in hohem Grade 
                    angezogen zu haben. Indessen ist mein Entschluß fest und unabänderlich und 
                    spätestens zu <date when="1816-05-31">Ende des nächsten Monates</date> 
                    bin ich bei Ihnen.</p>
                <p>Was sagen Sie zu dem Rufe, den <persname ref="1099">Görres</persname> nach <placename ref="96">Jena</placename> 
                    erhalten haben soll? Ob er angenommen wurde, weiß ich nicht. – <persname ref="3397">Schulze</persname> hat eine Anstellung in <placename ref="303">
                    Koblenz</placename> (als Konsistorial- und Schulrath mit 2000 fl. Gehalt) 
                    bekommen und geht in 3–4 Wochen gleichfalls von <placename ref="255">hier</placename> ab. Er empfiehlt sich 
                    dringend Ihrem wohlwollenden Andenken und wird Hanau nicht verlassen, ohne 
                    Ihnen einen schriftlichen Beweis seiner hohen Achtung zu übermachen.</p>
                <p>Die nachsichtsvolle Aufnahme, welche Sie fortdauernd meinen Tabellen 
                    vergönnen, verdanke ich herzlich. <unclear cert="high">Verkennen</unclear> Sie nicht, daß 
                    der Mit-Verfasser (ich meine mich) seine wissenschaftliche Ausbildung in 
                    der Krise eines drangvollen Geschäftslebens zu gewinnen bemühet war und 
                    daß niemand mehr und lebendiger als er einsieht, wie sehr sein Wissen 
                    Stückwerk ist. – Es versteht sich, daß Sie demnächst über mich und meine 
                    Sammlungen ganz zu gebieten haben. – Der <bibl sameas="16981" type="literature">Ee Bogen</bibl> 
                    ist noch nicht gedruckt. Besondere Umstände haben dieses früher nicht 
                    zugelassen, ich werde ihn indessen <hi rend="u">nächstens</hi> nachsenden. Anbei die 
                    weiteren Bogen bis BBb.</p>
                <p>Dringend bitte ich, wenn es geschehen kann ohne daß es Sie zu sehr 
                    belästigt, mir die bemerkten Unrichtigkeiten in dem Abschnitte über 
                    Galvanismus nicht vorzuenthalten. Mein Mit-Herausgeber, unser Chemiker <persname ref="4955">Gärtner</persname>, freut sich sehr durch Ihre Güte zu einer 
                    Berichtigung <pb source="2r"></pb>veranlaßt zu sehen.</p>
                <p>Die Kupfer – 10 grosse Tafeln meist illuminirt, bringe ich Ihnen demnächst 
                    mit.</p>
                <p>Welche Gesezze bestehen in <placename ref="889">T˖<expan>eutschland</expan></placename> hinsichtlich der 
                    GeistesErzeugnisse innerländischer Gelehrten? – An wen hat man Exemplare 
                    zu senden? – Ich dachte S˖<expan>einer</expan> M˖<expan>ajestät</expan> dem <persname ref="443">
                    Könige</persname>, S˖<expan>einer</expan> E˖<expan>xzellenz</expan> dem Herrn <persname ref="896">
                    Minister</persname> und Herrn  G˖<expan>eheim</expan>R˖<expan>ath</expan> <persname ref="71">von Ringel
                    </persname>, so wie der <placename ref="1059">Akademie</placename> Exemplare zu 
                    überreichen. Ist es hinreichend? – – Sollte es dienlich seyn auch 
                    S˖<expan>einer</expan> K˖<expan>öniglichen</expan> H˖<expan>oheit</expan> dem <persname ref="444">Kronprinzen
                    </persname>, der, wie man sagt, nächstens nach <placename ref="160">Aschaffenburg
                    </placename> kommt, ein Ex˖<expan>emplar</expan> zu überreichen?</p>
                <p>Was die Wohnung angeht, so hatten wir <hi rend="uu">nur Sie</hi>, mein Theuerster, 
                    mit einer Bitte belästigt. – Ihren Rath wegen Miethung eines 
                    Absteigequartiers mit Möbeln halte ich – troz des Widerwillens gegen alles 
                    Provisorische den mir die neueste Politik beibrachte – für sehr trefflich. 
                    <persname ref="4582">Louise</persname> hat in der Beilage das Weitere 
                    entwickelt. Rechnen Sie beide auf unsern tief gefühlten Dank.</p>
                <p>Ich werde den Hinweg über <placename ref="7">Stuttgardt</placename> nehmen, um in 
                    <placename ref="14">Heidelberg</placename> einen Tag mit <persname ref="599">Daub
                    </persname>, <persname ref="20">Creuzer</persname>, <persname ref="1165">Fries</persname> 
                    u.s.w. zu verleben und in <placename ref="16">Tübingen</placename> die Bekanntschaft 
                    <persname ref="248">Kielmeyers</persname> zu machen.</p>
                <p>Meine <bibl sameas="16983" type="literature">Rede über Bedeutung und Stand der Mineralogie</bibl> ist zur Hälfte 
                    skizzirt, die oryktognostische Beziehungen bleiben mir noch zu 
                    <unclear cert="low">###</unclear> übrig, indessen denke ich damit in einigen Wochen zu Ende 
                    zu seyn.</p>
                <p>Wie steht es mit der Wiederbesezzung der Stelle für die Chemie? – – Ich 
                    hörte daß <persname ref="4265"><hi rend="u">Vogel</hi></persname> in <placename ref="25">
                    Paris</placename> grosse Hoffnungen habe. Es wundert mich, daß man <persname ref="3645"><unclear cert="low">###</unclear></persname> in <placename ref="27">Berlin</placename> gar nicht beachtete, ich 
                    versichere Sie es ist ein Mann von gründlichen und umfassenden Wissen <pb source="2v"></pb>und hat dabei das treueste Gemüth.</p>
            </div>
            <div> 
	            <p>Ich schliesse unter den aufrichtigen Zusicherungen 
                    innigster Verehrung und unter den freundlichsten Empfehlungen an Ihre 
                    würdige Frau <persname ref="223">Gemahlin</persname><lb></lb>
                    Ihr<lb></lb>
                    ganz ergebener</p>
	            <p>Leonhard.</p>
            </div>
		</body>
	</text>
</TEI>