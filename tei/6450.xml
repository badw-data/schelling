<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 6450</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/6450">https://schelling.badw.de/text/6450</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/6450">https://schelling.badw.de/doc/6450</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
				<pb source="1r"></pb><p><placename ref="96">Jena</placename>, den <date when="1821-05-19">19ten Mai 1821</date>.</p>
            </div>
            <div>
                <p>Seit mehr als zwei Jahren, mein theurer Freund, trage ich Ihren letzten, mir so erfreulichen <bibl sameas="3583" type="document">Brief</bibl> im eigentlichsten Sinne mit mir herum. Bis an die nördlichsten Gränzen des ehemaligen <placename ref="889">Deutschen</placename> Reiches (<hi rend="aq"><foreign xml:lang="la">Eudora romani terminus imperii</foreign></hi>) hat er mich begleitet; 
                    denn auch ich habe meine Nordpolexpedition gemacht, obwohl ich nicht völlig bis zum 80sten Grade gekommen bin. 
                    Allein ich habe eingesehen, daß es für mich ein durchaus vergebliches Unterfangen ist, auf Reisen oder an einem 
                    fremden Orte Briefe beantworten zu wollen, und überhaupt irgend etwas Vernünftiges zu beginnen. Also ist Ihr 
                    Brief wieder mit mir nach <placename ref="96">Jena</placename> zurückgekehrt. Und da, wenn man einmal in’s Aufschieben 
                    hinein gerathen ist, nur eine besondere Veranlassung wieder heraus helfen kann, so freue ich mich, eine solche 
                    an beikommendem neuen <bibl n="Bd. 4" sameas="2478" type="literature">Calderonbande</bibl> gefunden zu haben. Möge der treffliche <persname ref="2687">Don Pedro</persname> bei Ihnen mein Fürsprecher und Entschuldiger seyn!</p>
                <p>Ich konnte den dringenden Einladungen der Meinigen und dem eigenen Verlangen, nach dreiundzwanzigjähriger 
                    Abwesenheit meine <placename ref="166">Vaterstadt</placename> wiederzusehen, nicht länger widerstreben. Und so machte ich 
                    im <date from="1819-09" to="1819-11">Herbst 1819</date> mich auf, und reiste über <placename ref="10">Göttingen</placename>, 
                    <placename ref="8">Hannover</placename>, <placename ref="117">Osnabrück</placename>, <placename ref="248">Oldenburg</placename> und <placename ref="553">Bremen</placename> nach Hamburg, wo ich den ganzen Winter zubrachte. Von dem <persname ref="3702">
                    Sardanapalischen</persname> Leben, <pb source="1v"></pb>welches ich daselbst geführt, ist wenig zu erzählen;</p>
                <lg>»<q>Immer war’s Sonntag, es dreht’ immer am Heerd sich der Spieß.<bibl n="100. Xenie" sameas="7015" type="literature"></bibl><persname ref="156"></persname></q>«</lg>
                <p>Doch war das rastlos thätige Treiben dieser großen <placename ref="166">Handelsstadt</placename>, schon um des Contrastes willen, mir sehr 
                    anziehend, und ich konnte der Wunderkraft einer freien Verfassung, die in so kurzer Zeit unheilbar scheinende 
                    Wunden zu heilen verstand, meine Bewunderung nicht versagen. Zwar ist mancher Einzelne in dem ungeheuren Umschwunge 
                    des Glücksrades zu Grunde gegangen; aber das Ganze besteht, und wird bestehen, bis ...</p>
               <p>Nachdem ich beinahe 6 Monate in <placename ref="166">Hamburg</placename> verschwelgt hatte, begab ich mich im <date from="1820-03" to="1820-05">
                    Frühjahr 1820</date> nach <placename ref="38">Kiel</placename>, als dem nördlichsten Punkte meiner Wanderschaft, und reiste 
                    dann über <placename ref="880">Eutin</placename>, <placename ref="369">Lübeck</placename>, durch <placename ref="1089">Meklenburg</placename>, 
                    nach <placename ref="27">Berlin</placename>, wo ich mehrere Wochen, im Kreise vieler alten und neuen Freunde und Bekannten, 
                    sehr vergnügt zubrachte. Ich gestehe, daß der Aufenthalt in der nordischen Königsstadt mir ungemein wohl behagte. 
                    Vielleicht ist kein Ort in Deutschland, und (<placename ref="25">Paris</placename> ausgenommen) vielleicht in <placename ref="890">Europa</placename> kein Ort, wo eine so große Anzahl von ausgezeichneten Männern in den meisten Fächern der Kunst und 
                    Wissenschaft versammelt wäre, wo zugleich eine so heitere Geselligkeit herrschte und auch für die Bedürfnisse des 
                    sinnlichen Menschen so reichlich und geschmackvoll gesorgt würde, wie in diesem deutschen <placename ref="1090">
                    Palmyra</placename>. Nur Eines fehlt: eine auch nur leidliche Gegend. Und da diese einmal zu meinen <pb source="2r"></pb>
                    unentbehrlichsten <hi rend="aq"><foreign xml:lang="fr">comforts</foreign></hi> gehört; da ich schlechthin keinen Begriff davon habe, wie man 
                    einen Sommer in Berlin überleben kann: so – kehrte ich nach <placename ref="96">Jena</placename> zurück.</p>
               <p>Seit einem Jahre lebe ich wieder in diesem einst so blühenden, jetzt so verfallenen, <placename ref="96">Musensitze</placename>; und daß ich 
                    die Hände nicht in den Schooß gelegt habe, davon wird beikommendes <bibl n="Bd. 4" sameas="2478" type="literature">Büchlein</bibl> Ihnen den bündigsten Beweis geben. Es 
                    ist wahr, arbeiten läßt sich vortrefflich in Jena; aber auch weiter nichts, als arbeiten. Im übrigen sage ich oft 
                    mit Faust: »<q>Es mögte kein Hund so länger leben!<bibl n="S. 34" sameas="16646" type="literature"></bibl><persname ref="156"></persname></q>« Was ehemals den Aufenthalt 
                    in Jena so anziehend machte, davon ist keine Spur mehr vorhanden, kaum der Name, kaum der Schatten des Namens. 
                    <bibl n="I,128" sameas="17136" type="literature"><hi rend="aq"><foreign xml:lang="la">Stat magni nominis umbra!</foreign></hi></bibl>
                    Indessen ist nichts so schlimm, das nicht auch sein Gutes hätte. Ich komme hier z.B. sehr selten in den Fall,
                    die Abnahme meines Gehörs zu beseufzen. Denn auch der letzte Rest von Geselligkeit kommt immer mehr abhanden, und 
                    <date from="1820-12" to="1821-02">vorigen Winter</date> ist manche Woche hingegangen, ohne daß ich meine Pantoffeln anders ausgezogen hätte, als um in’s 
                    Bett zu steigen, oder irgend einen andern Menschen gesehen, als meine betagte Aufwärterinn.</p>
               <p>Doch weßhalb behellige ich Sie mit diesen Kläglichkeiten? Lieber will ich Ihnen meinen herzlichen Dank sagen 
                    für die große Freude, die ich bei der Nachricht empfand, daß Sie <placename ref="21">München</placename> mit <placename ref="128">Erlangen</placename> vertauscht und den Wirkungskreis wieder aufgesucht haben, der unter allen Ihnen am meisten 
                    angeeignet <pb source="2v"></pb>ist. Auch für Ihre Gesundheit hoffe ich von dieser glücklichen Veränderung die besten 
                    Folgen. Mit großem Kummer hörte ich, während meines Aufenthalts in <placename ref="166">Hamburg</placename>, von der bedeutenden Gefahr, welcher 
                    Sie ausgesetzt waren. Gottlob, daß es sich so gewandt hat, mein theurer Freund! Das mildere Klima Ihres jetzigen 
                    Wohnortes wird den übeln Einflüssen der Münchener Sumpfluft kräftig entgegen wirken, und ich hoffe, mich selbst 
                    noch augenscheinlich von Ihrem vollkommenen Wohlseyn zu überzeugen. Wir sind ja nun nicht mehr so weit von 
                    einander entfert, daß eine Zusammenkunft zu den ganz undenkbaren Dingen gehören sollte. Ihre liebe <persname ref="223">Pauline</persname> <placename ref="96">hier</placename> zu sehen, ward uns im vergangenen <date from="1820-09" to="1820-11">Herbst</date> 
                    einige Hoffnung gemacht; leider unerfüllte! Empfehlen Sie mich der trefflichen Freundinn auf’s angelegentlichste.</p>
               <p><persname ref="156">Goethe</persname> – <hi rend="u">Sie</hi> werden gewiß noch eben so gern von ihm hören, wie vor 20 Jahren, 
                    obwohl andere unsrer Zeitgenossen ihn gleichsam als einen mediatisirten Fürsten zu behandeln gemeint sind – Goethe 
                    befindet sich jetzt, in seinem 72sten Jahre, besser als in früherer Zeit und ist noch immer zu unsrer Freude thätig. 
                    Der erste Band von <bibl sameas="16666" type="literature">W˖<expan>ilhelm</expan> Meisters Wanderjahren</bibl> – einer Art von <bibl sameas="9124" type="literature">
                    Dekameron</bibl>, dessen meiste Erzählungen schon einzelne gedruckt waren – wird noch in dieser Messe ausgegeben; so 
                    auch neue Hefte von <bibl n="Bd. 3" sameas="16667" type="literature">Kunst und Alterthum</bibl> und von der <bibl n="Bd. 1. H. 3" sameas="16668" type="literature">
                    Morphologie</bibl>. Auch hat er kürzlich einen <bibl n="Bd. 4. H. 1. S. 1-18" sameas="16667" type="literature">Prolog</bibl> zur <bibl sameas="16669" type="literature">
                    Iphigenie</bibl> gedichtet, womit das neue <placename ref="1301">Berliner Schauspielhaus</placename> soll eröffnet werden. Welch 
                    ein kräftiges, unerschöpfliches Alter!</p>
            </div>
               <div>
                  <p>Leben Sie wohl, mein theurer Freund. Lassen Sie mich, wenn es seyn kann, bald etwas von 
                    Ihnen hören, und bleiben Sie meiner in Freundschaft eingedenk.<lb></lb>Für immer<lb></lb>Ihr</p>
                  <p>JD Gries</p>
              </div>
              <div>
                  <p>Auf Ihr ausdrückliches Verlangen lasse ich dies Packet unfrankirt abgehen.</p>
            </div>      
        </body>
	</text>
</TEI>