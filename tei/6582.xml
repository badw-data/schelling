<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 6582</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/6582">https://schelling.badw.de/text/6582</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/6582">https://schelling.badw.de/doc/6582</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p><placename ref="96">Jena</placename>, 
                    <date when="1814-11-22">22 Nov˖<expan>ember</expan> 1814</date>.</p>
                <p>Mein theuerster Freund</p>
            </div>
            <div>
                <p>Ich habe so lange nichts von Ihnen gesehen, dass ich fast Ihrer 
                    Erinnerung zweifeln möchte, wenn ich nicht zuweilen von den 
                    <placename ref="96">hiesigen</placename> und <placename ref="57">gothaischen</placename> Freundinen Ihrer 
                    Frau <persname ref="223">Gemalin</persname> zuweilen erfreuliche 
                    Nachrichten von Ihrem Wohlbefinden von Ihrem häuslichen Glücke, 
                    wozu ich Ihnen theilnehmend<unclear cert="low">##</unclear> Glück wünsche, 
                    erhalten hätte.</p>
                <p>Ich schreibe Ihnen heute, um sie in einer literar˖<expan>ischen</expan> 
                    Anlegenheit um Ihren freunschaftlichen Rath zu bitten.</p>
                <p>Aus den öffentlichen Blättern wird Ihnen bekannt sein, dass seit 
                    einem halben Jahr meine holländische <bibl sameas="16884" type="literature">
                    Preisschrift über die Anatomie und Physiologie der Pflanzen</bibl> 
                    zu <placename ref="1608">Harlem</placename> erschienen ist. Um mir das <placename ref="889">deutsche</placename> Manuscript zu erhalten, habe ich das 
                    Original französisch geschrieben, und es ist mir jetzt in mehrerer 
                    Hinsicht daran gelegen, die deutsche Ausgabe bald in den Druk zu 
                    geben.</p>
                <p>Es hat mir leid gethan, dass ich, da die Preisschrift der <persname ref="4489">Teylerschen</persname> Gesellschaft gehört, ausser Stand 
                    gewesen bin, Ihnen oder der <placename ref="1059">bayerischen
                    Academie</placename> ein Exemplar derselben zu übersenden. Es ist bisher 
                    so wenig in der Pflanzenanatomie geschehen, und selbst <persname ref="4490">Moldenhawers</persname> neueste <bibl sameas="16885" type="literature">Schrift
                    </bibl> hat die vorhandene Verwirrung nur noch vermehrt, dass es 
                    <pb source="1v"></pb>mir eine der belohnendste Geschäfte gewesen 
                    ist, diese Schrift auszuarbeiten, und die Bildungsformen der 
                    Elementarorgane der Pflanzen auf bestimmte Gesetze zurückzuführen. 
                    In der beinahe vollendeten deutschen Bearbeitung, welche als eine 
                    zweite Ausgabe der französischen Schrift betrachtet werden muss, 
                    bin ich zu noch bestimmteren Resultaten gekommen, das Eigenthümliche 
                    der Mono- und Dicotyledonen ist hier auch in anatomischen <unclear cert="low">###</unclear> 
                    nachgewiesen; die nothwendige Urform der Pflanzenzellen, was manche 
                    andere, bisher kaum berührte Puncte sind zur Sprache gebracht, wird 
                    zum Theil gelöset werden, so sehr es mir unlieb seyn würde, die 
                    Herausgabe derselben verzögert zu sehen.</p>
                <p>Nun sind aber die Verhältnisse des <placename ref="889">deutschen</placename> Buchhandels und der 
                    deutschen Literatur <unclear cert="high">noch</unclear> so unsicher, dass es mir 
                    nicht möglich ist, für ein wissenschaftliches Werk, welches vermöge 
                    seines speciellen Gegenstandes nur ein beschränktes Publicum findet, 
                    und dessen Herausgabe eine Auslage von 900–1000 rh. 
                    fordert, einen Verleger zu finden, und ich selbst bin 
                    ausser Stande, auf eigne Kosten das Werk herauszugeben, so wie der 
                    angegebenen Verhältnisse wegen auch der Weg der Subscription eben 
                    so unsicher ist. Das französische Original enthält 46 Bogen von 22 
                    Kupfertafeln in <pb source="2r"></pb>Quart. Die deutsche Ausgabe (da 
                    sie mit allen meinen Arbeiten seit <date when="1812">1812</date> 
                    vermehrt ist) mochte leicht 60 Bogen mit 30 Kupfertafeln betragen, 
                    welche letzteren, wenn das deutsche Werk dem Französischen nicht 
                    nachstehen soll, von demselben vortreflichen <persname ref="4491">Künstler</persname> von <placename ref="926">Amsterdam</placename>, welcher die 
                    Platten der franz˖<expan>ösischen</expan> Ausgabe gestochen, gearbeitet werden 
                    möchten.</p>
                <p>Es ist mir nun gerathen worden, diese deutsche Ausgabe der bayerschen 
                    <placename ref="1059">Academie der Wissenschaften</placename> anzubieten, ob sie sie als ein Geschenk 
                    annehme, und auf ihre Kosten herausgeben wollte, da ich gerne auf 
                    allen pecuniairen Vortheil verzichte, wenn meine Arbeiten nur auf 
                    andre Weise sich belohnen.</p>
                <p>Ich weiß nicht, an welches Mitglied der Academie ich mich deshalb zu 
                    wenden habe, daher ich Sie, mein hochverehrter Freund, 
                    wohl ersuchen darf, hierüber bei der Academie vorläufig anzufragen, 
                    und mir davon Nachricht zu ertheilen. Allenfalls kann ich Ihnen auch 
                    vorher mein Handexemplar des franz˖<expan>ösischen</expan> Werkes zur Einsicht 
                    ubersenden.</p>
                <p>Von meinen persönlichen Verhaltnissen habe ich Ihnen noch nichts gesagt. 
                    Im <date when="1814-01">Jan˖<expan>uar</expan> d<expan>ieses</expan> J˖<expan>ahres</expan></date> zog ich 
                    in der <placename ref="98">weimarischen</placename> Schaar <pb source="2v"></pb>der Freiwilligen mit zu Felde, wo ich in dieser herrlichen Zeit das 
                    Leben in seiner edelsten Gestalt kennen lernte, so sehr mir jetzt die 
                    Zeit des Feldzuges kaum mehr als Wirklichkeit; nur als ein schöner 
                    Jugendtraum erscheint. Ich war mit <persname ref="181">Steffens</persname> 
                    und <persname ref="1249">Raumer</persname> 4 Wochen in <placename ref="25">
                    Paris</placename>, wo wir uns in den Hass gegen dies Volk ersättigten; dann 
                    14 Tage in <placename ref="1225">Holland</placename>, und bin seit anfangs 
                    <date when="1814-07-01">Juli</date> wieder hier, wo manche harte 
                    Schicksale mich betrafen die mich an alles Besseres würden haben
                    verzweifeln lassen, wenn nicht jener Feldzug mich das Höhere des 
                    Lebens kennen gelernt hätte.</p>
                <p>Unsre <placename ref="2050">Academie</placename> hat sich vegetativ erhalten, und wir erwarten jetzt den 
                    Ausgang des Congresses, ob dieser uns vielleicht etwas bringt, 
                    was auch unserm wissenschaftlichen Leben einen neuen Schwung giebt.</p>
            </div>
            <div>
                <p>Leben Sie wohl. Es wird mich sehr erfreuen, wenn ich 
                    mich Ihres ferneren Wohl durch sie selbst Nachricht erhalte.<lb></lb>
                    Ihr</p>
                <p>Dr DG Kieser</p>
            </div>
		</body>
	</text>
</TEI>