<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 6627</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/6627">https://schelling.badw.de/text/6627</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/6627">https://schelling.badw.de/doc/6627</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p><placename ref="21">München</placename> <date when="1816-08-11">11. Aug˖<expan>ust</expan> XVI</date>.</p>
            </div>
            <div>
                <p>Mich selbst nach <placename ref="694">Schleedorf</placename> zu bringen, das wird 
                    mir, wie ich fürchte, kaum möglich werden. Meine <persname ref="4582">
                    Frau</persname>, welche im <date when="1816-12">Dezember</date> ihre 
                    Niederkunft erwartet, fühlt sich, seit einigen Tagen, zwar nicht unwohl, 
                    aber doch unbehaglich; jede Anstrengung wirkt bedeutend auf sie und so 
                    scheint uns dieser Ausflug etwas zu gewagt, obgleich es schmerzhaft ist, 
                    den schönen Plan ohne Ausführung lassen zu müssen. – Vielleicht versuchen 
                    wir eine Fahrt nach dem <placename ref="1780">Stahrenberger See</placename> und 
                    verweilen da einige oder mehrere Tage, das wird aber auch Alles seyn. 
                    Und allein mag ich meine arme Frau jezt nicht lassen, um so weniger, da 
                    die klimatischen Mißverhältnisse und das Ungesellige der Eingebohrenen 
                    nicht günstig auf ihre Stimmung einwirken. – – So werde ich denn des 
                    Genusses, auf den ich so lange schon mich innnig gefreut: Sie recht bald 
                    persönlich zu begrüssen, noch länger entbehren müssen. Vergönnen Sie mir 
                    darum immer, Ihre ländliche Abgeschiedenheit durch einige Zeilen nach 
                    der geräuschvollen <placename ref="21">Stadt</placename> (worinn ich übrigens sehr 
                    geräuschlos lebe) unterbrechen und diese Unterbrechungen <pb source="1v">
                    </pb>von Zeit zu Zeit wiederholen zu dürfen.</p>
                <p>Was Sie mir über <persname ref="156">Göthe</persname> zu <bibl sameas="2576" type="document">
                    sagen</bibl> so gefällig waren, hat mich tief bekümmert, um so mehr, da ich 
                    auch drei Briefe ohne Antwort bin und auch sonst keine Kunde erhielt. 
                    Oeffentlichen Nachrichten zu Folge sollte er nach <placename ref="1693">
                    Baaden-Baaden</placename> haben reisen wollen; ob das wirklich geschehen seyn 
                    mag? – Wäre ich anmassender, als ich zu seyn mir schmeichle, so würde 
                    ich sagen: Ihr Urtheil über seine neueste <bibl sameas="16667" type="literature">kleine 
                    Schrift</bibl> ist mir wie aus dem Innersten genommen. – Ich weiß nicht, 
                    ob Ihnen die mannigfachen, mitunter bitter würkende Urtheile zu Ohren 
                    gekommen, welche man sich über das Werke gar erlaubt. Man spricht sogar 
                    von einer Parodie, welche Frau <persname ref="4956">von Savigny 
                    (geb˖<expan>orene</expan> Brentano)</persname> unter der Feder, oder vielleicht schon unter 
                    der Presse habe. Ich vermag übrigens nicht zu sagen, wie es damit gemeint 
                    ist, möglich, daß mein Unwille mich nur zur Hälfte hören, oder unrichtig 
                    verstehen ließ.</p>
                <p>In <placename ref="27">Berlin</placename> hat <persname ref="156">Göthe</persname>, wie mich ganz neue Briefe von 
                    daher belehren, mächtig angeregt – <persname ref="3966">Schuckman</persname> 
                    soll zum Kunstkenner, oder mindestens zum Kunstbeschüzzer geworden seyn. 
                    So viel ist gewiß, daß <persname ref="3218">Schinkel</persname> <pb source="2r">
                    </pb>sich in <placename ref="14">Heidelberg</placename> befindet, nur wegen Ankauf 
                    der Sammlung von <persname ref="515">Boisserées</persname><persname ref="3780"></persname> zu 
                    unterhandeln. Der <persname ref="35">König</persname> bestimmte sie, wie ich 
                    höre, für das Museum in Berlin.</p>
                <p>Meine <bibl sameas="16983" type="literature">Abhandlung</bibl> habe ich in der <placename ref="1059">Akademie</placename> vorgelesen und die hat, wie ich glaube, nicht ganz 
                    mißfallen. Daß sie aber nun ohne Ihre Prüfung, ohne Ihr Urtheil hinaus 
                    in die grosse Welt soll, behagt mir keineswegs. – Beim Verfassen war es 
                    mir stets ein Trost, wenn ich Sie, als höchste Instanz im Geiste 
                    erblikte. Wie soll nun das werden? – Ich habe zwar allerdings mit 
                    manchen unserer achtbaren Kollegen Rath gepflogen, so mit <persname ref="456">Sömmerring</persname> über das Petrefaktelogische, mit <persname ref="4265">Vogel</persname> über das Chemische u.s.w. und mich viel guten 
                    Rathes über solche Einzelnheiten zu erfreuen gehabt, allein das 
                    Ganze, da wußte ich mir keinen bewährtern Richter als Sie. – Und nun 
                    ruht Gefahr auf dem Verzuge. <date when="1816-08-19">Montag (19. 
                    Aug˖<expan>ust</expan>)</date> soll mit dem Sezzen der Anfang gemacht werden und das 
                    scheint mir auch in der That nothwendig, da die Abhandlung längst 
                    8-10 Bogen stark werden <pb source="2v"></pb>dürfte und Herrn G˖<expan>eheim</expan>R˖<expan>ath</expan> 
                    <persname ref="71">von Ringels</persname> Wunsche gemäß vor dem <date when="1816-10-12">12. Okt<expan>o</expan>ber</date> gedruckt seyn soll. – – 
                    Hätte ich gewußt, daß es so kommen würde, ich hätte die Vorlesung 
                    bis zum <date when="1817-03-28">28. März</date> aufgeschoben. 
                    Und wollte ich es auch wagen Ihnen das M<expan>anu</expan>s<expan>cri</expan>pt zu senden, 
                    allenfalls durch einen Eilboten, so wäre das einmal eine Sünde gegen 
                    Ihre Zeit, die ich mir nicht vergeben könnte und sodann ist das 
                    Geschriebene zum Selbstlesen für einen Dritten bei meistem zu 
                    unlesbar (obgleich es jezt im Sekretariat niedergelegt ist, um, wie 
                    ich fürchte, nicht gelesen zu werden). – – –</p>
                <p>Um von diesem zu egoistischen Gegenstande abzukommen, wende ich mich 
                    zunächst zu den neulichen Debatten, die Aenderung in dem 
                    Konstitutionellen der Akademie bet<expan>reffend</expan>. Vom Präsidenten und 
                    Senate war vorläufig noch keine Rede (obgleich, wie auch mir 
                    bekannt, Alles dieses im Hintergrunde liegt), sondern nur von einem 
                    berathenden Ausschusse über mögliche und anwendbare Verbesserungen. 
                    Darüber ist an die <pb source="3r"></pb>höchste Stelle Bericht erstattet 
                    worden, ohne daß bis jezt Entscheidung eingekommen wäre, wenigstens 
                    so viel mir laut geworden.</p>
                <p>Es ist erfreulich zu sehen wie jezt bei wiedergekehrter erträglicherer 
                    Zeit (noch sieht es in vielen Staaten im Besondern und selbst im 
                    Ganzen des lieben <placename ref="889">Deutschen Landes</placename> nicht 
                    geregelt und klar genug aus, um ein anderes Beiwort gebrauchen zu 
                    dürfen), die Gelehrten nach allen Seiten sich bewegen. Seit <date when="1817-08-03">acht Tagen</date> sind <persname ref="970">
                    Raumer</persname> und <persname ref="3354">Hagen</persname> aus <placename ref="219">Breslau</placename>, auf der Reise nach <placename ref="1034">Italien
                    </placename> begriffen, bei uns. Vom ersteren hatte ich das Vergnügen Ihnen 
                    einen <bibl sameas="6861" type="document">Brief</bibl> zu senden. Er beabsichtigt, wie 
                    ich höre, Nachforschungen in Betreff der Geschichte der 
                    Hohenstauffen und Hagen jagt, wie natürlich, den <bibl sameas="16933" type="literature">Niebelungen</bibl> nach. Ebenso ist jezt <persname ref="1447">Niebuhr</persname> <placename ref="21">hier</placename>, der gleichfalls nach Italien sich begiebt. – –</p>
                <p>Über die neue <placename ref="1783">Universität</placename> in <pb source="3v"></pb>Rhein-Preussen scheint, selbst was das 
                    Ortliche betrifft, noch immer nichts entschieden. Man sagt 
                    <persname ref="3966">Schuckman</persname> werde sich selbst dahin begeben, um zwischen <placename ref="263">Kölln</placename> und <placename ref="146">Bonn</placename> zu bestimmen.</p>
                <p>Doch es ist Zeit, daß ich schließe, mein Plaudern möchte Ihnen 
                    Langeweile bringen.</p>
            </div>
            <div> 
	            <p>Unter den herzlichsten Empfehlungen von Haus zu Haus 
                    und mit reinster Verehrung<lb></lb>
                    Ihr ganz ergebenster</p>
	            <p>Leonhard.</p>
            </div>
            <div>
                <p>Ich habe, um eine Frage Ihres gütigen <bibl sameas="2576" type="document">Briefes</bibl> nicht unbeantwortet zu 
                    lassen, vor dem neuesten (ich glaube <placename ref="2131">Josephs-)Thore</placename>, beim 
                    Proviantbäcker <persname ref="5185">Zänger</persname> gemiethet, eine ganze Etage im 1. Stock und 
                    zahle – – 700 fl. – –</p>
            </div>
		</body>
	</text>
</TEI>