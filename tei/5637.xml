<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 5637</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/5637">https://schelling.badw.de/text/5637</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/5637">https://schelling.badw.de/doc/5637</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p><placename ref="128">Erlangen</placename> <date when="1827-06-11">11. Jun˖<expan>i</expan> 1827</date>.</p>
                <p>Liebster Bruder!</p>
            </div>
            <div>
                <p>Es ist mir höchst schmerzlich, zu sehen, daß ich durch frühere Äußerungen Dich gekränkt habe. Ich läugne nicht: 
                    mir schien, als wärest Du persönlich gegen meine Absicht eingenommen, gleich als könntest Du dadurch compromittirt 
                    werden, da ich doch von Anfang an Dich aus dem Spiel gelassen hätte, wenn Du nicht früher Dich in Bezug auf <persname ref="225">Paul</persname> 
                    von freyen Stücken zu der Gefälligkeit erboten hättest, um die ich Dich nachher in Bezug auf <persname ref="224">Fritz</persname> bat. Diese 
                    Mißstimmung, schien es mir, verhinderte Dich, in meine Lage Dich zu versetzen; Du scheinst meinen Entschluß als einen willkührlichen
                    anzuseh'n, vieleicht bloß gefaßt in der Meynung, dabey wieder auf Deine Güte für die Kinder rechnen zu dürfen, da ich doch, 
                    Gott weiß es, besonders in Ansehung Pauls gern jeden andern Ausweg ergriffen hätte; denn für Friz die Gelegenheit zu versäumen, 
                    wenn sie mir, wie <persname ref="958">Planck</persname> versicherte, offen stand, hätte ich bey meiner Lage in keinem Fall gerathen 
                    und recht finden können. Du kannst von der Beschaffenheit dieser Mittel-Schulen bey uns Dir keine Vorstellung machen; ich konnte 
                    noch etwa denken, Paul hieher zu nehmen, wo das was unsren Gymnasien an zweckmäßiger Einrichtung und Methode fehlte, durch meine 
                    Einwirkung sich vielleicht ersetzen ließ; aber nun erhielt ich die Gewißheit, daß der <persname ref="444">König</persname> mich nicht 
                    hier lassen würde, und diese Art von Schule in <placename ref="21">München</placename> habe ich als wahre geistige und moralische Mördergruben 
                    anseh'n lernen. Dazu kam nun noch, daß ich Deine <pb source="1v"></pb>Äußerungen in Bezug auf die <hi rend="aq"><foreign xml:lang="la">hospites</foreign></hi> 
                    mißverstehen, und die Meynung bei Dir voraussetzen mußte, als wollte ich auch Paul in eine Promotion bringen. Dieß alles zusammen, 
                    das mir allerdings empfindlich war, mochte freylich meinen Erwiederungen einen Charakter von Gereiztheit geben, der nicht in meinem 
                    Vorsatz lag. Ich bedaure dieß von Grund des Herzens, bitte Dich aber, wenn meine Äußerungen Dich verletzt haben sollten, mir auch aufrichtig 
                    und von Herzen zu verzeihen.</p>
                <p>Von der gefährlichen Wendung, welche die Masern bey Friz genommen, haben wir durch Deinen <date when="1827-06-10">gestern</date> angekommnen <bibl sameas="7245" type="document">Brief</bibl> die 
                    erste Nachricht erhalten - Gott sey gepriesen, und nächst Gott gebührt Dir unser gerührtester Dank dafür, daß der gute 
                    Junge erhalten worden. Ich muß darauf verzichten, Worte und Ausdrücke zu finden, Dir für diesen neuen, großen Beweis Deiner 
                    brüderlichen Liebe, mit welcher Du, unangesehen Deiner vielen und drückenden Geschäfte und selbst der zu Deiner nothwendigen 
                    Erholung unentbehrlichen Nachtruhe, dem lebensgefährlich erkrankten zu Hülfe kamst, gebührend zu danken; wie ich ohnedieß 
                    darauf verzichten muß, jemals Mittel und Wege zu finden, alle die Liebe und Güte, die Du in so überreichlichem Maß meinen 
                    Kindern bewiesen, einigermaßen zu vergelten. Wir haben inzwischen von dem Planck'schen <persname ref="3645">Verwandten</persname> in 
                    <placename ref="624">Weißenburg</placename> gehört, daß Herr Rector über <date from="1827-06-03" to="1827-06-04">Pfingsten</date> 
                    verreist sey. Soviel wir wissen, war es seine Absicht, um diese Zeit mit Paul nach <placename ref="36">Urach</placename> zu gehen. 
                    Wir hoffen daraus schließen zu dürfen, daß es dennoch mit Friz fortwährend besser gegangen und auch Paul <pb source="2r"></pb>bis dahin 
                    verschont geblieben sey, bey dem es mit den Masern vielleicht wie mit dem Scharlachfieber geht, von dem 
                    er auch nicht angesteckt wurde, obgleich er mit Friz in beständiger Berührung blieb. Gott gebe, daß wir bald vollkommen 
                    beruhigt werden.</p>
                <p>Wegen der Erndtevacanz war es unsre Absicht, die Kinder dießmal hieher kommen zu lassen, dagegen im Herbst, wo entweder erst unser 
                    Umzug stattfinden oder wenigstens die Einrichtung in München noch nicht vollständig getroffen seyn wird, den Paul gleich 
                    nach Urach gehen zu lassen, um vielleicht während der Ferien noch von <persname ref="819">Köstlin</persname> bestimmtere Vorbereitung 
                    zu erhalten; den Friz aber würden wir dann gern zu dem <persname ref="204">Onkel</persname> nach <placename ref="695">Neustadt</placename> gehen lassen. 
                    Allein es ist bis jetzt alles noch so unbestimmt, unter anderm auch wohl die Zeit der Erndtevacanz, worauf es doch auch ankommt (da ich 
                    <date when="1827-08-15">Mitte Augusts</date> noch einmal nach <placename ref="187">Carlsbad</placename> zu gehen wünsche), daß ich 
                    Dich bitte, vorläufig, wenn Du an August schreibst, ihm nur herzlich für seine gütige Einladung zu danken,
                    von der wir jedenfalls entweder für die Erndte- oder die Herbstvacanz, für letztere wenigstens, wenn ihm nicht grade 
                    die Zeit entgegen ist, für Friz, dankbaren Gebrauch machen werden.</p>
                <p>Wegen der Uhr für Paul bitte ich Dich recht sehr, wenn Du je sie ihm aufheben willst, sie ihm wenigstens jetzt nicht zu geben.
                    Er würde sie in kürzester Zeit verdorben haben, und in etwas längerer, so wie ich ihn kenne, ganz gewiß verlieren. Ich war gewiß 
                    schon zwey Jahre <hi rend="aq"><foreign xml:lang="la">hospes</foreign></hi> in <placename ref="43">Bebenhausen</placename>, als ich die erste Uhr von den 
                    <persname ref="191">Eltern</persname><persname ref="88"></persname> <pb source="2v"></pb>geschenkt erhielt. Ebenso bitte ich das Geschenk von 
                    Herrn <persname ref="278">Georgii</persname> noch zurückzuhalten; dagegen werde ich ihm auftragen, einen Danksagungsbrief an diesen zu 
                    schreiben.</p>
            </div>
               <div>
                    <p>Lebe recht wohl; ich bitte Dich nochmals, wenn ich durch meine Äußerungen Dich gekränkt habe, es mir nicht im Herzen 
                        zu behalten. Grüße Deine liebe <persname ref="211">Frau</persname> bestens von uns; auch an <persname ref="226">Clärchen</persname> und 
                        <persname ref="101">Beate</persname> herzliche Grüße<lb></lb>Dein<lb></lb>tr˖<expan>euer</expan> Br˖<expan>uder</expan></p>
                    <p>Fr.</p>
            </div>
            <div>
                <p>N.S.</p>
                <p>Aus einem mir geschickten <bibl sameas="8402" type="document">Brief</bibl> von <persname ref="819">Köstlin</persname> habe ich erst ersehen, daß man Dich ObermedicinalRath titulirt!
                    Entschuldige die bisherige Unterlassung mit meiner Unwissenheit. Auch bitte ich Dich das »Hochwohlgeb˖<expan>ohren</expan>« auf der Addresse an mich 
                    wegzulassen, wie ich es bey Dir mache. Es kommt mir gar zu seltsam vor, daß Brüder einander so betiteln.</p> 
                <p>Das hast Du Dir in <placename ref="96">Jena</placename> wohl nicht eingebildet, daß die Thesen, die Du <date when="1802">einmal</date> in 
                    meinem Disputatorium aufstelltest, nach 25 Jahren noch gedruckt werden sollten. Gleichwohl ist dieß geschehen, in 
                    <hi rend="u"><persname ref="119">Solger</persname></hi>'s <bibl n="Bd. 1. S. 88-90" sameas="3933" type="literature">Nachgelaßnen Schriften</bibl>. 
                    Er war, was ich auch nicht mehr wußte, Dein Opponent und man scheint die Papierschnizel von ihm noch zusammengerafft zu haben.</p>
            </div>
        </body>
	</text>
</TEI>