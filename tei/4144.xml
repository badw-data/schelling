<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 4144</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/4144">https://schelling.badw.de/text/4144</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/4144">https://schelling.badw.de/doc/4144</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p>H.H.</p>
                <p><placename ref="21">München</placename> den <date when="1811-06-02">2. Juny 11</date>.</p>
            </div>
            <div>
                <p>Herr Dr. <persname ref="270">Spix</persname> sagt mir, daß Sie von der Messe 
                    zurück sind. Auf das frühere <bibl sameas="8273" type="document">Schreiben</bibl>, das 
                    Sie mir die Ehre erzeigt haben an mich zu erlassen, konnte ich in so 
                    fern nicht antworten, als sich damals über meine nächsten literarischen 
                    Arbeiten nichts bestimmen ließ. Da Sie aber den Wunsch geäußert haben, 
                    auch mit anderen Gelehrten von meiner Bekanntschaft in Verbindung 
                    zu kommen, so benutze ich eine so eben sich anbietende Gelegenheit, 
                    Ihnen einen Antrag der Art zu machen.</p>
                <p><persname ref="391">A.W. Schlegel</persname> hat mich ersucht, 
                    ihm für 2 Werke einen Verleger in <placename ref="889">Deutschland</placename> zu 
                    schaffen. Das erste sind seine <bibl sameas="8881" type="literature">poetische Werke</bibl>, 
                    oder eine Sammlung seiner Gedichte, die zwar im dießjährigen <bibl sameas="16783" type="literature">O˖<expan>ster</expan>M˖<expan>esse</expan> Katalog</bibl> bereits von <persname ref="2492">Mohr</persname> und <persname ref="2516">Zimmer</persname> in <placename ref="14">Heidelberg</placename> als in ihrem Verlag erscheinend angekündigt worden. 
                    Allein da diese Herren seitdem Herrn Prof. Schlegel 
                    erklärt haben, daß Sie von widrigen Umständen gezwungen mit allen noch 
                    nicht angefangnen Unternehmungen auf 1–1 1/2 Jahre innehalten müssen, 
                    dieser aber mit der Herausgabe nicht so lange warten will, so ist der 
                    Contract zwischen beyden Theilen aufgehoben worden und der V<expan>er</expan>f˖<expan>asser</expan> 
                    sucht einen anderen Verleger. Ich bemerke nur, daß die frühere bey <placename ref="1182">Cotta</placename> erschienene und schon längere Zeit vergriffene 
                    <bibl sameas="6104" type="literature">Auflage</bibl> höchstens 1/3 der neu hinzukommenden 
                    Gedichte beträgt, welche in der neuen Auflage erscheinen sollen. Der 
                    V<expan>er</expan>f˖<expan>asser</expan> verlangt 100 Carol˖<expan>in</expan> honorar, und eleganten Druck.</p>
                <pb source="1v"></pb><p>Das andre Werk soll den Titel führen: <hi rend="u">Geschichtliche 
                    Forschung über das Lied der Nibelungen und die Alterthümer der deutschen 
                    Sprache und Dichtung überhaupt</hi>. – Es ist Ihnen nicht unbekannt, welches 
                    Interesse seit mehrern Jahren dieß uralte <bibl sameas="16785" type="literature">National
                    gedicht</bibl> fast allgemein erregt hat. Über den Ursprung, die Fortbildung 
                    und das gesammte Historische desselben ist nichts von Bedeutung geschrieben, 
                    und von den neuesten Bearbeitern in dieser Beziehung so gut wie 
                    nichts geleistet worden. Auch ist wohl anzunehmen, daß keiner unsrer 
                    Gelehrten in dieser Forschung soviel als <persname ref="391">A.W. Schlegel</persname> 
                    zu leisten vermögen würde. Ich halte daher diese Schrift für eine der 
                    empfelenswerthesten für jeden Verleger. Der V<expan>er</expan>f˖<expan>asser</expan> verlangt 
                    2 1/2 Carol˖<expan>in</expan> honorar für den Bogen.</p> 
                <p>Ich selbst gehe in diesem Augenblick schwanger mit dem Entwurf eines neuen 
                    <bibl sameas="44" type="literature">Journals</bibl>, wodurch einmal wieder kräftig auf das 
                    ganze Publikum und die gesammte Nationalität unsres <placename ref="889">Volks</placename> gewirkt werden 
                    soll. <hi rend="u">Es wird kein Fach ausschließen</hi>, aber in jedem Fach so weit 
                    es nur möglich, auf das Rechte, Kräftige und Tüchtige hinwirken. Original-
                    Aufsätze, Beurtheilungen merkwürdiger literar˖<expan>ischer</expan> Erscheinungen, 
                    kritische Übersichten dessen, was in den Hauptfächern der Wissenschaft 
                    geschieht, werden abwechseln. Im Ganzen wird es ein Journal von der Art 
                    des ehemaligen <bibl sameas="70" type="literature">deutschen Museums</bibl>, oder des 
                    zeitherigen <bibl sameas="104" type="literature">deutschen Merkurs</bibl> seyn, nur mit weit 
                    entschiednerer Absicht und <pb source="2r"></pb>kräftigerem Sinn geschrieben. 
                    Um ihm den größtmöglichen Gehalt und die allgemeinste Wichtigkeit zu 
                    geben, wird alles darauf ankommen, die vorzüglichsten Männer zu 
                    Mitarbeitern zu gewinnen. Manche derselben zähle ich unter meine Freunde; 
                    diese haben auch die Idee davon mit Freuden ergriffen und alle Thätigkeit 
                    zugesagt; andre müssten noch gewonnen werden. Ein Haupterforderniß dazu 
                    wäre ein tüchtiges Honorar. – Die Hefte würden zwanglos, doch so viel 
                    möglich alle 1–2 Monate eines, erscheinen. Ich würde die Redaktion 
                    übernehmen. Da ich meine übrigen Arbeiten, der langen Verbindung wegen, 
                    in der ich mit Herrn <persname ref="246">Cotta</persname> stehe, in der Regel 
                    seiner Buchhandlung überlasse: so suche ich für dieses Journal aus dem 
                    Grunde einen andern Verleger, weil Cotta schon mit Zeitschriften überhäuft 
                    ist und daher der einzelnen, wie ich zu bemerken glaube, nicht den Fleiß 
                    widmen kann, der zum Betrieb eines Journals schlechterdings erfoderlich 
                    ist. Die allgemeine Meynung und besonders die meiner Freunde, <persname ref="182">Gehlen</persname> u.a. läßt mich mit Zuversicht erwarten, daß in 
                    Ihren Händen diese Zeitschrift einen ganz vorzüglichen Schwung erhalten 
                    könnte. Haben Sie die Güte, sich einstweilen die ganze Unternehmung zu 
                    überlegen; denn sie geht allerdings etwas in’s Große.</p>
                <pb source="2v"></pb><p>Ich ersuche Sie, mir wegen der beyden Werke von Schlegel 
                    <hi rend="u">baldmöglichst</hi> Antwort zu geben; es sollte mich sehr freuen, wenn 
                    Sie dieselben übernähmen.</p> 
                <p>Wollten Sie sich wegen des <bibl sameas="44" type="literature">Journals</bibl>, (von dessen Idee ich Sie bitten muß, 
                    schlechterdings niemanden etwas mitzutheilen, indem nur die nächsten Freunde 
                    bis jetzt davon wissen sollen), im Allgemeinen wenigstens jetzt schon, 
                    erklären, so bitte ich dieß auf einem besondern Blatte zu thun. Entscheiden 
                    müssten Sie sich auf jeden Fall bis <date when="1811-07-15">Mitte 
                    nächsten Monats</date>, indem der Anfang spätstens zu <date when="1812-03-29">Ostern 1812</date> gemacht werden soll.</p> 
            </div>
            <div>
                <p>Mit wahrer Hochachtung empfele ich mich,<lb></lb>
                    Dero<lb></lb>
                    ergebenster </p> 
                <p>Schelling.</p>
            </div>  
        </body>
	</text>
</TEI>