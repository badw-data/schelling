<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 3911</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/3911">https://schelling.badw.de/text/3911</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/3911">https://schelling.badw.de/doc/3911</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p>Was werden Sie denken, theurester Freund, daß ich es 
                    wieder so lange anstehen lassen, Ihnen zu schreiben? Allein ich stelle 
                    Sie mir vor ganz verschlungen in die großen Angelegenheiten unseres 
                    <placename ref="1009">Vaterlandes</placename> und daß Sie wenig bemerken, ob Ihre auswärtigen Freunde 
                    Ihnen schreiben oder nicht. Dank für einige Aufschlüsse, die Sie mir 
                    <bibl sameas="2136" type="document">gegeben</bibl>. Die letzten Acten-Stücke in der <bibl n="Nr.37 v. 28.03.1816. S.145-148" sameas="102" type="literature">Allg˖<expan>emeinen</expan> Zeitung
                    </bibl> sind nicht sehr erfreulich, ausgenommen daß ich an der 
                    geistreichen und kraftvoll-würdigen Sprache in den Königlichen 
                    Erlassen mich wahrhaft geweidet. Es wäre schlimm, wenn unsre ehrlichen 
                    Landsleute noch am Ende gar ein Werkzeug in den Händen des <hi rend="u">Adels</hi> würden. Was Sie mir ferner vom Stand der Dinge mittheilen können 
                    und wollen werde ich mit dem größten Dank annehmen.</p>
                <p>Haben Sie die <bibl n="Nr. 29 v. Februar. S.225–232" sameas="2" type="literature">Recension</bibl> meiner <bibl sameas="15525" type="literature">samothrac˖<expan>ischen</expan> Abh˖<expan>andlung</expan></bibl><bibl n="Bd. I 19. S. 207–271" sameas="6347" type="literature"></bibl> 
                    in der Hallischen L˖<expan>iteratur</expan> Z˖<expan>eitung</expan> 
                    gelesen? Der Verf˖<expan>asser</expan> ist Herr Prof. <persname ref="764">
                    Köppen</persname> in <placename ref="250">Landshut</placename>, ein Günstling und 
                    Vorfechter <persname ref="269">Jacobis</persname>, der in jener Zeitung 
                    stets alle meine Schriften beurtheilt. Es ist <pb source="1v"></pb>schon 
                    an sich schändlich, daß einem notorisch in literarischer und 
                    persönlicher Gegnerschaft mit mir stehenden Menschen dort alle meine 
                    philos˖<expan>ophischen</expan> Arbeiten überlassen werden; aber daß ihm auch eine 
                    durchaus gelehrte Abh˖<expan>andlung</expan> über einen Gegenstand, von dem ihm 
                    eigene Kenntnisse gänzlich mangeln, preisgegeben wird, ist doch zu 
                    grob. Ich irre mich in dieser Vermuthung nicht, denn obgleich sich 
                    die Rec˖<expan>ension</expan> einen Anstrich von Gelehrsamkeit zu geben sucht, so 
                    zeigt sie doch keine andre Kenntnisse als die aus der Ab˖<expan>handlung</expan> 
                    selbst geschöpft sind und nur verdreht werden. Solche Recens˖<expan>ionen</expan> 
                    meiner philos˖<expan>ophischen</expan> Schriften habe ich immer verachtet; wenige 
                    sind so einfältig oder unbekannt, nicht zu wissen, daß dieß bloße 
                    Parteyschriften sind. Aber in diesem Fach gelehrter Forschung ist 
                    jene Abh˖<expan>andlung</expan> mein erstes Werk; niemand kann glauben, daß auch 
                    hier jene Partey im Spiel sey: unter diesen Umständen und da ich die 
                    wirklich in’s Unglaubliche gehende Unkenntniß oder (wenn es 
                    absichtliche Verdrehungen sind, wie fast zu glauben ist, weil <hi rend="u">
                    solche</hi> Misverständnisse unabsichtlich kaum stattfinden können) – 
                    Unverschämtheit des Recens˖<expan>enten</expan> augenscheinlichst darthun kann, 
                    scheint es mir doch nöthig, eben auf diese Rec˖<expan>ension</expan> öffentlich 
                    zu antworten. Ich bitte Sie über diese ganz eigne Lage, in der ich 
                    mich befinde, <pb source="2r"></pb>unaufhörlich vor unsrem <placename ref="889">deutschen</placename> Publicum in jener Zeitung von meinen wüthendsten 
                    Feinden heruntergerissen, geschmäht, ja verläumdet zu werden, einen 
                    Augenblick ruhig nachzudenken, und mir dann Ihre Meynung darüber zu 
                    sagen. Ich wäre nicht abgeneigt, diese und noch eine andre 
                    (obwohl weit bessere) Recension zusammen, mit ganz kurzen Anmerkungen 
                    abdrucken zu lassen? Würden Sie dieß gut finden und mir dazu die Hände 
                    bieten?</p>
                <p>Es ist hier vor etlichen Tagen eine <bibl sameas="16924" type="literature">Abhandlung 
                    über die älteste Kunst-Epoche Griechenlands</bibl> erschienen, von der 
                    man suchen wird, viel Aufhebens wenigstens in nichtgelehrten Blättern 
                    zu machen. Desto schlimmer für uns, da der <persname ref="487">Verf˖<expan>asser</expan></persname> von Kunst 
                    weder Kenntnisse noch reife Anschauung besitzt, und ihn bey dieser 
                    Arbeit sogar sein sonstiger Takt für philologische Genauigkeit 
                    verlassen hat. Wir müssen wünschen, daß man auswärts wenigstens nicht 
                    glaube, es lasse sich irgend Jemand von dengleichen hier imponiren 
                    oder man dürfe in <placename ref="21">München</placename> eben alles schreiben. 
                    Ein hier befindlicher auswärtiger <persname ref="1409">Gelehrter</persname> 
                    hat über diese Abh˖<expan>andlung</expan> einen trefflichen und (ohne alle Beleidigung) 
                    gründlichen <bibl n="Nr. 8 v. 17.06.1816. S. 29–31. Nr. 9 v. 28.06.1816. S.333–36" sameas="16744" type="literature">Aufsatz</bibl> zu schreiben angefangen. Ich biete 
                    Ihnen denselben <pb source="2v"></pb>für das kürzlich begonnene <bibl sameas="16744" type="literature">Kunst-Blatt</bibl> an, dem er gewiß sehr wohl anstehen würde. 
                    Die Sache ist übrigens so beschaffen, daß niemand es übel gethan 
                    finden wird, wenn so unberufenen Schwätzern auf humane Art das Maul 
                    gestopft wird.</p>
            </div>
            <div> 
	            <p>Leben Sie recht wohl; empfehlen Sie mich schönstens Ihrer 
                    Frau <persname ref="3742">Gmalin</persname>, und erhalten Sie die alte 
                    Freundschaft<lb></lb>
                    Ihrem<lb></lb>
                    Ganz ergebensten</p>
	            <p>Schelling.</p>
            </div>
		</body>
	</text>
</TEI>