<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 3562</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/3562">https://schelling.badw.de/text/3562</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/3562">https://schelling.badw.de/doc/3562</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p><placename ref="21">München</placename> den 
                    <date when="1812-10-28">28. Oct˖<expan>ober</expan> 12</date>.</p>
            </div>
            <div>
                <p>Noch immer muß ich schreiben und werde es noch eine Zeitlang müssen.
                    Was wir neulich fürchteten, liebste Mutter, war nur zu wahr; noch 
                    an dem nämlichen <date when="1812-10-24">Tag</date> verloren 
                    wir unsre süßeste Hoffnung. Rettung war nicht möglich und es bleibt 
                    mir nichts als die traurige Pflicht Ihnen diese herzerschütternde 
                    Nachricht zu geben. – Ich beeile mich, bis zur ausführlichen 
                    Erzählung nur mit zwey Worten beyzufügen, daß <persname ref="223">Pauline</persname> ohne Gefahr ist, daß sie nicht zu viele Schmerzen 
                    ausgestanden, daß sie mehr Fassung bewiesen als ich erwartete.</p>
                <p>Liebste Mutter, es ist wahrlich nicht meine Schuld, daß ich Sie mit 
                    dieser schmerzhaften Nachricht in <hi rend="u">dem</hi> Grad überrasche; ich 
                    selbst war es in noch höherem von dem unglücklichen Ereigniß, das 
                    Herr <persname ref="4049">Fischer</persname> mir als gänzlich nun 
                    beseitigt und nicht mehr zu erwarten vorgestellt hatte, nachdem am 
                    <date when="1812-10-23">Tage vorher</date> <unclear cert="low">##</unclear>schmerzen durch ein Lavement gänzlich waren gehoben worden. <date when="1812-10-23">Freytag Abends</date> waren wir ganz 
                    vergnügt, indem alles auf Blähungen in den Gedärmen geschoben war; 
                    die Nacht und der folgende Vormittag (<date when="1812-10-24">
                    Samstag</date>), an dem ich den letzten <bibl sameas="3558" type="document">Brief
                    </bibl> schrieb, war ruhig; Pauline guter Dinge, voll Hoffnung und 
                    munterer Laune, aber Nachmittags 3 1/2 Uhr kamen langsam neue 
                    Schmerzen, die sich endlich in wahre <pb source="1v"></pb>Wehen verwandelten. 
                    Fischer machte noch seine Verordnungen; eine geschickte Hebamme war 
                    anwesend; Abends 8 1/2 Uhr war alles beendigt. Das Kind, ein schöner 
                    etwas über 3 Monat alter Knabe, schon ganz formirt, und für die Zeit 
                    sehr groß und lebenskräftig, war schon 3 Stunden früher ausgetrieben
                    worden.</p>
                <p>Dieß die kurze Geschichte der traurigen Katastrophe, welche uns die 
                    Vergangenheit vernichtet hat und die Zukunft raubte; das Kränkendste, 
                    was meinem Gefühl widerfahren konnte, höchst schmerzlich für <persname ref="223">Pauline</persname>, 
                    und eine, der Himmel weiß wie lange daurende Störung unsres Glücks.</p>
                <p>Es ist so natürlich, nach dergleichen Ereignissen sich um die Ursachen 
                    zu erkunden. – Hier was ich davon weiß! – Ich kann nicht umhin, zu 
                    glauben, daß die unselige Reise nach <placename ref="156">Nürnberg</placename> 
                    den Grund dazu gelegt, die erste Disposition hervorgebracht. Es war 
                    nicht <hi rend="u">meine</hi> Absicht, <persname ref="223">P˖<expan>auline</expan></persname> mitzunehmen; ich bat sie erst,
                    hier zu bleiben und schlug ihr vor, so lange Mme. <persname ref="275">Köhler</persname> zu sich zu nehmen; sie wollte das nicht; ich frage <persname ref="4049">Fischer</persname> 
                    in der Erwartung, daß er es abschlüge: zu meiner größten Verwunderung 
                    findet er die Reise nicht allein unschädlich, sondern sogar nüzlich 
                    und erwünscht; jetzt war kein Rath mehr; gegen mein Gefühl und ganze 
                    Ansicht der Sache <hi rend="u">mußte</hi> ich mich entschließen, P˖<expan>auline</expan> 
                    mitzunehmen. <pb source="2r"></pb>Vielleicht hätte ich eher alles 
                    aufgeboten, die Reise oder wenigstens den Auftrag an mich rückgängig 
                    zu machen, wenn nicht die eigne Meynung und die Versicherung andrer 
                    mich zu der Vorstellung gebracht hätten, daß in dieser Hinsicht für 
                    Paulinen weniger zu fürchten wäre. Die Erschütterungen des Fahrens, 
                    (dreymal, des Tags 24–26 Stunden), die Erhitzungen und Erkältungen, 
                    die beständige Gêne des Erbrechens in dem nämlichen Wagen mit <persname ref="500">Langer</persname> – Ursachen genug, der guten Frau zu schaden! 
                    <persname ref="692">Marcus</persname> war der nämlichen Meynung; nur hätte 
                    er sie nicht vor P˖<expan>auline</expan> äußern sollen, die dadurch auf dem Rückweg 
                    sehr apprehensiv geworden war. – Seit der unglücklichen Reise nahm ich 
                    eine deutliche Veränderung wahr; sie verlor ihr blühendes Aussehen, 
                    das sie noch in <placename ref="366">Augsburg</placename> gehabt hatte, das 
                    Gesicht wurde länglich; es zeigten sich von Zeit zu Zeit Durchfälle, 
                    die jedoch bald wieder nachließen. Selbst der Umzug in die <placename ref="21">Stadt</placename>, da er für sie mit keinen großen Anstrengungen 
                    verknüpft war, machte den Zustand nicht schlimmer; aber jetzt galt es 
                    die ersten Einrichtungen; es war aus der besten Absicht, dem edelsten 
                    Trieb, daß unsre gute Pauline alles <pb source="2v"></pb>selbst anordnen 
                    wollte; aber leider war jetzt nicht die Zeit dazu; alle Ermahnungen 
                    und Bitten noch am letzten Tag vor dem ersten Anfall, da ich wohl 
                    <hi rend="u">dreymal</hi> des Morgens nur in der Küche war, sie zurückzuholen, 
                    halfen nichts; und ihre durch das Übelseyn ohnedieß empfindliche 
                    Laune erlaubte kein ernstes strenges Wort. So, kann ich sagen, bin 
                    ich gleichsam mit sehenden Augen und unter beständigen Kämpfen 
                    dagegen doch in das Unglück hineingerathen. – Am Freytag den 
                    <date when="1812-10-16">16.</date> früh kam der Schrecken über 
                    den Tod meines guten <persname ref="191">Vaters</persname>, den sie 
                    unvermuthet durch Anblick eines schwarzen Sigels auf <persname ref="212">Karls</persname> <bibl sameas="7288" type="document">Briefe</bibl> vor Augen sah. 
                    Sonnabend den <date when="1812-10-17">17ten</date> kam heftiges 
                    Kopfweh; in der Nacht auf den <date when="1812-10-18">Sonntag
                    </date> der erste Blutabgang, der abwechselnd aufhörte und wiederkam
                    bis zum Ende. In dieser Zeit ist gewiß nichts versäumet worden. 
                    Pauline kam nicht vom Sopha; wurde täglich ins Bett und aus dem 
                    Bette getragen, das einzige was schaden konnte, waren die nicht 
                    immer abzuhaltenden Besuche. –</p>
                <p>Jetzt ist <persname ref="223">Pauline</persname> freylich noch sehr angegriffen; sie kommt nicht 
                    aus dem Bett; und schont sich in jeder Hinsicht, der Blutverlust 
                    war verhältnißmäßig nicht sehr stark; desto größer ist der Andrang 
                    der Milch, wovon sie leidet; man sieht, wie sehr die Natur sie für 
                    die Mütterlichkeit bestimmt. Es ist doppelt schmerzhaft, bey so 
                    guten <pb source="3r"></pb>glücklichen Anlagen auf so erbärmliche Art 
                    um seine erste Hoffnung zu kommen. Im Grunde kann dieß doch <hi rend="u">nie</hi>
                    ersetzt werden. Bedauren Sie uns, liebe Mutter, es ist ein 
                    eignes Verhängniß, was wir in der Zeit von 4 Monaten erfahren mußten.</p>
                <p>Ich sehe ein, daß es mit unsrer bisherigen Lebenseinrichtung nicht 
                    fortgehen kann. War <persname ref="223">P˖<expan>auline</expan></persname> nicht so allein, so verlangte sie 
                    die Reise nicht mitzumachen; oder schonte sich doch wenigstens die 
                    ersten 8 Tage in der <placename ref="21">Stadt</placename>. Der <date from="1812-12" to="1813-02">
                    Winter</date> wird traurig für uns beyde seyn, ich <hi rend="u">muß</hi> den 
                    größten Theil des Tags arbeiten; Pauline wird allein sitzen; auch 
                    die Abende, wenn wir beysammen sind, werden wir uns einsam vorkommen. 
                    Ginge es auf den Frühling 
                    zu, so würde ich keinen Anstand nehmen, Paulinen, so bald sie sich 
                    erholt, nach <placename ref="57">Gotha</placename> zu bringen und sie den 
                    ganzen Sommer da zu lassen.
                    So ist es unmöglich. Pauline fragt, ob sich denn keines von den 
                    ihrigen entschließen wolle, unter diesen Umständen hieherzukommen 
                    und den Winter bey ihr zuzubringen. Das Liebste wäre ihr die Mutter.
                    P˖<expan>auline</expan> meynt, so wie sie <persname ref="1148">Schäzlers</persname><persname ref="4579"></persname> 
                    kenne, würden diese nichts dagegen haben, wenn Mutter ihnen die 
                    Umstände schriebe, daß <persname ref="4366">Rickchen</persname> auf ein 
                    paar Monate <persname ref="1149">Julchen</persname> übergeben würde. – 
                    Was mich betrifft, so wünsche ich die Hierherkunft der lieben 
                    Mutter oder Julchens ebenso lebhaft.</p>
                <pb source="3v"></pb><p><persname ref="223">Pauline</persname> hatte Pflege wahrlich <hi rend="u">nöthiger</hi> in 
                    der Zeit ihrer Schwangerschaft, als sie es in den Wochen gehabt 
                    haben würde. Nur unter dem heftigsten Widerstreben ihrer ganzen 
                    Natur konnte das arme Kind sich festsetzen; meine Gegenwart wirkte 
                    statt zu beruhigen nur größeren Aufruhr. Es war zwischen uns ein 
                    Rapport entstanden, den ich magnetisch nennen muß in Ansehung der 
                    Wirkung selbst in die Ferne; dieser Rapport war für Paulinen nicht 
                    vortheilhaft; die Gegenwart eines Dritten war beruhigend und 
                    erleichternd. Sie können sich vorstellen, was sie gelitten hat, da 
                    sie so viel mit mir allein seyn mußte. Auch in andrer Hinsicht 
                    wünsche ich sehr, daß jemand von ihrer Familie um sie seyn möge. 
                    In Sachen der Diät wird es dem Gemahl erstaunend schwer auf die 
                    Frau zu wirken; sie sieht seine Foderungen als lästige Wirkungen 
                    einer häuslichen Tyranney an, so lange sie nicht durch Erfahrung 
                    belehrt ist. In manchen Fällen hatte ich bis jetzt selbst keine 
                    Erfahrung, und konnte also um so weniger Glauben fodern. Es ist 
                    für mich eine äußerst schwierige Sache, meine Pflicht in der 
                    Hinsicht zu erfüllen, und der Verantwortlichkeit, die ich 
                    gewissermaßen für sie übernommen, Genüge <pb source="4r"></pb>zu 
                    thun. Die Gegenwart irgend einer Person ihrer Familie, vorzüglich 
                    aber der lieben Mutter würde mich ganz ungemein beruhigen. 
                    Doch was rede ich von mir? – Die arme Pauline ist allein zu 
                    beklagen. Sie fühlt ihren Verlust jetzt noch nicht einmal recht, 
                    weil sie zu matt ist; allein das wird ihr alles noch sehr nahe 
                    gehen. Die Gesellschaft, die ich ihr leiste, tröstet sie nicht; 
                    wir sind uns <hi rend="u">zu</hi> nahe und jeder Augenblick wird ihr durch 
                    den Gedanken gestört, daß ich auf ganz andre Art beschäftigt seyn 
                    sollte.</p>
                <p>Lassen Sie mich nun, liebste Mutter, Ihren Entschluß wissen; – 
                    einrichten ließe sich alles; ob Sie übrigens wollen, hängt von 
                    Ihnen ab; ich habe für Pflicht gehalten, Ihnen alles zu sagen wie 
                    es ist. Ich hätte Ihnen gern einen gefaßteren Brief geschrieben; 
                    es war nicht möglich, ich leide zu sehr; der Vorfall thut mir zu 
                    <hi rend="u">weh</hi> im eigentlichen Verstande; ich muß mir außerdem so 
                    viel Gewalt anthun. –</p>
                <p><persname ref="223">Pauline</persname> bittet, die liebe Mutter oder <persname ref="1149">Julchen</persname> wolle doch an <persname ref="1203">Gretchen</persname> schreiben, da sie es sobald nicht 
                    können würde. Ferner statt 1 ## Wolle 2 ## schicken. Pauline hat 
                    Ihnen eine Addresse nach <placename ref="156">Nürnberg</placename> für die Kiste geschickt. Haben 
                    Sie nur die Güte, die Kiste unmittelbar <placename ref="21">hieher</placename> <hi rend="u">an Frau</hi> <pb source="4v"></pb><hi rend="u">Direktorin Schelling, vor dem Karlsthor links 
                    No. 9</hi> zu addressiren; wir kommen auf diese Art besser durch. 
                    Was <hi rend="u">neue</hi> Waaren sind muß soviel möglich beyseitgepakt 
                    werden, was <hi rend="u">gemacht</hi> ist, gilt schon nicht für neu.</p><milestone unit="section"></milestone>
            </div>
            <div>
                <p>Die herzlichsten Grüße von <persname ref="223">Pauline</persname>. Sie dürfen, wie 
                    es jetzt steht nichts für sie fürchten. So scheint es mir; so 
                    wird es mir versichert. Versäumt wird nichts; sie hat 2 sehr gute 
                    Mädchen, die sie bestens bedienen. Ach liebe Mutter, wie weh thut 
                    es mir, Ihnen die 2 Blätter zu schicken; wie gerne schickte ich 
                    bessere! Sie müssen aber doch fort; es ist leider wahr was sie 
                    enthalten. Hab’ ich es zu gerad’ hingesetzt, so halten Sie es zu 
                    gut; mein Kopf ist zu sehr angegriffen um vieles Nachdenkens fähig 
                    zu seyn. Der Himmel erhalte Sie alle gesund und geb’ uns bald 
                    Beruhigung von Ihrer Seite.<lb></lb>
                    Ihr<lb></lb>
                    treuerg˖<expan>ebenster</expan></p>
                <p>F. S.</p>
            </div>
        </body>
	</text>
</TEI>