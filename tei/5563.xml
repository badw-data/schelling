<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 5563</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/5563">https://schelling.badw.de/text/5563</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/5563">https://schelling.badw.de/doc/5563</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p>Liebster Karl!</p>
            </div>
            <div>
                <p>Es hat mich ungemein erfreut, nach so langer Zeit wieder einen <bibl sameas="8396" type="document">Brief</bibl> von Dir zu erhalten, obwohl ich eigentlich nicht 
                    weiß, wer von uns beyden dem anderen Antwort schuldig war. Ich hätte Dir 
                    auch schon früher wieder geschrieben, wenn ich nicht so eben erst von 
                    einer kleinen Krankheit genäse, einem rheumatisch-gastrischen Fieber, 
                    mit dem wieder eine Halsentzündung verknüpft war. Dieses Übel hat mich 
                    an die 3 Wochen Zeit gekostet, doch bin ich leidlich, ohne sonderliche 
                    Nachwehen davon gekommen. Äußerst betrübt sind für uns die Nachrichten 
                    von dem Befinden unseres guten <persname ref="40">Schwagers</persname>; wir 
                    hoffen immer noch das Bessere, so ungünstig die Witterung, besonders für 
                    solche Zustände, ist.</p>
                <p>Du siehest es diesem Brief wohl an, daß er schon bald <date when="1816-12-18">vor 3 Wochen</date> 
                    angefangen worden. Einmal daran gestört, konnte ich nicht wieder dazu 
                    kommen, ihn zu vollenden. Ich hatte nämlich eine Arbeit auf dem Gewissen, 
                    die ich übernommen hatte, nämlich einen <bibl sameas="2314" type="literature">Bericht</bibl><bibl n="Bd. I 19. S. 315–404" sameas="6347" type="literature"></bibl> 
                    des Malers <persname ref="169">Wagner</persname> über die unsrem <persname ref="444">Kronprinzen</persname> gehörigen <placename ref="1585">äginetischen
                    </placename> Kunstwerke zu redigiren und für den Druck zuzubereiten, welches 
                    ohne Zusätze und Anmerkungen von meiner Hand nicht wohl abgehen konnte. 
                    Da hab’ ich denn gleich nach der Krankheit mich drüber gemacht und nach 
                    meiner Gewohnheit unabgesetzt und in Einem Feuer dran fortgearbeitet, 
                    daß ich mit <date when="1817-01-01">Neujahr</date> fertig war. Diese 
                    Kunstwerke sind von der höchsten Merkwürdigkeit, ja, wenn man <hi rend="u">darauf
                    </hi> und nicht auf künstlerische Vollendung sieht, bey weitem 
                    merkwürdiger, als die jetzt nach <placename ref="19">London</placename> gekommnen 
                    <pb source="1v"></pb>sogenannten <persname ref="4916">Elgin</persname>’schen. Ich 
                    habe ihnen darum auch einen besondern Fleiß gewidmet, und glaube etwas 
                    geleistet zu haben, das meines Namens Gedächtniß auch in der 
                    Kunst-Geschichte erhalten soll. Ich hoffe, <persname ref="246">Cotta</persname> wird den Druck gehörig beschleunigen, das M<expan>anu</expan>sc<expan>rip</expan>t˖ ist schon 
                    <bibl sameas="3903" type="document">abgegangen</bibl>; kannst Du ihm noch 
                    <hi rend="aq"><foreign xml:lang="la">calcar addere</foreign></hi>, so versäum’ es nicht.</p>
                <p>So wenig man auch auf Zeitungs-Lobsprüche geben mag, so sprechen doch die 
                    Thatsachen entschieden für den neuen <persname ref="417">König von 
                    Wirtemberg</persname>. So viel sieht man, daß er einen 
                    festen Willen für Gerechtigkeit und überhaupt für ein moralischeres 
                    Regiment hat, als bisher in der Welt geführt worden. Um so weniger freut 
                    mich, was man aus den Zeitungen von den Entwürfen für die wissenschaftlichen 
                    Anstalten hört. Ist es denn wahr, daß die katholische Universität nach 
                    <placename ref="16">Tübingen</placename> soll verlegt werden? Ich habe Gelegenheit 
                    gehabt, die Folgen dieser Vermischungen zu sehen und mich durch 
                    Erfahrung zu überzeugen, daß sie höchst nachtheilig für beyde Theile 
                    sind. Das wäre, meines Erachtens, doch ein Punct, wogegen die Landstände 
                    Einsprache thun müßten. Die Katholiken werden es am wenigsten zufrieden 
                    sein; denn es ist vorauszusehen, daß sie in Tübingen nur die <hi rend="aq"><foreign xml:lang="la">Ecclesia pressa</foreign></hi> sein werden. Die 
                    wahre Toleranz besteht im <hi rend="aq"><foreign xml:lang="la">Suum cuique</foreign></hi>. Auch daß man den besten Professor an der <placename ref="1156">Universität</placename> in 
                    die <placename ref="7">Hauptstadt</placename> ziehen will (<persname ref="248">Kielmeyer</persname>) leuchtet 
                    mir gar nicht ein, so wenig als das Lyceum, mit dem, wie Du schreibst, 
                    die Klosterschulen combinirt werden sollen. Gedenkt man denn diese 
                    aufzuheben? Wie sich die Unterrichtsanstalten in <placename ref="1009">Wirtemberg</placename> einmal 
                    bewährt haben, kann jede Veränderung, die das Wesentliche betrifft und 
                    nicht bloß dahin abzweckt, sie in derselben Form und Art zu 
                    vervollkommnen und zu steigern, nur zum größten Schaden gereichen.</p>
                <pb source="2r"></pb><p>Eine eigentliche Akademie der Künste will mir auch, nach 
                    den dortigen Verhältnissen, nicht einleuchten. Erstens fragt sich’s 
                    <hi rend="aq"><foreign xml:lang="la">cui bono</foreign></hi>? Bis jetzt hat 
                    <placename ref="1009">Wirtemberg</placename> an seinem Einen <persname ref="781">Bildhauer</persname> genug 
                    gehabt und diesen nicht einmal beschäftigt. Was soll denn aus zehn, 
                    zwanzigen werden, die sich bilden? Und dieß ist nicht zu vermeiden. 
                    Gründet der Staat Lehranstalten, so nimmt er auch gewißermaßen die 
                    Verbindlichkeit auf sich, die, welche sie benutzen, zu versorgen. Bisher 
                    hat es in Wirtemberg auch an Malern nicht gefehlt, so weit man sie 
                    brauchte, noch an Baumeistern. Freylich, kann man einwenden, würde 
                    Wirtemberg auch den <persname ref="781"><hi rend="u">Dannecker</hi></persname> nicht 
                    haben, wäre nicht die <placename ref="1380">KarlsSchule</placename> gewesen. – 
                    Wohl! also müßte man Mittel schaffen, daß auch wieder sich einer bilden 
                    könnte; aber eine Akademie? Meine Meynung ist, daß, wenn man die Abgüsse 
                    vermehrte und in ein Local brächte, wo sie gut beleuchtet wären und 
                    junge Leute darnach studiren könnten; wenn ferner in demselben Gebäude 
                    ein Saal eingerichtet würde, wo man im Winter täglich Abends nach der 
                    Natur studiren könnte, (ein Zimmer auch etwa für das Studium bey Tag 
                    im Sommer), und man ernennte einige Mitglieder, die abwechselnd die 
                    Aufsicht bey diesem Studium hätten, überließe übrigens jedem 
                    Studirenden die Wahl seines Lehrers, daß es ein völlig freyes Institut 
                    wäre, das jedermann benutzen und nach eigner Wahl, Neigung und Trieb 
                    besuchen könnte: so wäre gewiß das Erfoderliche geschehen, damit die 
                    Kunst in Wirtemberg nicht ganz ausstärbe. – Was dagegen sehr nothwendig 
                    und nützlich schiene, wäre die Einrichtung einer Bauschule, in der alle 
                    zur Baukunst gehörigen Theile und Wissenschaften genau, theoretisch 
                    sowohl als, so weit möglich, practisch gelehrt würden. Diese könnte mit 
                    dem andern Institut in Verbindung stehen, müßte aber doch eine davon 
                    unabhängige innere Einrichtung haben. – Ich habe dieß geschrieben, weil 
                    Du mich um meine Gedanken über diesen Gegenstand ersucht hast. Etwas 
                    anders ist es allerdings in <placename ref="1061">Baiern</placename>. Schon darum, 
                    weil dieß zweymal so groß ist. Dann wegen der vortrefflichen 
                    Kunstschätze, die man jetzt um alles Geld nicht mehr  <pb source="2v"></pb>zusammenbringen könnte. Deßwegen müßte auch jene Lehranstalt in 
                    Wirtemberg nur dahin gehen, den Schüler bis zu dem Grad von Fähigkeit 
                    zu bringen, wo er selbst studiren kann, und das Beste dabey müßten 
                    reichliche Stiftungen thun, von denen man die geschicktesten jungen 
                    Leute unterstützte, die eigentliche hohe Schule der Künste, <placename ref="99">Rom</placename>, zu besuchen, nicht auf 1 oder 2 sondern wenigstens 
                    auf 5–6 Jahre. Lächerlich wäre, in <placename ref="7">Stuttg˖<expan>art</expan></placename> 
                    eine vollendete und vollendende, oder überhaupt mehr als eine 
                    <hi rend="u">VorbereitungsAnstalt</hi> für die Künste haben zu wollen.</p>
            </div>
            <div>
                <p>Ich habe Dir so weitläuftig über diesen Gegenstand 
                    geschrieben, daß ich nun nichts mehr beysetzen kann, als 
                    unsre herzlichen Glückwünsche zum neuen Jahr für Dich und Deine 
                    liebe <persname ref="211">Frau</persname>. Sie und Dich bitten wir, 
                    uns in freundlichem Andenken zu behalten. Schreibe mir doch auch öfter 
                    und lasse mich einigermaßen ersehen, was im Wissenschaftlichen pp bey 
                    euch vorgeht. Lebe recht wohl.<lb></lb>
                    Dein<lb></lb>
                    tr˖<expan>euer</expan> Br˖<expan>uder</expan></p>
                <p>Fr.</p>
                <p><placename ref="21">M˖<expan>ünchen</expan></placename> <date when="1817 -01-07">7. Jänner 17</date>.</p>
            </div>
            <div>
                <p>N.S.</p>
                <p>Da mein obenerwähntes <bibl sameas="2314" type="literature">Büchlein</bibl><bibl n="Bd. I 19. S. 315–404" sameas="6347" type="literature"></bibl> bald fertig seyn kann, so bitte ich Dich 
                    sehr (vergiß es aber nicht) mir zu schreiben, <hi rend="u">wem</hi> ich etwa in 
                    <placename ref="7">Stuttg˖<expan>art</expan></placename> 1 Ex˖<expan>emplar</expan> davon verehren sollte? Ob auch <persname ref="259">Wangenheim</persname> und ihm dazu schreiben oder nicht schreiben? 
                    Vielleicht, der großen Merkwürdigkeit halber, dem <persname ref="417">König</persname> oder Eurer 
                    geistreichen <persname ref="3782">Königin</persname>? Schreibe mir hierüber 
                    nach dem Stand der Dinge. – Meine Lage ist <placename ref="21">hier</placename> nicht grade die beste, 
                    noch habe ich das Versprochene nicht erhalten und werde, wie es scheint,
                    von Tag zu Tag damit hingezogen. Dennoch möchte ich um keinen Preis 
                    in der Welt dort den Schein haben, als ob ich das Geringste für mich 
                    <hi rend="u">suchte</hi>. Da man nun grade mit solchen Projecten schwanger geht, 
                    habe ich mich wohl in Acht zu nehmen. Du kannst mir aber dieß Alles 
                    genau schreiben, da der König das Geheimniß der Posten so bestimmt 
                    ausgesprochen, daß man nicht zweifeln kann, es werde auch gehalten. – 
                    Schreibe mir doch auch über <persname ref="246">Cotta</persname>’s Verhältnisse, die mir mehr als 
                    zweydeutig scheinen; und besonders wie er mit Wangenheim steht.</p>
                <p>Seit einigen Tagen ist unser <persname ref="444">KronPrinz</persname> wieder 
                    außer Gefahr. Er hatte die heftigste Lungenentzündung; in Abwesenheit 
                    des Leibmedicus <persname ref="4893">Harz</persname> hat ihn 
                    Dr. <persname ref="4940">Loë</persname> behandelt, den Du bey 
                    <persname ref="213">Breyer</persname> gesehen hast und der seit Kurzem 
                    zweyter Leibarzt ist. – Man hat ihn oftmal zu Ader gelassen. – Mit Ihm 
                    hätten wir viel verloren.</p>
            </div>
		</body>
	</text>
</TEI>