<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 5948</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/5948">https://schelling.badw.de/text/5948</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/5948">https://schelling.badw.de/doc/5948</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p><placename ref="128">Erlangen</placename> <date when="1826-11-08">8 Nov˖<expan>ember</expan> 1826</date>.</p>
                <p>Hochverehrter Herr Doctor!</p>
            </div>
            <div>
                <p>Nächst der Freude, längere Zeit Ihres erhebenden und belehrenden Umgangs in <placename ref="187">Carlsbad</placename> 
                    genossen zu haben, konnte mir nichts Angenehmeres widerfahren, als ein so willkommnes Zeichen Ihres 
                    gütigen Andenkens zu erhalten, wie Ihr <bibl sameas="2477" type="document">Brief</bibl> vom <date when="1826-09-26">26. 
                    Sept˖<expan>ember</expan></date> und die ihn begleitenden <bibl sameas="16697" type="literature">Schriften</bibl><bibl sameas="16696" type="literature"></bibl> 
                    über die Hypsistarier mir gewesen. Leider habe ich im vergangenen <date when="1826-10">Monat</date> 
                    unter fortdaurenden Abhaltungen und unbeliebigen Zerstreuungen nicht die Zeit gefunden, diese beyden 
                    so gelehrten und scharfsinnig gedachten Schriften mit der gehörigen Überlegung zu lesen, und so blieb denn 
                    auch meine Antwort verschoben, weil ich um nicht mit bloßen allgemeinen Worten zu danken, auch über den 
                    Inhalt so interessanter Schriften Ihnen zu schreiben wünschte.</p>
                <p>Denn obgleich, was die Hauptsache betrifft – die ursprüngliche Existenz eines der eigentlichen Vielgötterey 
                    vorausgegangnen, aber doch mit Sabäismus versetzten Monotheismus – ich damit ganz einverstanden bin, ja 
                    dieß selbst die <hi rend="aq"><foreign xml:lang="la">ipsissima verba</foreign></hi> eines schon vor <date from="1821-08" to="1821-09">4 
                    Jahren</date> von mir gehaltnen öffentlichen Vortrags <bibl sameas="???" type="document"><hi rend="u">über die Bedeutung der Mythol˖<expan>ogie</expan></hi></bibl> 
                    sind: so fand ich doch in der Anwendung dieser Idee auf die Hypsistarier verschiedne Bedenken, insbesondre kann 
                    ich nicht glauben, daß die Werke <bibl n="S. 9" sameas="16697" type="literature"><foreign xml:lang="el">ὑπ’ειδωλοις κ.τ.λ.</foreign></bibl> 
                    p. 9. der lat˖<expan>einischen</expan> Abh˖<expan>andlung</expan> mit Herrn <persname ref="3758"><hi rend="u">Böhmer</hi></persname> auf die Hypsistarier 
                    <hi rend="u">als solche</hi> bezogen werden können. Denn 1) im Zusammenhang mit der Erzählung von der Mutter 
                    (<bibl n="S. 8" sameas="16696" type="literature">S. 8.</bibl> der deutschen Schrift) kann ich in den Worten <foreign xml:lang="el">ὑπ’εἰδώλοις 
                    πάρος ἦεν ζώων</foreign>, nur die Erwähnung eines <hi rend="u">äußern</hi> Umstandes (mit <persname ref="3759">Ullmann</persname>,
                    dessen Argumentation l.c. ich <hi rend="u">in soweit</hi> ganz beystimme), und nicht eine Andeutung seines specifischen 
                    oder individuellen Glaubens sondern nur der <hi rend="u">allgemeinen</hi> Categorie finden, nämlich daß er im Heydenthum 
                    geboren worden und als Heyde gelebt habe, wie wir von <persname ref="3760">Arnold von Brescia</persname> und andern 
                    frühern Zeugen der Wahrheit sagen könnten, sie haben im Pabstthum gelebt, und von <persname ref="3436">Johann 
                    Taulerus</persname> er sey ein <hi rend="u">Papist</hi> gewesen, ohne damit im Geringsten seine individuelle Denkweise ausdrücken 
                    zu wollen. Eine ganz falsche Stellung scheint mir (aufrichtig zu sagen) p. 9. der Sache gegeben, wenn es heißt: 
                    <q><hi lang="la" rend="aq">Ex quo cognoscendum, eundem, <hi rend="u">quum Hypsistariorum numero esset adhuc adscriptus</hi>, numina 
                    statuisse fictitia</hi><bibl n="S. 9" sameas="16697" type="literature"></bibl></q>. Grad’ umgekehrt, so lang er Heyde war, war er 
                    Hypsistarier, er hörte erst auf, Hypsis˖<expan>tarier</expan> zu sein als er Christ wurde. – 2) Sollte der Ausdruck: 
                    <foreign xml:lang="el">ὑπ’εἰδώλ˖<expan>οις</expan> κ.τ.λ.</foreign> <pb source="1v"></pb>auf den Glauben des Manns als <hi rend="u">Hypsistarier</hi> 
                    gehen, so wäre der Ausdruck gewiß unangemessen und nach Herrn B<expan>öhmer</expan>’s eigner Vorstellung von den H˖<expan>ypsistarier</expan> 
                    zu stark: <foreign xml:lang="el">ὑπ’εἰδ˖<expan>ώλοις</expan> ζῆν</foreign> = <foreign xml:lang="el">δουλεύειν τοῖς εἰδ˖<expan>ώλοις</expan></foreign>. Für 
                    eine so bedingte Anerkennung ohne allen Cultus wäre nie das <foreign xml:lang="el">ὑπό</foreign> pp gebraucht worden. Daß 
                    von gottähnlichen, aber <hi rend="u">durchaus nicht</hi> <hi rend="uu">verehrten</hi> <hi rend="u">Wesen</hi> je der Name 
                    <foreign xml:lang="el">εἰδ.</foreign> gebraucht worden, erlaube ich mir um so mehr zu bezweifeln, als dieß eine rein 
                    theoretische Vorstellung (ohne alle practische Bedeutung) ist, welche ja auch im <bibl resp="AT" type="bibel">A˖<expan>lten</expan> T˖<expan>estament</expan></bibl> herrscht, 
                    wie der Gebrauch des Worts <foreign xml:lang="he">###</foreign> von den Engeln zeigt – noch mehr unter den Juden; denn 
                    was fehlt z.B. dem <persname ref="3761"><hi rend="u">Metatron</hi></persname> zu einem untergeordneten gottähnlichen Wesen? – 
                    3) Endlich wenn <foreign xml:lang="el">εἰδ.</foreign>, wie Herr B˖<expan>öhmer</expan> will, <hi rend="aq"><foreign xml:lang="la">numina fictitia</foreign></hi> 
                    überhaupt bedeutet, so heißt in der Hauptstelle p. 6. <bibl n="S. 6" sameas="16697" type="literature"><foreign xml:lang="el">τὰ εἴδ˖<expan>ωλα</expan> 
                    ἀποπεμπόμενοι</foreign></bibl>, <hi rend="aq"><foreign xml:lang="la">numina fictitia</foreign></hi> (nicht <hi lang="la" rend="aq"><hi rend="u">adorationem</hi> 
                    numinum fict˖<expan>itiorum</expan></hi>) <hi rend="aq"><foreign xml:lang="la">rejicientes</foreign></hi>, und unmöglich ist dann, daß von eben 
                    denselben, als solchen, gesagt werde, sie haben <foreign xml:lang="el">ὑπ’ειδωλοις</foreign> gelebt. Hätten sie außer 
                    dem <foreign xml:lang="el">ὕψιστος</foreign> andre Götter gelehrt, so würde da, wo jetzt 
                    <bibl n="S. 6" sameas="16697" type="literature"><foreign xml:lang="el">τιμῶσι τὰ πῦρ καὶ τὰ λύχνα</foreign></bibl> steht, statt des bloßen 
                    <hi rend="aq"><foreign xml:lang="la">usus ignis et lucis in cultu sacro</foreign></hi>, vielmehr jenes erwähnt sein. Mit weit größerer 
                    Sicherheit als aus dem <foreign xml:lang="el">ὑπ’εἰδ˖<expan>ώλοις</expan> ζῆν</foreign> auf die Anerkennung könnte hieraus vielmehr 
                    auf die Nichtanerkennung von andern gottartigen Wesen geschlossen werden. Es gäbe, meines Ermessens, nur Ein 
                    Mittel, den Widerspruch hinwegzuschaffen, nämlich: <foreign xml:lang="el">τὰ εἴδωλα</foreign> in seinem genaueren Sinn 
                    zu nehmen, wo es nicht <hi rend="aq"><foreign xml:lang="la">numina fictitia</foreign></hi> überhaupt, sondern bloß <hi rend="u">bildlich verehrte</hi> 
                    (also freylich auch nicht die bloßen Bilder) – oder mit andern Worten: wo es bloß die <hi rend="u">concreten</hi> nicht 
                    aber auch die <hi rend="u">allgemeinen Götter</hi> (Elemente, Sterne) bedeutet; ein Sprachgebrauch, der durch die 
                    p. 97. angef˖<expan>ührte</expan> Stelle des <bibl sameas="1738" type="literature">Epiphanius</bibl> nicht widerlegt 
                    wird. Denn wenn er von den Magusäern sagt: <q><foreign xml:lang="el">εἴδωλα μὲν βδελυττόμενοι, εἰδώλοις προσκυνοῦσι, 
                    πυρὶ καὶ σελήνῃ καὶ ἡλίῳ</foreign></q>, so ist dieß vielmehr ein Beweis, daß zwar 
                    <hi rend="u">Epiphanius</hi> zwischen <hi rend="u">wirklichen</hi> Gegenständen, dergleichen z.B. das Feuer ist und bloß <hi rend="u">bildlichen</hi> 
                    (wie die griechischen Götter waren) keinen Unterschied macht, wie denn in Bezug auf die wahre Religion auch 
                    kein Unterschied ist, und <hi rend="u">beyde</hi> für <foreign xml:lang="el">τὰ εἴδ˖<expan>ωλα</expan></foreign> erklärt – aber zugleich, daß 
                    sonst oder anderwärts unter <foreign xml:lang="el">εἰδ˖<expan>ώλοις</expan></foreign> blos Götter der letzten Art verstanden wurden. Wollte 
                    man den Widerspruch zwischen dem: <foreign xml:lang="el">τὰ εἴδ˖<expan>ωλα</expan> ἀποπεμπ˖<expan>όμενοι</expan></foreign> und der Annahme, 
                    daß sie untergeordnete Götter geglaubt, <hi rend="u">auf diese Art</hi> hinwegschaffen, so müßte dann a) bey dem 
                    <foreign xml:lang="el">τιμῶσι</foreign> p doch mehr verstanden werden, als Herr B˖<expan>öhmer</expan> will, nämlich eine wirkliche 
                    dem Feuer und den Lichtern (letztern nicht als <hi rend="u">Abbildungen</hi> und <foreign xml:lang="el">εἰδώλοις</foreign> 
                    sondern als <hi rend="u">reellen Erscheinungen</hi> der himmlischen Lichter) erwiesene – nicht grade Anbetung aber doch 
                    religiöse Verehrung, wie der Katholik das Weihwasser z.B. nicht anbetet aber doch religiös verehrt; b) hätte 
                    man dann doch damit nur erst die <hi rend="u">Freyheit</hi> gewonnen, den Hypsistariern die Anerkennung subalterner 
                    Quasi-Götter zuzuschreiben, aber <pb source="2r"></pb>in dem <foreign xml:lang="el">ὑπ’εἰδώλ˖<expan>οις</expan> ζῆν</foreign> müßte 
                    denn doch das Wort <foreign xml:lang="el">εἴδ˖<expan>ωλα</expan></foreign> <hi rend="u">auch</hi> jene genauere Bedeutung behalten und daher 
                    nicht auf den Glauben der Hypsistarier <hi rend="u">als solcher</hi> bezogen werden. Vergleiche ich aber 4) vollends 
                    die Stelle des nyssenischen <persname ref="3762">Gregorius</persname> <bibl n="S. 10" sameas="16697" type="literature">p. 10</bibl>, so 
                    scheint es mir ganz unmöglich, daß irgend ein Kirchenvater von einer Secte, die gottähnliche Wesen in irgend 
                    einem Sinn, <hi rend="u">der sie noch <foreign xml:lang="el">εἴδωλα</foreign> zu nennen erlaubte</hi>, anerkannte, <hi rend="u">so</hi> gesprochen, daß er von ihr 
                    gesagt hätte, <hi rend="u">was sie</hi> von den Christen <hi rend="u">unterscheide</hi>, sey, daß sie zwar den höchsten Gott <hi rend="u">aber 
                    nicht als Vater</hi> anerkennen; denn dieß würden wir kaum von den Muhammedanern, die sich noch durch ganz 
                    andre Dinge von den Christen unterscheiden, wir würden es aber unbedenklich von den Juden oder den modernen 
                    Theisten sagen. <hi rend="u">Diese</hi> Stelle erhebt die Hyps˖<expan>istarier</expan> in meinen Augen zu dem Range <hi rend="u">reiner Theisten</hi>. 
                    Ich will damit nicht die Möglichkeit ausschließen, daß sie <hi rend="u">in bloß speculativer Beziehung</hi> das Daseyn 
                    untergeordneter gottähnlicher Wesen nicht läugneten und dadurch den Massalianern ähnlich wurden (mehr als das 
                    bloße Läugnen, oder <hi rend="u">gleichgültige Zulassen</hi> drückt das von letzteren gebrauchte: <foreign xml:lang="el">θεοῦς 
                    μὲν (sc. εἶναι) λέγοντες</foreign> nicht aus); nämlich beyde, als eine bloße <foreign xml:lang="el">αἵρεσις</foreign> des 
                    Heydenthums hatten nicht das Interesse, welches die Christen hatten, gegen Gott vergleichbare Wesen <hi rend="u">in jedem 
                    Sinn</hi> zu kämpfen. Wie ganz <hi rend="u">bloß speculativ</hi> – ohne alle religiöse Bedeutung aber dieß bey ihnen war, 
                    erhellt daraus, daß Gregor, wo er das Heydnische in ihnen nachweisen will, nichts andres als das Feuer und die 
                    Lichter anzuführen weiß, die sie auch wohl dem <foreign xml:lang="el">ὕψιστος</foreign> als <hi rend="u">Vater der Lichter</hi> 
                    anzünden konnten. Eben dieß <hi rend="u">bloß</hi> Speculative scheint nicht zu erlauben, sie so fernher, von jenem uralten, 
                    sehr <hi rend="u">reellen</hi> und sehr <hi rend="u">practischen</hi>, Sabäismus abzuleiten, wozu auch die Hauptstelle: 
                    <bibl n="S. 6" sameas="16697" type="literature"><foreign xml:lang="el"> ἐκ δυοῖν – – – συνετέθη</foreign></bibl> keine Veranlassung gibt. Das 
                    einzige zu Gunsten jener entfernten Herleitung wäre die Stelle des <persname ref="3763">Kyrillos</persname> 
                    <bibl n="S. 68" sameas="16697" type="literature">p. 68</bibl> aber ich würde großes Bedenken tragen, derselben eine historische 
                    Geltung zuzugestehn. Denn da er grade die Hauptsache, die von den ältesten <foreign xml:lang="el">θεοσεβεῖς</foreign>, 
                    <persname ref="3765">Jethro</persname>, <persname ref="3764">Melchisedek</persname> u.s.w. angeführt wird, daß sie – 
                    <bibl n="S. 68" sameas="16697" type="literature"><foreign xml:lang="el">τάχα που</foreign></bibl> – auch andre Götter – nämlich die 
                    vornehmsten Schöpfungswerke, Erde, Himmel, Sonne, Mond zu ihm (Gott) gerechnet – von den späteren 
                    <foreign xml:lang="el">θεοσεβεῖς</foreign> nicht wiederholt sondern nur das Allgemeine wieder sagt, was Gregor schon 
                    von den Hypsistariern gesagt hatte, daß sie ein Mittleres zwischen Judenthum und Hellenenthum (eigentlichem 
                    Heydenthum) sey'n, so erhellt daraus bloß, daß Kyrillos, in der Verlegenheit, sie nicht gradezu als Heyden 
                    und doch auch nicht als Juden oder Christen ansprechen zu können, den Ausweg ergreift, sie mit jenem Heydenthum 
                    <hi rend="u">vor dem eigentlichen Heydenthum</hi> zu vergleichen, um ihnen auf diese Art doch einen Platz unter den Heyden 
                    geben zu können. Diese Combination des Kyrillos kann aber uns keine Nothwendigkeit auferlegen, die 
                    <foreign xml:lang="el">θεοσεβεῖς</foreign> grade durch eine <hi rend="u">wirkliche</hi> Abstammung von jener uralterthümlichen 
                    Religionsweise zu erklären, und noch viel weniger, diesen Schluß auf die Hypsistarier auszudehnen, die wir 
                    alle Ursache haben für reine Theisten zu halten. Wie sie aber übrigens eigentlich zu erklären sey'n, 
                    <pb source="2v"></pb>und wie ich mir die Entstehung dieser unter sich analogen Secten, der Hypsistarier, der 
                    Massalianer und der <foreign xml:lang="el">θεοσεβεῖς</foreign> denke, ist theils noch zu unreif, theils würde es hier 
                    zu weit führen. Ich erschrecke ohnedieß, indem ich gewahr werde, wie viel ich Ihrer Geduld schon zugemuthet. 
                    Sehen Sie diese Weitläuftigkeit wenigstens als einen Beweis an, wie wichtig mir alles ist, was von Ihnen 
                    kommt, und als eine Art meinen Dank dafür auszudrücken, daß Sie mich auf diese interessanten Secten 
                    aufmerksam gemacht haben, die sich an einer Stelle meiner demnächst erscheinenden Schrift, wo ich bereits 
                    der <hi rend="aq"><foreign xml:lang="la">Coelicolarum</foreign></hi> des <bibl sameas="16698" type="literature"><hi rend="aq"><foreign xml:lang="la">Cod˖<expan>ex</expan> Theod˖<expan>osianus</expan></foreign></hi></bibl> 
                    erwähnt hatte, wofern es die Zeit erlaubt, noch wohl werden anschließen lassen.</p>
                <p>Bei meiner Zurückkunft fand ich den <bibl n="Bd. 2" sameas="2233" type="literature">2ten Th˖<expan>eil</expan></bibl> Ihrer <hi rend="u">Kirchengeschichte</hi>; diese 
                    wird meine erste Lectüre sein, so wie ich Zeit gewinne, und ich freue mich zum voraus besonders der weitern Entwicklung 
                    Ihrer Ansicht von den Gnostikern und Ihrer Einladung, Ihnen darüber zu schreiben. Ich lebe gegenwärtig leider in großen 
                    äußern und innern Beunruhigungen. Bitten Sie für mich, daß sie bald von mir genommen und ich dadurch fähig werde für die 
                    Vollendung der Arbeit, die wenigstens meiner Intention nach nur Ein Ziel hat – den <hi rend="u">Herrn</hi>.</p>
            </div>
            <div>
                <p>Leben Sie recht wohl, theuerster Herr und Freund, wenn Sie dieß meiner Empfindung für Sie entsprechende Wort 
                    von mir annehmen wollen. Mit inniger Verehrung und herzlicher Liebe<lb></lb>der Ihrige</p>
                <p>Schelling.</p>
            </div>
            <div>
                <p>N.S.</p>
                <p>Die beiden <bibl sameas="16696" type="literature">Schriften</bibl><bibl sameas="16697" type="literature"></bibl>, deren Sie mir zulieb offenbar sich selbst auf einige 
                    Zeit beraubt haben, werde ich mit der ersten Gelegenheit zurücksenden.</p>
            </div>
		</body>
	</text>
</TEI>