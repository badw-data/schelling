<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 1236</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/1236">https://schelling.badw.de/text/1236</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/1236">https://schelling.badw.de/doc/1236</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <p></p>
            </div>
            <div>
                <pb source="1r"></pb><p><placename ref="768">Stockholm</placename> den <date when="1820-02-01">1 Febr˖<expan>uar</expan> 1820</date>.</p>
            </div>
            <div>
                <p>Nun, so sey denn unser himmlischen Våter vieltausendmal gedankt und
                    gelobt, mein theurer Meister und Freund, dass Sie noch unter uns
                    Erdebewohnern auf dieser freilich ziemlich schlechten sublunarischen Welt
                    umherwandeln! Die <placename ref="889">Deutschen</placename> Zeitungen und nach ihnen die <placename ref="888">Schwedischen</placename> haben
                    Ihre hiesigen Freunde eine lange Zeit hindurch mit drohenden Nachrichten
                    von Ihrem fast gewissen Tode geängstigt. Wir betrauerten schon das
                    Schicksal der <bibl sameas="8404" type="document"><hi rend="u">Weltalter</hi></bibl> und der ganzen philosophischen Wissenschaft, über
                    welche wir in diesem schrecklichen Ereigniss ein von Gott für Jahrhunderte
                    ausgesprochenes Todesurtheil sahen. Von meinem persönlichen Gefühl bei
                    der Aussicht eines <hi rend="u">solchen</hi> Verlustes will ich kein Wort sagen. Indessen
                    beruhigte mich über <hi rend="u">diesen</hi> Punct einigermaassen der Gedanke: <hi rend="u">wenn</hi> er
                    wirklich von der Erde entschwunden ist, so kann sein jetziges Loos nur ein
                    schönes und seeliges seyn, und du wirst ihn wohl früh genug wiedersehn,
                    wenn auch <hi rend="u">du</hi> dein Tagewerk vollendet hast — denn wie kurz ist in der
                    That die Spanne Zeit von ein Stück zehn, zwanzig oder dreissig Jahren?</p>
                <p>Aber, Gottlob! besser ist doch besser; wir wollen den vergangnen
                    Schmerz über die gegenwärtige Freude vergessen. Ich glaube jezt sehr fest
                    sogar an die Möglichkeit, dass wir noch diesseits des Grabes uns nicht nur
                    im Geiste umarmen werden. Gewiss komm’ ich noch einmal in mein
                    geliebtes <placename ref="889">Deutschland</placename> zurück. Unterdessen will ich treu für unsre
                    gemeinsame Sache thätig seyn, nach meinem Maass von Wissen, Können und
                    Gewissen.</p>
                <p>Ihr letztes <bibl sameas="4496" type="document">Schreiben</bibl> an mich, datirt <hi rend="u"><placename ref="1132">Franzens-Brunnen</placename> in <placename ref="1232">Böhmen</placename>
                    den <date when="1819-08-19">19 Aug˖<expan>ust</expan> 1819</date></hi>, kam ziemlich spät in meine Hände. Jedoch hätt’ ich es
                    weit früher beantworten können und sollen, wenn ich nicht bei dessen Empfang mit der Ausgabe eines <bibl sameas="16855" type="literature">poetischen Taschenbuchs</bibl> 
                    beschäftigt gewesen wäre, das kurz darauf die Presse verlassen sollte; und bald nachher zerstreute mich eine Lustreise auf's Land in den 
                    <date from="1819-12-24" to="1819-12-26">Weihnachts-Tagen</date>, so wie gleich darauf die Veränderung meines Wohnorts, der jezt auf noch
                    unbestimmte Zeit von <placename ref="190">Upsala</placename> nach <placename ref="768">Stockholm</placename> versetzt geworden ist. Sie haben
                    vielleicht aus dem <bibl sameas="16852" type="literature">Hamburger Correspondenten</bibl> vernommen, dass unser <persname ref="4339">Kronprinz</persname> 
                    in Up<pb source="2r"></pb>sala den <date from="1819-09" to="1819-11">vorigen Herbst</date> zugebracht; dort alle Menschen entzückt
                    (was wirklich <hi rend="u">wahr</hi> ist), Vorlesungen fleissig gehört und besucht, und einen Cursus in der schönen Literatur der <placename ref="889">Deutschen</placename> 
                    unter meiner Leitung angefangen. Dieses Studium setzt er auch <placename ref="768">hier</placename> fort, und hat mich deswegen
                    hieher berufen. Er hat eine gewandte Sprachfähigkeit und viel poetisches Gefühl. Wir haben in kurzer Zeit die meisten 
                    <persname ref="329">Schiller</persname>’schen Trauerspiele durchgelesen, unter denen er <bibl sameas="9139" type="literature"><hi rend="u">Wallenstein</hi></bibl> und 
                    <bibl sameas="7095" type="literature"><hi rend="u">Tell</hi></bibl> am meisten schätzt. Dass er Schiller dem <persname ref="156">Goethe</persname> vorzieht, 
                    dessen <bibl sameas="13374" type="literature"><hi rend="u">Egmont</hi></bibl> er doch sehr lieb gewommen hat, mag man ja leicht einem jungen <hi rend="u">Fürsten</hi> 
                    verzeihen, den natürlicherweise grosse Staats-Actionen, kriegerische Gesinnungen und politische Grundsätze mehr 
                    interessiren als die blosse reine <hi rend="u">Poesie an sich</hi>. Nachher will ich seine Aufmerksamkeit zur Bühne des gewaltigen
                    <persname ref="1856">Shakspeare</persname> hinlenken, und ihm in der schönen <persname ref="391">Schlegel</persname>'schen Übersetzung
                    besonders die <hi rend="u">rein historischen</hi> Trauerspiele mittheilen, die wahre <hi rend="u">Fürstenspiegel</hi> zu nennen sind. Den 
                    <bibl sameas="16853" type="literature"><hi rend="u">Sigurd Schlangentödter</hi></bibl> von <persname ref="3574">Fouqué</persname> will ich
                    ihm auch nächstens vorlesen, weil die zierliche Behandlung, die übrigens dem Stoff nach sich sehr treu an die alte 
                    <placename ref="1161">Skandinavische</placename> Sage anschliesst, <pb source="2v"></pb>mir geeignet scheint ihm lockend die freundlichste 
                    Seite zu zeigen von der heilig-schauerlichen Gemüthstiefe des Landes und des Volkes, die er zu beherrschen bestimmt ist. 
                    Von der Existenz jener Tiefe ahndet man freilich hier in der <placename ref="768">Hauptstadt</placename> sehr wenig, und Sie können 
                    sich leichtlich denken, wie die <hi rend="u">alten Perücken</hi> über mein Verhältniss zum Kronprinzen ihre Köpfe schütteln. 
                    Da der <persname ref="4340">König</persname> aber still schweigt und seinen Sohn lesen und treiben lässt wras er will — 
                    dem Yater ist wohl im Grunde wenig dran gelegen ob man die <placename ref="627">Französischen</placename> Tragiker u.s.w. 
                    respectirt oder nicht, wenn man ihm nur nicht politische Kreuzsprünge macht — so sehen sie die Bekehrung des 
                    Prinzen zur Schwärmerey als entschieden an und damit das läng gefürchtete Jahrhundert der durch mich und meine 
                    Freunde auf die Bahn gebrachten ästhetischen Barbarei und Schellingischen Mystik als leider! angefangen. —</p>
                <p>Man muss dem <persname ref="4339">Prinzen Oscar</persname> übrigens innig gut werden, wenn man ihn näher kennt. 
                    Er ist schön und ritterlich, unschuldig und brav, weich und kräftig, ernst und heiter, alles am rechten Platze. 
                    Er kann fröhlich, ja muthwillig seyn, wie es seine zwanzig Jahre mit sich bringen, aber das ernste und 
                    besonnene Prinzip, das ein<pb source="3r"></pb>em Nordischen Fürsten so wesentlich zu besitzen ist, scheint 
                    immer mehr und mehr in ihm vorherrschend zu werden. <hi rend="aq"><foreign xml:lang="la">In summa</foreign></hi>, wir haben sehr viel 
                    von ihm zu hoffen und erwarten. — Es ist ein eignes und in seiner Art, so viel ich weiss, ganz neues
                    psychologisches Phänomen, diese Transsubstantiation eines jungen <placename ref="627"><hi rend="u">französisch</hi></placename>
                    gebornen Menschen in eine durchaus entgegengesetzte Natur. Die Übersetzung seiner ersten Individualität 
                    in die zweite ist aber so gelungen, dass keiner, der nicht das Gegentheil historisch wusste, aus der Art 
                    seines Sprechens und seines Umgangs vermuthen würde, er wäre nicht mitten in <placename ref="888">Schweden</placename>
                    geboren. Dieses Glück haben wir, nächst Gott, dem eben so ächtschwedischen, als gründlich gebüdeten und 
                    liebenswürdigen Erzieher des Prinzen, dem Canzlei-Rath <persname ref="4341">von Tannström</persname> zu verdanken; 
                    so wie auch, dass er eine tüchtige, freie und constitutionelle politische Denkungsart in seinem Innern
                    entwickelt hat. Geschichte, Geographie und Kriegswissenschaft sind übrigens seine täglichen Hauptstudien; 
                    er ist von der Armee sehr geliebt, und scheint entschlossen, seinen Feinden einmal recht derb auf's Leib 
                    zu gehen.</p>
                <p>So habe ich denn das »<hi rend="aq"><foreign xml:lang="la">a Jove Principium</foreign></hi>« im Au<pb source="3v"></pb>ge habend, Ihnen
                    meinen fürstlichen Gönner und Jünger recht ausführlich geschildert. Es wird Ihnen nicht unlieb seyn zu 
                    hören, dass der Schwedische <persname ref="4339"><hi rend="aq"><foreign xml:lang="la"><hi rend="u">Princeps Juventutis</hi></foreign></hi></persname> das 
                    Deutsche gründlich lernen will. Er <hi rend="u">spricht</hi> es auch schön, obwohl noch sehr ungeübt, und will es jezt 
                    anfangen zu <hi rend="u">schreiben</hi>. — Jezt etwas von mir selbst und meinen hiesigen Freunden. Meine Gesundheit ist
                    noch häufig von allerlei Magenschmerzen, hämorrhoidalischen Verdriesslichkeiten u. dgl. gezwackt und geplagt. 
                    Ich will aber im künftigen <date from="1820-06" to="1820-08">Sommer</date> eine ungeheure Menge Brunnenwasser 
                    verschlucken, wovon mir Freunde und Ärzte grossen Nutzen versprechen. Unterdessen verarbeite ich allmählich
                    allerlei während meiner Reise gesammelten Stoff, und werde wohl bald ein sich darauf beziehendes grösseres 
                    Werk, aus Prosa und Versen vermischt, herausgeben. Über <placename ref="889">Deutschland</placename> muss ich wohl die 
                    meisten von meinen Briefen im Pulte behalten, bis auf eine fernere Zukunft; denn die Gränzlinie zwischen 
                    dem, was ich über die dortige Lage der Dinge und bedeutende persönliche Verhältnisse <hi rend="u">sagen</hi> oder 
                    <hi rend="u">nicht sagen</hi> darf, ist gar fein und schwierig <pb source="4r"></pb>zu ziehen. Ich bin noch über diesen 
                    Punct mit mir selber in grosser Confusion und Uneinigkeit. Einige Gedichte aus diesem beabsichtigten 
                    Reisewerke hab’ ich einem Poetischen Taschenbuch für 1820 einverleibt, unter 
                    dem Nahmen <bibl sameas="16854" type="literature"><hi rend="u">Vandrings-Minnen</hi></bibl> (buchstäblich: Wanderungs-Erinnerungen). 
                    Die Überschriften känn ich Ihnen mittheilen, weil sich nichts weiter mittheilen lässt, und Sie doch ein 
                    so gütigen Antheil an meinem Thun und Treiben nehmen: 1. <hi rend="u">Die neunjährige Dichterin</hi> (An die
                    Frau <persname ref="1055">von Helwig</persname>, veranlasst durch ein kleines Bildniss, wo sie als Kind
                    gezeichnet ist.) 2. <hi rend="u">An Schelling</hi> (das deutsche Original kennen Sie.) 3. <hi rend="u">Tyroler-Lied</hi>. 
                    4. <hi rend="u">Italia</hi>. 5. <hi rend="u"><persname ref="4342">Byström</persname>’s Juno</hi> (eine schöne Gruppe von
                    einem in <placename ref="99">Rom</placename> lebendem <placename ref="888">Schwedischen</placename> Bildhauer.) 6. <hi rend="u">Bettina</hi>. 
                    (Nachklang eines Albaner-Liedes.) 7. <hi rend="u">Die schöne Nonne</hi>. 8. <hi rend="u">Auf der Gebirgshöhe von 
                    <placename ref="1499">Olebano</placename></hi>. 9. <hi rend="u"><placename ref="1011">Sicilianisches</placename> Lied</hi>. 10. <hi rend="u">Der 
                    Besuch in <placename ref="1500">Sorrento</placename></hi>. 11. <hi rend="u">Die Guitarrspielerin auf dem Jahrmarrkte</hi>. 
                    12. <hi rend="u">Abschied von <placename ref="1501">Villa Borghese</placename></hi>. 13. <hi rend="u">Die Gondelfahrt</hi>. 14. 
                    <hi rend="u">In den <placename ref="1290">Kärnthner</placename>-Alpen</hi> (das Deutsche Original hab’ ich Ihnen ja mitgetheilt.) 
                    15. <hi rend="u">An eine Schwester</hi>. 16. <hi rend="u">Das Kinder-Ballett</hi> (eine <placename ref="11">Wienerische</placename> Theaterscene.) 
                    17. <hi rend="u">Einsamkeit</hi>. 18. <hi rend="u">Aussicht</hi>. 19. <hi rend="u">An <persname ref="181">Steffens</persname></hi> (auch zuerst 
                    Deutsch gedichtet, an seinem GeburtsTage.) 20. <hi rend="u">Wandrers Abendlied</hi>. — Unter vielen andern, die zu 
                    diesem Cyclus gehören, aber noch ungedruckt sind weil ihnen die letzte Hand fehlt, werden auch jene
                    <pb source="4v"></pb><note place="left">Wenn Sie den Herrn <persname ref="571">von Baader</persname> treffen, grüssen Sie 
                    Ihn) – Ich will ihm bei Gelegenheit einige neue magische Nachrichten mittheilen.</note>Sonette 
                    zur Ehren der <persname ref="3072">Heil˖<expan>igen</expan> Jungfrau</persname> zum Vorschein kommen,
                    die ich in <placename ref="21">München</placename> <hi rend="u">deutsch</hi> niederschrieb. Auch ein Gedicht zum Lob des
                    <persname ref="1452"><hi rend="u">Albrecht Dürer</hi></persname>, eine Canzone über <hi rend="u"><persname ref="1453">Raphaels</persname> 
                    <persname ref="2953">Cäcilia</persname></hi>, Stanzen über <placename ref="99">Rom</placename> u.s.w. wovon künftig ein 
                    mehreres.*)<note place="foot">*) Auch über den <persname ref="4343"><hi rend="u">Kaiser Julianus</hi></persname> (dem Abtrünnigen) hab’ ich 
                    eine wunderliche Art von <hi rend="u">antiker Romanze</hi> im Sinn, aus vier Abtheilungen in Alcäischem Versmaasse bestehend.</note> 
                    Ich beeile mich um mit einer Menge dergleichen Gegenständen so schnell wie möglich fertig zu werden, weil ich
                    nachher mich <placename ref="888">vaterländischen</placename> Romanzen und dramatischen Bearbeitungen <placename ref="1161">Skandinavischer</placename> 
                    Stoffe fast ausschliessend widmen will. Mein Freund <persname ref="1381"><hi rend="u">Geijer</hi></persname> arbeitet sehr ämsig 
                    in der Schwedischen Geschichte, und hat im <date from="1819-09" to="1819-11">leztverflossnen Herbste</date>, wo der 
                    <persname ref="4339">Kronprinz</persname> täglich seine Vorlesungen besuchte, einen fast göttlich herrlichen Yortrag 
                    über die altnordische Geschichte (von <persname ref="4344">Odin</persname> an bis zum <persname ref="4345">Gustaf Wasa</persname>) 
                    gehalten. In <placename ref="190">Upsala</placename> giebt man jezt eine Zeitschrift <bibl sameas="16856" type="literature"><hi rend="u">Svea</hi></bibl> heraus, 
                    die sehr viel für die Zukunft verspricht. Geijer hat dort eine schöne Abhandlung <bibl n="H. 2. S. 189–312" sameas="16856" type="literature"><hi rend="u">über 
                    Feodalismus und Republicanismus</hi></bibl> einrücken lassen, die auf eine merkwürdige, aber ganz selbstständige Weise 
                    mit <persname ref="181">Steffens</persname>’ politischen Ideen in den wesentlichsten Puncten zusammentrifft. Sie
                    wird auch vom <persname ref="4339">Prinzen Oscar</persname> mit lebhaftem Interesse gelesen. Unsre <hi rend="u">Presse</hi>
                    ist wohl jezt die <hi rend="u">freieste</hi> in <placename ref="890">Europa</placename>; nur die Tageblätter stehen unter einiger
                    Aufsicht, die aber keineswegs besonders streng ist. Auch wird die <placename ref="1190">Universität von Upsala</placename> 
                    jezt von der Regierung mit grosser <pb source="5r"></pb>Achtung und Auszeichnung behandelt. Drum sind auch die 
                    900 Studenten dort sammt und sonders ausgemachte (obwohl constitutionelle) Royalisten. — Hier in 
                    <placename ref="768">Stockholm</placename> hat man wohl an der Persönlichkeit des <persname ref="4340">Königs</persname> 
                    allerlei auszusetzen, aber mit dem <persname ref="4339">Sohne</persname> ist man allgemein zufrieden. In den 
                    Provinzen hat aber auch <hi rend="u">jener</hi> sich im grossen Credit zu setzen gewusst. Im Grunde hat er
                    den besten Willen, nach Maassgabe seiner Ein- und Ansichten; aber er ist ein <placename ref="627"><hi rend="u">Franzose</hi></placename>, 
                    und fühlt sich als <hi rend="u">Ausländer</hi>, ist dabei durch eine Revolution gebildet und an revolutionären Schauspielen 
                    gewöhnt, und dies macht sein Innres unsicher, schwankend und schwermüthig. Gegen unsre Nachbarn
                    beträgt er sich indessen mit Muth und Gewandtheit, gegen das Volk mit Freundlichkeit, und mit Achtung 
                    gegen die Repräsentanten desselben. Er lebt an seinem Hofe wie ein militärischer Einsiedler, streng und sparsam,
                    will aber dass die Armee prächtig und glänzend erscheinen soll, und ist in seinem eignen Anzug fast ein 
                    bischen zu ausgesucht. Eitel ist er freilich, wie alle Franzosen; aber, wäre er nur nicht so reizbar, hitzig, 
                    inconsequent und hypochondrisch, er würde doch einer von unsern tüchtigsten Herrschern seyn. Dies erwarten wir 
                    aber von seinem Sohne, der es freudig fühlt, dass er in der Nation gleichsam <hi rend="u">eingewurzelt</hi> ist. —</p>
                <pb source="5v"></pb><p>Von den alten Edda's haben wir durch <persname ref="4348">Rask</persname> und 
                    <persname ref="4347">Afzelius</persname> eine schöne <bibl sameas="16857" type="literature">Ausgabe</bibl> erhalten, mit einer gut gelungnen 
                    Übersetzung in unsrer jetzigen Sprache. <hi rend="u">Afzelius</hi> hat auch eine treffliche <bibl sameas="16858" type="literature">Sammlung</bibl> von unsern
                    Ritter- und Liebesliedern aus dem Mittelalter herausgegeben, mit den alten Melodieen dazu. Er macht selbst sehr hübsche 
                    Lieder, so wie auch ein andrer Freund von mir, <persname ref="4349"><hi rend="u">Hedborn</hi></persname>. Beide sind Hofprediger hier in 
                    Stockholm. —</p>
                <p>Ist <persname ref="1204"><hi rend="u">Hjort</hi></persname> noch in <placename ref="21">München</placename>, oder ist er dort über 
                    <date from="1819-12-24" to="1819-12-26">Weihnachten</date> gewesen? Ich wollte dem treffiichen Freund etwas 
                    Geld schicken, was ich ihm noch schuldig bin, weiss aber gar nicht wohin ich Briefe und Gelder
                    adressiren soll. Er hat mir aus <placename ref="162">Florenz</placename> geschrieben im <date when="1819-11-01">Anfang 
                    Novembers</date>, wo er sich auf ein vorhergehendes Schreiben aus <placename ref="99">Rom</placename> bezieht, worin mir alles
                    Nöthige über Geld und Adresse gesagt seyn sollte; dieser Brief ist aber unterwegs veiioren gegangen, denn ich hab’ 
                    ihn nie gesehen. Nur so viel merkte ich aus seinem Florentinischen Brief, dass er in München den <date from="1819-12" to="1820-02">Winter</date>
                    verleben wollte und nachher <placename ref="25">Paris</placename> besuchen. Er ist in Florenz der Krankenwärter des unglücklichen 
                    <persname ref="4350">Rühs</persname> gewesen; dies sieht dem guten und wahrhaft grossmüthigen Hjort sehr ähnlich. Ob Rühs noch 
                    lebt, weiss ich nicht.</p> 
                <pb source="6r"></pb><p>Schliesslich, mein ehrwürdiger Lehrer, lassen Sie sich Ihre letzte Krankheit fein als <hi rend="u">Warnung</hi> gelten, 
                    dass Sie nun nicht länger die Ausgabe Ihrer heiss erwarteten <bibl sameas="8404" type="document"><hi rend="u">Weltalter</hi></bibl> verzögern!!! Wer weiss, wie lange der irdische
                    Lebensfaden fortgesponnen wird? —</p>
                <p>Wenn das Buch wirklich fertig ist, so senden Sie es entweder der <hi rend="u">Frau Generalin <persname ref="1055">von Helwig</persname> in 
                    Berlin</hi> oder dem dortigen <placename ref="888">Schwedischen</placename> Legazions-Secretär 
                    <persname ref="4351"><hi rend="u">von Maule</hi></persname> — so werd’ ich’s wohl am sichersten bekommen. An mich können Sie schreiben 
                    entweder unter Umschlag an die Helwig, oder an Professor <persname ref="1381">Geijer</persname> in 
                    <placename ref="190">Upsala</placename>,*<note place="foot">* Oder auch an den Buchdrucker der <placename ref="1190">Universität</placename>, <hi rend="u">Magister 
                    <persname ref="1380">Palmblad</persname></hi>.</note> weil ich noch nicht weiss wie lange ich hier in <placename ref="768">Stockholm</placename> 
                    verweilen werde. Im letzten Fall thun Sie vielleicht besser, Ihren werthen Brief lieber über <placename ref="1100"><hi rend="u">Stralsund</hi></placename>, 
                    als <placename ref="166">Hamburg</placename> gehen zu lassen; man sagt, dass der Postgang über Hamburg jezt langsamer ist. —
                    Wenn ich die Sache recht bedenke, so ist, was die <bibl sameas="8404" type="document"><hi rend="u">Weltalter</hi></bibl> betrifft, das Räthlichste, dass Sie mir das Buch 
                    gradezu mit der Briefpost senden — wenn nur dieser Weg überhaupt <hi rend="u">zuverlässig</hi> ist. Denn das bischen mehrere
                    Postporto macht mir keine Bedenklichkeit — Die Hauptsache ist, das Buch <hi rend="u">geschwind</hi> <pb source="6v"></pb>zu bekommen — 
                    weil mir sehr viel daran gelegen ist.</p>
                <p>In <placename ref="889">Deutschland</placename> scheint die seltsam-peinliche Lage der Dinge jezt bald ihren höchsten Culminationspunct 
                    erreicht zu haben. Was wohl daraus entstehen wird? — Die Rolle, die besonders <placename ref="1076">Preussen</placename> in dieser 
                    tragikomischen Farce spielt, ist zum Grausen abgeschmackt. Hier behauptet man, die Deutschen hätten eine unermüdliche 
                    Lammesgeduld und werden sich in Alles willig fügen und schmiegen. Ich kann aber die Möglichkeit von so Etwas
                    nicht im Kopf hineinzwingen. Mag aber geschehen was da will, Gott und das Gute werden doch wohl am Ende siegen.</p>
                <p>Wie geht's den lieben Freunden und Freundinnen in <placename ref="21">München</placename>? und vor allen Ihrer holden, trefflichen 
                    <persname ref="223">Pauline</persname>? Was macht die gute <persname ref="1161">Louise Seidler</persname>? Ist sie noch immer in 
                    <placename ref="99">Rom</placename>? — Sie hat mir Ihr Bildniss versprochen und sie muss ihr Gelübde erfüllen. Sag’ ihr das! — 
                    Grüssen Sie auch den braven <persname ref="487">Thiersch</persname> herzlich und seine heitre <persname ref="1155">Frau</persname> — 
                    Die <persname ref="132">Niethammer</persname><persname ref="160"></persname>’s sind wohl noch lebend und gesund? — Wie mögen jezt 
                    Ihre <persname ref="392">Knaben</persname><persname ref="***"></persname> sich blühend entfalten! — O könnt’ ich mich doch in 
                    Ihrem Kreise wieder versetzen, Ihr Lieben Alle! — Aber ich bin im Geiste oft bei Euch — zweifle auch nicht,
                    dass Sie mich in Ihrem Andenken behalten.</p>
            </div>
            <div>
                <p>Atterbom.</p>
            </div>
        </body>
	</text>
</TEI>