<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 5933</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/5933">https://schelling.badw.de/text/5933</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/5933">https://schelling.badw.de/doc/5933</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p><placename ref="21">München</placename> 
                    den <date when="1812-08-25">25. Aug˖<expan>ust</expan> 1812</date>.</p>
            </div>
            <div>
                <p>Soll ich Ihnen gestehen, daß ich nichts weniger erwartete, als 
                    von Ihnen mit einem <bibl sameas="8369" type="document">Brief</bibl> erfreut zu 
                    werden? Ich dachte Sie mir immer – <hi lang="la" rend="aq">remotum longe sejunctumq<expan>ue</expan> 
                    a nostris rebus</hi>, und nun rücken Sie mir auf einmal nahe in 
                    einer Gegend, wo ich kaum hoffen konnte, Ihnen jemals zu 
                    begegnen. Es bedurfte einiger Augenb˖<expan>licke</expan> mich zu überzeugen, 
                    daß der Brief wirklich von <hi rend="u">Ihnen</hi>, von dem 
                    nämlichen Freunde sey, zu dem vor (vielleicht grade) <date when="1810">zwey Jahren</date> mein Geist eine so starke 
                    Anziehungskraft fühlte, den ich aber fast als eine für mich nur 
                    vorübergehende Erscheinung betrachten mußte. Bringen Sie mit 
                    dieser Freude nicht die lange Verzögerung der Antwort in 
                    Widerspruch, Ihr Brief vom <date when="1812-04-05">5ten Apr˖<expan>il</expan></date> ist bis <date when="1812-06-15">Mitte Junys</date> bey unsrem Freund, <persname ref="388">Perthes</persname> liegen geblieben, es kommt also 
                    wenigstens nur die Hälfte der Schuld auf meine Rech<pb source="1v"></pb>nung. – – Ich möchte Sie nun gern festhalten, weil ich 
                    eine ungemeine Sehnsucht habe, mit Männern Ihrer Art umzugehn, 
                    da ich mich mit denen, welche billig meine Nächsten seyn sollten, 
                    meist langweile. –</p>
                <p>Sie trauen mir (doch nur halb) den Gedanken 
                    zu, daß die Zeit keine bloße Denkform, sondern in der That 
                    etwas wirkliches sey. Ich darf Ihnen wohl sagen, daß dieser 
                    Gedanke und das System der Zeiten selbst, (Vergangenheit, 
                    Gegenwart und Zukunft, aber in einem größeren Maßstabe, als 
                    der ist, nach dem wir diese Begr˖<expan>iffe</expan> nur in der Gegenwart 
                    anzuwenden pflegen) das Thema eines längst angekündigten, aber 
                    noch nicht vollendeten Buchs: <bibl sameas="8196" type="document">die Weltalter
                    </bibl><bibl sameas="8404" type="document"></bibl> ist. Bin ich einst so glücklich, Ihnen dieß 
                    Buch zu übersenden, so hoffe ich, Sie werden mit der drinn 
                    gegebnen Entwicklung und Darlegung des Wesens der Zeit 
                    zufrieden seyn. Sie sind aber auch, nebst höchstens Einem oder 
                    zwey Menschen, der Einzige, von dem ich so über die Zeit <pb source="2r"></pb>reden hören. In diesem Punkt ist fast alles 
                    <persname ref="157">Kantianer</persname>, oder beruhigt sich wenigstens darüber und schläft 
                    sozusagen an diesem Abgrund, der einmal aufgedeckt für unser 
                    ganzes religiöses und wissenschaftliches Wesen einen ganz andern 
                    Gehalt hergeben wird.</p>
                <p>Mathematische Beweise von übersinnlichen Sachen kann es nicht 
                    geben, weil hier alles auf Untersuchung beruht, nichts Dogma 
                    ist. Es lassen sich da keine Sätze hinstellen, wie von 
                    geometrischen Wahrheiten. Das Philosophiren ist zuletzt ein 
                    Erforschen der Wege Gottes oder des W˖<expan>esens</expan>, <q>das da war, das 
                    da ist und das da seyn wird<bibl resp="Apk 1,8" type="bibel"></bibl></q> nennen wollen. Hiebey sind wohl 
                    Recherchen so nöthig ja nöthiger, als in der Geschichte der 
                    Erde (Geologie) oder irgend einer andern Histor<expan>ie</expan>. Die 
                    Faulheit, die sich vor Recherchen fürchtet, ist es, die alles 
                    mit der reinen Vernunft, auf dem leichtesten Weg, ausmachen 
                    möchte und deßhalb Gott und göttliche Dinge mit dem Verstand 
                    selbst in Widerspruch erklärt.</p>
                <p>Was Sie mir mit einer Gewißheit, die keinen Zweifel verstattet, 
                    von <persname ref="1141">Lessings</persname> Denkart in Bezug auf 
                    <persname ref="669">Mendelssohn</persname> mir mittheilten, war 
                    <pb source="2v"></pb>nun in so f<expan>ern</expan> merkwürdig, als ich <persname ref="269">J˖<expan>acobi</expan></persname> in der That nicht einmal 
                    zugetr<expan>aut</expan>. Was Sie ihm in Bezug auf jenen Handel Schuld 
                    geben, hätte ich ihm nicht einmal zugetraut. Bedenke ich, mit 
                    welchen Künsten wenigstens bey der gesammten nachgewachsenen 
                    Welt, jene Meynung hervorgebracht worden, die Sie bestreiten: 
                    so entsteht mir der lebhafte Wunsch, daß Männer die dazu 
                    Bezug haben den alten Mendelssohn noch in die gebührende Ehre 
                    wieder einsetzen; ehe die Meynung unwiderruflich auf ihm 
                    haftet. So wenig ich mit ihm sympathisire, so oft habe ich 
                    mir einen Mann seiner Klarheit zurück gewünscht, mit dem es 
                    doch möglich war, in’s Reine zu kommen. Wollten über 
                    M˖<expan>endelssohn</expan>s Verh˖<expan>ältniß</expan> zu Leßing etwas sagen, mit 
                    oder ohne Ihren Namen, so biete ich Ihnen dazu eine <bibl sameas="44" type="literature">Zeitschr˖<expan>ift</expan></bibl> an, die ich demnächst 
                    herausz<expan>ugeben</expan> gedenke und wovon Ihnen <persname ref="388">Perthes</persname> das Nähere 
                    mittheilen kann. Erlauben Sie mir überh˖<expan>aupt</expan> die Bitte, 
                    so Vieles, das Sie noch  mittheilen könnten, der Welt nicht 
                    vorzuenthalten und alles der Art, besonders hist<expan>orische</expan> 
                    Beyträge einstweilen jener Zeitschr˖<expan>ift</expan> anzuvertrauen, 
                    die, ich hoffe es, einen lang gewünschten Vereinigungspunkt 
                    abgeben soll, indem sie nichts ausschl˖<expan>ießt</expan>, was in s<expan>eine</expan>r 
                    Art ganz und tüchtig ist und nur das tödtende unkräftige 
                    verfolgt. Daß Sie in Ans˖<expan>ehung</expan> solcher Mittheilungen, 
                    bey denen es darauf ankommt, auf volle Diskr˖<expan>etion</expan> rechnen 
                    dürfen glaube ich Sie nicht erst versichern zu d<expan>ürfen</expan>.</p>
            </div>
            <div>
                    <p>Leben Sie recht wohl, gedenken Sie m˖<expan>einer</expan> 
                        Bitte und erhalten Sie mir Ihre sehr werthe Gunst,<lb></lb>
                        Ihr</p>
            </div>
        </body>
	</text>
</TEI>