<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 120</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/120">https://schelling.badw.de/text/120</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/120">https://schelling.badw.de/doc/120</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p><placename ref="21">München</placename> den <date when="1813-12-22">22 Dec˖<expan>ember</expan> 13</date>.</p>
                <p>Liebste Mutter!</p>
            </div>
            <div>
                <p>Der Proceß wegen des Stillens hat sich von selbst entschieden.  Der <persname ref="225">Kleine</persname>, nachdem er ein 
                    einzigesmal tüchtig gezogen und auch etwas getrunken, war durchaus nicht wieder an die Brust zu bringen, so daß man 
                    für die <persname ref="223">Mutter</persname> sorgen und auf Vertreibung der Milch denken mußte. Ob er nun dabey vielleicht 
                    mehr Verstand bewiesen als wir, die ihn durch Hunger und Durst dazu zwingen wollten, steht dahin.</p>
                <p><persname ref="223">Pauline</persname> hat die letzten Tage wie natürlich vom Milchfieber gelitten, und ist durch die 
                    magere Diät, die sie halten mußte, und die starken Ausleerungen etwas entkräftet; doch ging alles ohne bedenkliche 
                    Zufälle vorüber, und ihre Schwäche wird durch kräftigere Nahrung bald gehoben seyn. Überhaupt befindet sie sich 
                    heute am 6. Tag im bestmöglichen Zustand; alles bis jetzt ging, Gott sey Dank! so gut, als wir kaum hoffen durften, 
                    das Stillen ausgenommen. Der Kleine hat schon die Gelbsucht überstanden; sein Betragen widerspricht seinem Aussehen 
                    nicht, <pb source="1v"></pb>d.h. es ist recht vernünftig; sobald ihm nur wohl ist und weder Hunger noch Durst ihn plagt 
                    ist er stille. Doch thut es mir weh, ihn mit Brey und Gerstenwasser zu füttern; auch sehe ich wohl daß trotz der vielen 
                    weiblichen Personen keine rechte Ordnung in seine Behandlung kommt, wenn nicht eine eigne Person da ist, die bloß für 
                    seine Wartung bestimmt ist. Meine <persname ref="88">Mutter</persname> muß ihn wenigstens die Nacht über aus ihren Armen 
                    lassen, und die gute <persname ref="275">Köhler</persname> steht um ebendiese Zeit Paulinen bey. Ich suche daher jetzt nach 
                    einer Amme für das Kind zu bekommen, und habe überall hin aufs Land darum geschrieben. Gelingt es, eine gute zu bekommen, 
                    so wird der Kleine trefflich gedeihen, da er so kerngesund und vollkommen ist, auch eine große Gemüthsruhe zu haben scheint.</p>
                <p><persname ref="223">Pauline</persname>, wie Sie sich leicht denken können ist überglücklich wegen des herrlichen Kindes. Ich erinnere mich wirklich 
                    nicht, ein Kind in den ersten Tagen so kräftig gesehen zu haben. Die Hebamme schätzt es auf 10 Pfund. Dieß ist aber das 
                    Wenigste; <pb source="2r"></pb>am meisten freut Paulinen und mich sein vernünftiges Aussehen. Er hat schon viel Physiognomie: der Ausdruck 
                    des kleinen Gesichts verräth ungemein viel Innerlichkeit; dadurch zieht er alle Menschen an, ohnerachtet ich manche Kinder 
                    gesehen habe, die schönere Augen hatten und mehr Anlage hübsch zu werden. Pauline sagt, es sey ein Juwel von Kind; und der 
                    Vater kann ihr nicht widersprechen.</p>
                <p>Nun, liebste Mutter, erlauben Sie mir noch eine Bitte vorzubringen, die Sie gewiß gern gewähren, daß Sie dem Kind nicht bloß 
                    Großmutter seyn, sondern ihm auch Pathe werden. Ihrer Liebe, Ihrem Gebet brauche ich zwar ein Kind Ihrer geliebten <persname ref="223">Pauline</persname> 
                    durch keinen besonderen Beweggrund zu empfehlen; doch ist es uns tröstlich, unser Kleinod auch in diesem Verhältniß zu 
                    Ihnen zu wissen. Haben Sie zugleich die Güte, die anliegende <bibl sameas="7466" type="document">Gevatterbriefe</bibl> an die Addressen abzugeben.</p>
                <p>Ich muß für heute schließen –, unter den herzlichsten Begrüßungen von uns allen an Sie alle. Seyn Sie ganz beruhigt, liebste 
                    Mutter, ich glaube <pb source="2v"></pb>Ihnen versprechen zu dürfen, daß wenn Gott will und die gute <persname ref="223">Wöchnerin</persname> 
                    sich gehörig hütet, das Wochenbett unserer lieben Pauline so fast beyspiellos glücklich vorübergehn wird als die Schwangerschaft. 
                    Um Ihnen einen Maßstab ihres Wohlbefindens zu geben, so denken Sie nur, daß sie trotz der Schmerzen und des vielen Säfte-Verlustes 
                    gleichwohl noch immer gesünder, blühender und stärker aussieht, als da ich sie <placename ref="21">hieher</placename>führte.</p>
            </div>
            <div>
                    <p>Leben Sie recht wohl. Gott erhalte Sie!<lb></lb>
                        Ihr Treugeh˖<expan>orsamster</expan> Sohn</p>
                    <p>S.</p>   
            </div>
            <div>
                <p>N.S.</p>
                <p>Der <persname ref="225">Kleine</persname> bekommt die Namen, Paul Heinrich Joseph. Paul wird er gerufen. Heinrich heißt er nach meinem 
                    älteren <persname ref="16">Onkel</persname> und nach <persname ref="4049">Fischer</persname>. Joseph ist der Name seines 
                    verewigten <persname ref="191">Großvaters</persname> und als Schellingischer Familien-Namen. Die Taufe wahrscheinlich 
                    den <date when="1813-12-29">29. d<expan>ieses</expan></date>.</p>   
        </div>
        </body>
	</text>
</TEI>