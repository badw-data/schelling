<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 6596</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/6596">https://schelling.badw.de/text/6596</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/6596">https://schelling.badw.de/doc/6596</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p><placename ref="81">Würzburg</placename> <date when="1820-02-26">26 Hornung 1820</date>.</p>
            </div>
            <div>
                <p>Ihr <bibl sameas="930" type="document">Brief</bibl> von <date when="1820-02-19">19ten d˖<expan>ieses</expan></date> mein theurer und verehrter Freund, 
                    war mir ein erfreuliches Bürge ihrer Genesung, von deren erwünsschten Fortschritten mich zuerst die freundschaftliche 
                    Theilnahme der treflichen Frau <persname ref="275">von Köhler</persname> unterrichtet hatte, so wie sie auch die erste war, 
                    durch die ich Nachricht von ihrer Krankheit erhielt. Möge sich ihr Wohlseyn nun wieder recht dauernd befestigen: dieß 
                    ist gewiß mein innigster Wunsch, welchen ich nicht allein mit ihren Freunden sondern mit einer Welt theile, die von 
                    Ihnen noch so groses erwartet und der Sie in Vollendung das groß begonnenen heilige Worte zu büsen haben. Nein! mein 
                    Freund, ich will nicht sterben, ehe ich ihre <bibl sameas="8404" type="document"><hi rend="u">Weltalter</hi></bibl> gelesen habe, wenn es gleich auch ein erhebender Gedanke 
                    ist, in jener nicht alternden Welt, wo Aeonen Tage sind, den ewigen Geist, der alle Welten durchdringt und bewegt, 
                    näher als hienieden kennen zu dürfen! – Dankbar empfange ich die herzliche Zusprache, mit der Sie meinen Muth, so 
                    wie die Rathschläge durch deren Anwendung Sie meine <unclear cert="high">physischen</unclear> Kräfte zu beleben suchen. Noch steht der erste 
                    fest in der Überzeugung, daß selbst von groser Entkräftung immer ein gewaltiger Schritt zu jenem gänzlichen Zerreißen der 
                    unerforschten Verbindung zwischen Seele und Körper übrig bleibt; aber, verhehlen läßt sichs nicht, ich fühle meine 
                    Kräfte tief gesunken! Kaum vermag ich die Feder zu halten, die diesen Zeilen meine Namenszüge beisezen soll und 
                    so eigensinnig ist der launenhafte Gang meiner Krankheit, daß was sich in ähnlichen Lagen andern heilsam erwiesen, 
                    bei mir nur mit Behutsamkeit anzuwenden ist, oder in seiner Wirkung durch die Complication des Übels aufgehalten wird. 
                    Ein langwieriger Katarrhe, von welchem ich im <date from="1818-12" to="1819-02">vorigen Winter</date> befallen war, 
                    zog im <date from="1819-03" to="1819-05">Frühjahre</date> einen Bluthusten mit Fieber nach sich,
                    dem eine entzündliche <pb source="1v"></pb>Affection der Schleimhaut des Kehlkopfes folgte. Dieses örtliche Leiden dauerte 
                    biß in den <date from="1819-12" to="1820-02">jezigen Winter</date> und nur durch eine äußerst strenge Diät in Verbindung 
                    mit Blutentleerungen und den geeigneten Arzneimitteln war es endlich gelungen, daßelbe zu bezwingen. Allein die Thätigkeit 
                    der Verdauungswerkzeuge, besonders des Magens, hatte dabei so außerordentlich gelitten, daß der leztere seine Dienste 
                    nun gleichsam versagen will und eine vollkommene Abneigung gegen alle Nahrungsmittel eingetreten ist, die eine 
                    unbeschreibliche Schwäche zur Folge hat. Die Ärzte hoffen indeß diesen Zustand zu heben und erwarten mit der 
                    herannahenden wärmeren Jahreszeit deßen dauerhafte Besserung. –</p>
                <p>So, mein Freund, liege ich nun dem Ausgange ruhig entgegenharrend hier. Ich blike mit heitrer Zuversicht in die Zukunft 
                    und das einzige unangenehme Gefühl, welches mich jezt belästigt, ist das der Abhängigkeit, worin der Geist, 
                    deßen freier Gedanken Welten umfaßen und zum Göttlichen aufstreben darf, von ein paar kranken Organen des thierischen 
                    Lebens gehalten wird! – Nur die Regungen des Herzens schlagen auch in diesem Zustande um so inniger fort und 
                    in der Fülle derselben empfangen denn auch Sie, mein ferner und geliebter Freund, mit ihrer 
                    <persname ref="223">Gattinn</persname> und allen den Ihrigen, meinen segnenden Gruß! Will es Gott, wie ich hoffe, 
                    so sehen wir uns baldmöglichst in freundlichen Umgebungen und milder Luft, woran ich <unclear cert="high">nur</unclear> mit sehnendem Entzüken 
                    denke, wieder; Wo nicht, so bleibt dieß Wiedersehen jenen Regionen aufbehalten, wohin ich zu reisen – nicht wie 
                    ein zeitlebens Reisefertiger, was sich ohnehin versteht, sondern wie einer, der einigemale schon dem Rufen des 
                    ankerlichtenden Steuermanns entgegenlauschte – gerüstet bin und wo ich vielleicht mit Augen <hi rend="u">sehe</hi>, was mein 
                    Freund Schelling in seinem Forschergeiste <hi rend="u">gedacht</hi> hat! –</p>
            </div>
            <div>
                <p>Ewig mit unveränderlicher Ergebenheit<lb></lb>Ihr Freund</p>
                <p>Klein <hi rend="uu"><hi rend="aq"><foreign xml:lang="la">m<expan>anu</expan> p<expan>ropria</expan></foreign></hi></hi></p>
            </div>
            <div>
                <pb source="2r"></pb><p resp="Klein">P.S.</p>
                <p>Sie sehen aus der Abfassung des Briefes, daß ich mich wegen großer Schwäche der Hand und des Geistes 
                    eines Freundes bedienen mußte, der im ganzen meine Gesinnungen ausdrükt, wovon aber die Form dem Concipienten
                    angehört. Ich hoffe, mit Gottes Beistand bald mit eigener Hand Ihnen schreiben zu können. Herzliche 
                    Grüße an Ihre geliebte <persname ref="223">Paulina</persname> und an die treffliche Freundin Frau 
                    <persname ref="275">von Köhler</persname>. Gott mit Ihnen allen, wie mit Ihrem unveränderlichen Freunde<lb></lb>Klein</p>
                <p><placename ref="81">Würzb˖<expan>urg</expan></placename> <date when="1820-02-27">27 Febr˖<expan>uar</expan> 1820</date>.</p>
            </div>
		</body>
	</text>
</TEI>