<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 2921</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/2921">https://schelling.badw.de/text/2921</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/2921">https://schelling.badw.de/doc/2921</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <p>Al Signore</p>
                <p>S<expan>i</expan>g<expan>no</expan>re Martino <hi rend="u">Wagner</hi></p>
                <p>Membro della <placename ref="1059">Reale Academia di Baviera</placename> pp.</p>
                <p><placename ref="99">Roma</placename></p>
                <p>al cafe greco</p>
                <p>Fr<expan>an</expan>co.</p>
            </div>
            <div>
                <pb source="1r"></pb><p>Liebster Freund!</p>
            </div>
            <div>
                <p>Es scheint, daß auch jetzt noch die Briefe nach <placename ref="99">Rom</placename> 
                    nicht zum sichersten gehen. Ich habe Ihnen für die angefangene 
                    <bibl sameas="7451" type="document">Reisebeschreibung</bibl> nach <placename ref="892">Griechenland</placename> so schön 
                    <bibl sameas="8449" type="document">gedankt</bibl> und Sie um die Fortsetzung gebeten, daß ich wenigstens auf eine 
                    Antwort hoffte. Es sey nun wie es wolle, so schreibe ich Ihnen dießmal, 
                    um für einen andern Genuß zu danken, den Sie mir bereitet haben. Seine 
                    Königliche Hoheit der <persname ref="444">Kronprinz</persname> haben die 
                    Gnade gehabt, mir Ihre <bibl sameas="2314" type="literature">Beschreibung</bibl><bibl n="Bd. I 19. S. 315–404" sameas="6347" type="literature"></bibl> der <placename ref="1585">äginischen</placename> Statuen <bibl sameas="8450" type="document">mitzutheilen</bibl>. Wahrscheinlich hatten 
                    Sie den Kronprinzen darum angegangen. Welche Erscheinung, welche Wunder! 
                    <placename ref="362">England</placename> mag den Raub von <placename ref="779">Athen
                    </placename> behalten, wir werden die Werke besitzen, die allein den Schlüssel 
                    geben zu Erklärung jener sonst unbegriffenen Vortrefflichkeit. Diese 
                    treue Nachahmung der schönen Natur, gegen welche auch die treueste, die 
                    wir sonst kannten, nur auf der Oberfläche zu spielen scheint, war also 
                    der Weg zu der Kunst des <persname ref="3188">Phidias</persname>! – Was die 
                    verwundersame Einförmigkeit und Gleichheit der Köpfe und der Bildung wie 
                    des Ausdrucks in den Gesichtern betrifft, so haben Sie, glaube ich, im 
                    Allgemeinen das Wahre getroffen. Nur kann ich mich davon nicht 
                    überzeugen, daß die Scheu, einen festen heiligen Typus zu verletzen, 
                    diese Einförmigkeit hervorbrachte. Kann da wohl von Typus die Rede seyn, 
                    wo im Grunde gar kein Charakter herrscht? Wenn die <persname ref="3158">Minerva</persname> oder jede andre Gottheit, ja wenn nur überhaupt alle 
                    Gottheiten sich auf solche Art glichen; so wäre das ein Typus; wenn aber 
                    <hi rend="u">alle</hi>, göttliche und menschliche, männliche und weibliche 
                    Bildungen, Sieger und Besiegte, wie Sie sagen, sich was die Köpfe 
                    betrifft, wie Ein Ey dem andern gleichen – diesen gänzlichen Mangel von 
                    Abwechselung in den Bildungen nicht <hi rend="u">Einer</hi> und <hi rend="u">derselben</hi>, 
                    sondern <hi rend="u">ganz verschiedner</hi> Persönlichkeiten weiß <pb source="1v">
                    </pb>ich mir nicht anders zu erklären, als aus einem absichtlich, auch in 
                    Ansehung der verschiednen Körpertheile stufenmäßigen Aufsteigen vom 
                    Niederern zum Höhern. Es ist so viel gesetzmäßiges in der ganzen 
                    Entwicklung der griechischen Kunst, daß es mir als ganz natürlich 
                    erscheint, wenn sie auch in dieser Beziehung mit Vorbedacht und 
                    Bewußtseyn den Weg <hi rend="u">von unten auf</hi> genommen, wenn sie, zufrieden, 
                    die niederen Theile auf’s treueste zu bilden, in Ansehung des edelsten 
                    so lange, bis sie auch dessen Meister geworden, lieber mit einer 
                    angenommenen (conventionellen), allgemeinen Form als einer 
                    halbkünstlerischen Ausführung sich begnügte, und Kopf und Gesicht lieber 
                    ruhen ließ als es weniger vortrefflich darstellte, wie sie vielleicht 
                    aus demselben Grund früher sich versagt hatte, die Beine zu trennen und 
                    in Bewegung darzustellen. Eine andre als bloß angenommene, gleichsam 
                    durch Verabredung geltende, wie nur symbolisch andeutende Form, kann ich 
                    in den Köpfen nach Ihrer Beschreibung nicht sehen. Was halten Sie davon?</p>
                <p>Und nun noch eine Frage. Ist es der Wille Seiner Kön˖<expan>iglichen</expan> <persname ref="444">Hoh˖<expan>eit</expan></persname> 
                    oder ist es Ihre Absicht, daß Ihre <bibl sameas="2314" type="literature">Beschreibung</bibl><bibl n="Bd. I 19. S. 315–404" sameas="6347" type="literature"></bibl> über kurz oder lang 
                    gedruckt werde? Dieß wäre sehr wünschenswerth, da schon so manches 
                    davon gefabelt wird und jetzt, da die Werke in <placename ref="99">Rom</placename> von Manchen gesehen 
                    werden, da doch vieles davon, obwohl unvollständig, verlautet, sich 
                    bald rüstige Federhelden finden werden, die Nachrichten davon in’s 
                    Publicum senden. Auf diesen Fall würde ich mich Ihnen <pb source="2r"></pb>zum Herausgeber anbieten und für mich außer den etwa nöthigen 
                    Veränderungen des Styls keine andere Freyheit bedingen, als einige 
                    Anmerkungen und Nutzanwendungen beyzufügen. Wären Sie damit 
                    einverstanden, so wollte ich Sie bitten, Seiner Kön˖<expan>iglichen</expan> Hoheit 
                    dieses auf gute Art vorzuschlagen; ich mochte es weder überhaupt, 
                    noch ohne Sie, thun. Sonst fürchte ich fällt über kurz oder lang die 
                    Sache einem unsrer hiesigen <placename ref="1383">norddeutschen</placename> 
                    Magister in die Hände, von denen <persname ref="487">einer</persname> 
                    dieser Tage eine <bibl sameas="16924" type="literature">Abh˖<expan>andlung</expan> über die älteste 
                    KunstEpoche Griechenlands</bibl> ohne allen Sinn und Kenntniß so 
                    geschrieben, daß es die Hunde nicht fressen möchten.</p>
                <p>Endlich noch eine Frage! Kommen Sie denn diesen <date from="1816-06" to="1816-11">Sommer oder Herbst</date> 
                    nicht zu uns? Wie sollte mich dieß freuen, der ich mich alle Jahre 
                    mehr sehne, Sie wieder zu sehen! In dem Fall, daß Sie kommen, bitte 
                    ich Sie um das Eine, es <unclear cert="high">mir</unclear> einige Monate vorher 
                    wissen zu lassen, um mich auch darnach einzurichten.</p>
                <p>Wie steht es mit Ihren künstlerischen Arbeiten? Werden wir bald Ihr 
                    großes Gemälde sehen? Wenn Sie schreiben, so schreiben Sie mir nur 
                    recht viel von Sich und Ihrem Thun und Treiben – die 
                    Reisebeschreibung gebe ich auch noch nicht auf. Lassen Sie mich 
                    nur nicht zu lange auf Antwort warten.</p>
            </div>
            <div>
                <p>Grüßen Sie auch den <persname ref="589">Fritz 
                    Gaertner</persname> recht schön von mir, wenn er noch in <placename ref="99">Rom</placename> ist, und 
                    leben Sie recht wohl. Ich bin und bleibe<lb></lb>
                    Ihr<lb></lb>
                    Getreuer</p>
                <p>Schelling.</p>
                <p><placename ref="21">München</placename> <date when="1816-04-04">4. April 1816</date>.</p>
                
            </div>
		</body>
	</text>
</TEI>