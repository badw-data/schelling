<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 110</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/110">https://schelling.badw.de/text/110</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/110">https://schelling.badw.de/doc/110</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p><placename ref="16">Tubingen</placename> 
                    den <date when="1812-03-01">1sten Merz 1812</date>.</p>
            </div>
            <div>
                <p>Empfangen Sie, verehrter Freund und Lehrer! meinen innigsten Dank für Ihr 
                    treffliches <bibl sameas="2442" type="literature">Buch</bibl><bibl n="Bd. I 18. S. 129–230" sameas="6347" type="literature"></bibl>! Es hat mich in jeder Hinsicht 
                    befriedigt und ich theile auch die Stimme derer nicht, welche eine fein 
                    ausgesprochene Lüge verzeyhen, während sie eine derb, aber wahrheitsvoll 
                    <hi rend="u">gerügte</hi> Lüge bis in den Abgrund der Hölle verdammen. Den Meynungen 
                    solcher Menschen aber wächst der vor aller Augen 10 Mal abgeschlagene 
                    Kopf immer wieder von Neuem. Ich begnüge mich daher solchen Menschen 
                    blos <persname ref="705">Christ</persname>’s <bibl resp="Mt 3,7" type="bibel">Otterngezüchte</bibl> und die <q>gegen die 
                    Mäkler im Tempel geschwungene Geißel<bibl resp="Mt 21,12–17" type="bibel"></bibl></q> und <persname ref="546">Luther
                    </persname>’s Kraftworte vorzuhalten und dann – ohne Antwort abzuwarten – kurz 
                    abzubrechen. Der Tadel solcher personifizirten Nullitäten verschwindet 
                    in seinem Nichts, während das Denkmal nicht blos 
                    ein Denkmal <persname ref="269">Jacobi</persname>’scher Schwachköpfigkeit und 
                    Herzensenge, sondern auch eins für die gediegene wißenschaftliche Polemik, 
                    wo der Gegner immer auf seinem eignen Felde geschlagen wird, immer und 
                    ewig in der literarischen Welt leben wird.</p>
                <p>Von jenen Schwätzern unendlich verschieden ist eine andere Klaße von 
                    Urtheilern, welche weder die Sünde des Gegners, noch das Recht des 
                    Vertheidigers verkennen; aber dennoch jenen beklagen, daß er nicht, statt 
                    auf die ewige Folter gelegt, kurzweg geköpft wurde und auch diesem, dem 
                    Vertheidiger, wünschen, er möge Herr seiner seit <date when="1804">8 Jahren</date> behaupteten 
                    philosophischen Ruhe lieber seyn, die sie durch die Vertheidigung mehr 
                    gefährdet glauben, als durch den unrechtlichen Angriff. Die meisten 
                    dieser Klaße wünschen den »<bibl n="Bd. I 18. S. 229" sameas="6347" type="literature">Sykophanten<bibl n="S. 213" sameas="2442" type="literature"></bibl></bibl>«, und die »<bibl n="Bd. I 18. S. 229" sameas="6347" type="literature">Doppel-Larve<bibl n="S. 213" sameas="2442" type="literature"></bibl></bibl>« weg.</p>
                <p>Eine dritte Klaße von Lesern ist ganz <hi rend="u">für</hi> die <bibl sameas="2442" type="literature">Schrift</bibl><bibl n="Bd. I 18. S. 129–230" sameas="6347" type="literature"></bibl>, weil diese 
                    ihnen den Wahn läßt, als könnten sie das Buch und mit ihm das ganze System 
                    verstehen oder vielmehr als hätten sie es verstanden, und weil sie darin 
                    den scharfen Witz finden, der ihnen allein das Ernste und Heilige 
                    erträglich macht. Mich argert das Lob solcher <pb source="1v"></pb>Menschen 
                    immer, ob ich mir gleich nicht verberge, daß es selbst für die Wißenschaft, 
                    die ins Leben durchdringen will, nicht gleichgültig ist, ob solche 
                    Weltmenschen ihre Stimme, die eigentlich nicht stimmen sollte, dahin oder 
                    dorthin giebt. Ich setze Ihnen eine Stelle aus einem Briefe her, den der 
                    <persname ref="2593">Repräsentant</persname> einer ganzen Klaße von Menschen 
                    in <placename ref="889">Deutschland</placename> an den Prof. <persname ref="631">Tafinger</persname> schrieb:</p>
                <p>»So eben lege ich Schellings <bibl sameas="2442" type="literature">Denkmal für Jacobi</bibl><bibl n="Bd. I 18. S. 129–230" sameas="6347" type="literature"></bibl> 
                    aus der Hand, ein in jeder Hinsicht äußerst interessantes Werk, das Sie 
                    so bald, als möglich lesen müßen. Der arme <persname ref="269">Jacobi</persname> ist im höchsten Grade 
                    gebrandmarkt und das Buch mit einer Klarheit geschrieben, die ich 
                    Schelling nicht zugetraut hätte. Ich bitte Sie, keinen Augenblick zu 
                    versäumen, dieses wichtige Buch, das sehr große Folgen haben muß, hohlen 
                    zu laßen. Noch mehr, wie Jacobi, – denn dieser hat sich sein 
                    Unglück sebst zugezogen – bedauere ich Herrn <persname ref="764">K˖<expan>öppen</expan>
                    </persname> p (und nun geht der Schreiber, <persname ref="2593">Graf Mandelsloh</persname>, auf einen andern Gegenstand über)</p>
                <p>Die beste Klaße von Lesern ist wohl diese, und zu ihr rechne ich mich 
                    auch, welche es aufrichtig beklagt, daß die Scham- und Ehrlosigkeit der 
                    Philosophaster, da man mit der neuen Philosophie zum philosophischen 
                    Leibe ihrer Nichtphilosophie gieng und die nun nichts Beßres zu thun 
                    wußten, als zu schimpfen, zu ridiculisiren, zu verdrehen und zu 
                    verketzern, um nur sich noch für etwas geben zu können, eine <hi rend="u">solche</hi> 
                    Züchtigung nothwendig machte; die es deswegen beklagt, weil der Anblik 
                    der Züchtigung immer etwas Unangenehmes hat und dem Strafenden beynahe 
                    mehr weh thut, als dem Gestraften, wenn er das Gefühl seines Unrechts 
                    aufkommen läßt; die es aber dankbar erkennen, daß der Gewalthaber bey 
                    der Strafe auch den hohern Zweck der Strafe – die Belehrung und Beßerung 
                    für Alle – im Auge behielt.</p>
                <pb source="2r"></pb><p>Für das, was Sie mir zu meiner Warnung über <persname ref="3124">M˖<expan>eyer</expan></persname> <bibl sameas="7349" type="document">schrieben</bibl>, danke ich Ihnen recht sehr. 
                    Noch glaube ich nicht an die Abscheulichkeiten, die man Ihnen von ihm 
                    gesagt hat und um so weniger, weil ich den Ungrund einiger, ebenfalls 
                    entehrenden, Anecdoten, juridisch bewiesen erhielt; aber dennoch ist 
                    Vorsicht bey einem Menschen nöthig, der wenigstens höchst unvorsichtig 
                    gehandelt haben muß, um nur in einen solchen Ruf kommen zu <hi rend="u">können</hi>.</p>
                <p>Das Scandalum, daß man einen jungen <persname ref="1502">Ausländer</persname>, der sich noch durch nichts 
                    ausgewiesen hat, hier anstellte und den anerkannt tüchtigen <persname ref="615">Pfister</persname> unangestellt ließ, gebe ich Ihnen zu und ich 
                    danke dem Himmel, daß es vor meiner Zeit gegeben wurde, wenn es doch 
                    einmal gegeben werden sollte; allein von der <hi rend="u">Erbärmlichkeit</hi> des 
                    Dr. Dresch habe ich auch noch keine Belege. 
                    Er ließt seine Collegia fleißig und nicht ohne Geschmak. Daß ihm die 
                    höchste Idee der Geschichte nicht aufgegangen ist, nicht aufgehen wird, 
                    ist ein Schiksal, das er mit den meisten berühmteren Historikern theilt. 
                    Wollte er mir aber einmal etwas bieten und etwa scheiden wollen von uns, 
                    ich würde keinen Augenblik säumen, ihm glükliche Reise zu wünschen und 
                    uns Glük, wenn Pfister an seine Stelle treten möchte.</p>
                <p>Wenn ich Ihr »wenn es sich schickt« misverstand, so liegt der 
                    Entschuldigungsgrund dieses Misverständnißes in der Ueberzeugung, daß 
                    es unmöglich sey, mich, der ich mich überall so gebe, wie ich bin, für 
                    einen Mann zu halten, bey dem solche Floskeln für unvornehme Vornahme 
                    angebracht wären. Vergeben Sie mir also um meines Stolzes Willen das 
                    Unrecht, was ich Ihnen angethan. Ich hatte <persname ref="111">Eschenmayern</persname> von der ganzen Sache nichts gesagt, bis er mir heute 
                    einen lieben (mir sehr lieben) <bibl sameas="3813" type="document">Brief</bibl> zeigte, in welchem Sie Selber 
                    davon gesprochen hatten. E˖<expan>schenmayer</expan> willigt gern in Ihren Wunsch 
                    und freut <pb source="2v"></pb>sich auf Ihre gedruckte <bibl sameas="3589" type="literature">Antwort</bibl><bibl n="Bd. I 19. S. 107–130" sameas="6347" type="literature"></bibl>. Ich bin herzlich froh, daß Sie ihn zwingen werden tiefer 
                    in Ihr System einzugehen, das er eben auch nur von einer Seite kennt 
                    und versteht. Ueberhaupt glaube ich selber, daß es für <placename ref="16">Tubingen</placename>, für die Wißenschaft und für Eschenmayer selber ein 
                    glüklicher Gedanke war, ihn hierher zu ziehen. Es ist doch etwas ganz 
                    anders in Einem Stück fort eine Sache durchzudenken, als dann, wann 
                    man von fremdartigen Berufsgeschäften ermattet ist. Doch ist auf der 
                    andern Seite auch nicht zu läugnen, daß die durchlaufene praktische 
                    Laufbahn seiner jetzigen guten Vorschub leisten mag. Das, was Sie mir 
                    jezt in Hinsicht auf E˖<expan>schenmayer</expan> rathen, war mein erster Gedanke, 
                    als mein liebster Plan auf Sie gescheitert war; allein dieser Gedanke 
                    schien wieder an E˖<expan>schenmayer</expan>’s Abneigung, sich in das Facultätsjoch 
                    zu spannen, scheitern zu wollen. Doch wird der hoffentlich nachgeben. 
                    Ich wenigstens bin in jedem Falle entschlossen, Ihrem guten Rath 
                    insofern zu folgen, daß ich, wenn mir, was ich nicht fürchte, nicht 
                    einer aufgedrungen wird, keinen Prof˖<expan>essor</expan> der Philosophie berufe. Dazu 
                    bestimmt mich nun noch mehr Ihr herrliches Anerbieten, dem <placename ref="1009">Vaterlande</placename> 
                    einen tüchtigen Philosophen zuzubilden. Wenn nur das Subject dazu 
                    schon gefunden wäre? Zwey junge Leute geben mir Hoffnungen – der 
                    Repetent <persname ref="728">Köstlin</persname> (den Sie ja personlich 
                    kennen und der einer Ihrer eifrigsten Anhänger ist und <persname ref="4089">M˖<expan>agister</expan> Schwab</persname>, der sich auch als Dichter angekündigt hat – 
                    allein noch wage ich nicht etwas Entscheidendes über sie zu urtheilen. 
                    Ich habe E˖<expan>schenmayer</expan> aufgefordert, sie scharf ins Auge zu faßen.</p>
                <p><persname ref="3125">Niederers</persname> <bibl sameas="4497" type="literature">Schrift</bibl> 
                    ist (sonderlich die zweyte Auflage davon) wirklich bedeutend und würde 
                    Sie nicht unangenehm in einer Erhohlungsstunde unterhalten. Auch ihm 
                    wirft man Derbheit, Arroganz pp vor, weil er <hi lang="la" rend="aq">scamnum 
                    scamnum</hi> nennt. Am meisten schreyen die sogenannten 
                    Pestalozzianer. Es thut ihnen gar zu weh, daß sie von <persname ref="3307">Pest˖<expan>alozzi</expan></persname> nichts verstanden haben sollen. Ihm wird 
                    freylich Ihre <bibl sameas="2442" type="literature">Schrift</bibl><bibl n="Bd. I 18. S. 129–230" sameas="6347" type="literature"></bibl> wichtiger seyn, als 
                    Ihnen <bibl sameas="4497" type="literature">seine</bibl>;  allein Ihr Ziel – Ihr System 
                    die ganze Denkart, auch die ins Leben wirkende, umwälzen zu lassen – 
                    kann Ihnen Niemand näher rücken und den Weg dahin beßer reinigen, 
                    als Er, wenn’s gleich nur Wenige merken. Die Stelle Ihres <bibl n="S. 102" sameas="2442" type="literature">Buchs S. 
                    102</bibl><bibl n="Bd. I 18. S. 176" sameas="6347" type="literature"></bibl> wird ihn entzücken. Sie ist von der höchsten pädagogischen 
                    Wichtigkeit und betrifft gerade den Punkt, in welchem er sich von 
                    Fre<expan>u</expan>nd Eschenmayer scheidet, den Pestalozzi’s Rede zu <bibl sameas="16899" type="literature">Prologomen für jede künftige Pädagogik</bibl> begeistert 
                    hatte, die auch nächstes in der <bibl sameas="4064" type="literature">Wochenschrift
                    </bibl> gedruckt erscheinen werden.</p>
            </div>
            <div>
                    <p>Leben Sie wohl mein bester, theuerster Freund! Ganz der Ihrige</p> 
                    <p>Wangenheim</p>
                    <p>Pasigraphie nicht zu vergeßen!</p>
            </div>
        </body>
	</text>
</TEI>