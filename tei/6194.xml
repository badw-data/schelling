<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 6194</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/6194">https://schelling.badw.de/text/6194</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/6194">https://schelling.badw.de/doc/6194</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p>Leider muß ich den Brief, wie immer, mit der Selbstanklage 
                    anfangen, beste Pauline! Es ist wieder über einen Monat, daß ich Ihren 
                    lieben <bibl sameas="6193" type="document">Brief</bibl> erhalten habe. Freylich ging die 
                    Zeit hin im Hin- und Herüberlegen, wie es wohl anzustellen wäre, die lieben 
                    Freunde zu sehen, und immer hoffte ich, darüber etwas gewisses schreiben zu 
                    können. Aber es ist jetzt alles so erschwert mit dem Reisen, besonders 
                    angestellter Leuten; und über die Gränzen unsrer kleinen Staaten zu kommen 
                    kostet bald soviel Mühe, als in’s große <placename ref="1398">chinesische Reich</placename> 
                    hineinzukommen. Wollte ich Ihr freundliches Erbieten, das mich im Herzen 
                    gefreut, einen Theil des Wegs entgegenzukommen, auch annehmen, was ich doch 
                    fast nicht verantworten könnte: so wäre mein äußerster Punkt ohne die Gränze 
                    zu überschreiten <placename ref="411">Cronach</placename> an der Spitze des <placename ref="74">Bamberger</placename> Landes; wie könnte ich aber Ihnen zumuthen, den 
                    ganzen <placename ref="1399">Thüringer Wald</placename> um meinetwillen zurückzulegen! 
                    – Ich habe mich nun so eingerichtet. Schon seit <date when="1811-07">
                    vorigem Monat</date> habe ich mich auf das Land in fast völlige Wald-Einsamkeit 
                    begeben, wo ich ungestört arbeite und einmal wieder die alte angeborene 
                    Wald-Lust recht befriedigen kann. <placename ref="1396">Hier</placename> geht es 
                    denn auch mit dem Arbeiten viel besser von Statten, und bleibt es bey 
                    dem schönen Wetter, das jetzt wieder aufs Neue angefangen, so denke ich bis 
                    <date when="1811-09-30">Ende Septembers</date> <pb source="1v"></pb>hier 
                    zu bleiben. Vielleicht gibt der Himmel Gnade, daß ich in dieser Zeit soviel 
                    beendige, als vor <date when="1811-12">Winters-Anfang</date> durchaus beendigt seyn muß, und daß mir im 
                    <date when="1811-10">Oktober</date> noch Raum zu einem kleinen Ausflug 
                    wird, den ich, wenn es nur immer möglich ist, nach keiner andern Richtung als 
                    der der Magnetnadel nehmen werde. Aber davon seyn Sie nur überzeugt, bestes 
                    Kind, wenn auch dieser <date from="1811-09" to="1811-11">Herbst</date> mir 
                    nicht zu wandern erlaubt, der nächste <date from="1812-03" to="1812-05">
                    Frühling</date> bringt mich eines Tags in den <placename ref="1399">Thüringer Wald</placename> und wenn nicht 
                    nach <placename ref="57">Gotha</placename>, doch in solche Nähe, daß wir uns ohne zu 
                    große Beschwerde von Ihrer Seite sehen und nach Lust sprechen können.</p>
                <p>Ich muß Ihnen doch auch eine kleine Beschreibung von dem <placename ref="1396">Ort</placename> 
                    meines Aufenthalts machen. Es ist ein Punkt an dem hohen Ufer der <placename ref="1443">Isar</placename>, 
                    die hier aus Schluchten und Wäldern hervorströmt, um sich dann 
                    in die weite Ebene von <placename ref="21">München</placename> zu ergießen. Der Anblick 
                    eines durch den grünen Wald strömenden reißenden Wassers, der gewiß zu den 
                    reizenden gehört, ist hier ganz vorzüglich schön. Nach allen Richtungen, auf 
                    mehrere Stunden in die Weite, ist der schönste Laubwald voll reger Thiere, 
                    Hirsche, Rehe und Vögel. Etwas wild sieht es freylich aus, aber in der Art wie 
                    es gefällt. Es ist ein Punkt, der Ihnen gefallen würde, der auch zu den 
                    Lieblingsplätzen der Münchner Welt gehört, die eine Viertelstunde von dem 
                    Bauernhof, den ich bewohne, ihren sonntäglichen Belustigungsort hat.</p>
                <pb source="2r"></pb><p>Sind Sie denn auch noch auf dem Lande, oder umfängt Sie schon 
                    wieder die Stadt? Wo Sie seyn mögen, meine Gedanken sind recht oft bey Ihnen, 
                    ja ich muß sie mit Gewalt zügeln und halten, da ich sie zu meinen jetzigen 
                    Arbeiten so sehr als möglich in der Nähe haben muß. Ich weiß nicht, liebe 
                    Pauline, ob Sie von dem jetzigen GelehrtenLeben einen Begriff haben; es hat 
                    mit dem Leben des Kriegers noch die meiste Ähnlichkeit. Der Künstler stellt 
                    sein Werk als etwas von ihm selbst unabhängiges hin, das unmerklich die Gemüther, 
                    auch die nichtwollenden, an sich zieht und allmählig sich verähnlicht. Der 
                    Gelehrte, der eine Überzeugung ausspricht und sie geltend machen muß, setzt 
                    zugleich seine Persönlichkeit daran, und wir Philosophen vollends sind die 
                    eigentlichen Krieger im Reiche der Intelligenz. Bei einer Bewegung und Unruhe, 
                    wie sie in die Köpfe gekommen ist, läßt sich an keinen Frieden denken, und auch 
                    hier liegt alles daran, sich immer thätig, rüstig und wehrhaft zu halten. Dieß 
                    ist nun freylich je nachdem man’s nimmt wieder das schönste Leben, aber die 
                    fröhlichen Gedanken an ein friedliches, stillgenießendes Leben, mit denen andre 
                    Menschen sich weiden, gedeihen nicht dabey, und fast müßte man ihre Verwirklichung 
                    von einem künftigen Leben fodern, wie die Seele des <persname ref="1983">
                    Ulysses</persname>, nach einer <bibl n="620 c–d" sameas="13080" type="literature">Erzählung</bibl> bey <persname ref="700">Plato</persname>, sich dort nichts andres ausgewählt, als das stille Leben eines 
                    Privatmanns, fern von Krieg und von Staat. – Lassen Sie mich doch immer wissen, 
                    liebe Pauline, was in Ihrer Nähe im Reich des Geistes vorgeht. Es entzündet 
                    uns, wenn wir andre <pb source="2v"></pb>um und neben uns in Thätigkeit wissen, 
                    und hier herum ist fast nichts zu gewahren, als der behaglichste Mittags- und 
                    Abendschlaf der Wissenschaft und besonders der Poesie. Nur der Musik und 
                    bildenden Künsten scheint dieser Himmel noch hold. In der ersten ward mir 
                    kürzlich ein großer Genuß durch den Gesang der Madame <persname ref="4179">
                    Milder</persname> von <placename ref="11">Wien</placename>, die hier auch in <persname ref="4180">Glucks</persname> <bibl sameas="13369" type="literature">Iphigenia</bibl> auftrat. Ihre Stimme hat 
                    wohl nicht ihres Gleichen in <placename ref="889">Deutschland</placename>; was aber die 
                    Aufführung betrifft, die auf unserm <placename ref="1428">Königlichen Theater</placename> mit dem großen, 
                    unvergleichlichen Orchester gegeben wurde, so habe ich mich theils mit Freude
                    theils mit Verdruß an jene bescheidnen <placename ref="98">Weimarischen</placename> 
                    Aufführungen erinnert, die ich <persname ref="156">Goethe</persname>’n zu danken 
                    hatte, als er einst, in einer trüben Zeit, mich über die 
                    <date from="1800-12-24" to="1800-12-26">Weihnachten</date> bey sich 
                    hatte; ich habe auch da wieder geseh’n, auf welchen stillen, fast unmerklichen 
                    Eindrücken und leisen Zusammenstimmungen die Wirkung eines Kunstwerks beruht. 
                    – Es geht doch nichts in der Musik über diese göttliche Iphigenia, die mich 
                    jedesmal neu erquickt.</p>
            </div>
            <div>
                <p>Leben Sie wohl beste Pauline! Herzliche Grüße an alle die 
                    Ihrigen; wie oft denke ich an Sie alle! Gedenken Sie auch meiner, doch nicht 
                    bloß in Gedanken auch in Werken, worunter ich hier, bestehen sie gleich 
                    aus Worten, Briefe meyne, die von Ihnen zu bekommen, immer ein Fest ist</p>
                    <p>Schelling -</p>
                    <p><placename ref="1396">Hessellohe bey München</placename> <date when="1811-08-18">18. Aug˖<expan>ust</expan> 11</date>.</p>
                
            </div>
            <div>
                <p>(Addressiren Sie trotz dessen immer nach München, wenn Sie mir bald schreiben 
                    sollten)</p>
            </div>
		</body>
	</text>
</TEI>