<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 6585</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/6585">https://schelling.badw.de/text/6585</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/6585">https://schelling.badw.de/doc/6585</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p><placename ref="96">Jena</placename>, <date when="1817-12-26">26 Dec˖<expan>ember</expan> 1817</date>.</p>
            </div>
            <div>
                <p>Meinen letzten <bibl sameas="6584" type="document">Brief</bibl> an Sie, Mein theurer, 
                    hochverehrter Freund, schrieb ich in Eile, und nehme mir heute die 
                    Zeit, sowohl Ihren <bibl sameas="8476" type="document">früheren</bibl> vom <date when="1817-11-13">13 Nov˖<expan>ember</expan></date> als Ihren <bibl sameas="8485" type="document">
                    letzten</bibl> vom <date when="1817-12-06">6 Dec˖<expan>ember</expan></date> 
                    ausführlichst zu beantworten.</p>
                <p>Mit den Vorwürfen, von denen ich in meinem letzten <bibl sameas="6584" type="document">Briefe</bibl> redete, war es 
                    nicht in dem ernsteren Sinne gemeint. Alles was <hi rend="u">Sie</hi> mir sagen, 
                    kann nie eine beleidigende Wirkung haben, obgleich von der andern Seite 
                    Ihre Misbilligung und Ihr Tadel mir weher als von irgend Jemand thut. – 
                    Je selbstständiger man wird, desto weniger findet man andere 
                    selbstständige Menschen, die mit einem übereinstimmen, desto isolirter 
                    wird man im Leben und in der Wissenschaft, und desto schmerzlicher wird 
                    jede neue Discrepanz, welche sich, sei es auch nur in Ansichten und 
                    Meinungen, zwischen zwei sich rein achtenden Menschen erheben. So kann 
                    mir gerade Ihr Urtheil, Mein theurer Freund, am wenigsten und um so 
                    weniger gleichgültig sein, je mehr ich Ihnen mit der innigsten Achtung 
                    zugethan bin, und einen je größeren Werth Ihr Urtheil für mich hat, und 
                    leider muß ich nach Ihren letzten beiden Briefen eine Differenz 
                    besonders unserer wissenschaftlichen Ansichten, nur zu sehr fürchten.</p>
                <p>Bei meinem <bibl n="Bd. 1" sameas="8236" type="literature">Systeme</bibl> bemerken Sie zuerst, in der 
                    früheren Ausgabe die Anerkennung eines gründlicheren, specielleren 
                    Dualismus gefunden zu haben. – Ich leugne nicht, daß das Grundprincip 
                    sehr allgemein gestellt ist (es ist wörtlich aus meinen früheren »<bibl sameas="16994" type="literature">Grundzügen</bibl>« 
                    abgedruckt) indessen scheint es mir einestheils, daß uns hieraus die 
                    Anknüpfungen an ähnliche Lebensverhältniße gegeben sind, wie Sie denn 
                    auch mit Recht bemerken, daß das Princip der Oscillation beßer Princip 
                    der Physik heißen könne. – Gerade hiermit ist ja aber die Krankheit dem 
                    bisherigen einseitigen Standpuncte entnommen, und als eine rein physische, 
                    (oder meinetwegen physikale) Erscheinung dem Leben überhaupt untergeordnet; 
                    auch sehe ich nicht ein, wie man desWegen die Krankheit specieller 
                    definiren könne. Alle bisherigen Definitionen, wenn sie von der <persname ref="2174">Galenischen</persname> der <foreign xml:lang="el">αμετρια</foreign> abgehen, 
                    und specieller werden, fassen immer nur einzelne untergeordnete Erscheinungen 
                    auf, <unclear cert="high">reissen</unclear> sie aus der Unterordnung unter die 
                    <pb source="1v"></pb>allgemeinen Gesetze des Lebens, und somit die Medicin 
                    aus der Unterordnung unter der Physiologie im allgemeineren Sinne.</p>
                <p>Sie möchten mich verkennen, wenn Sie glauben, daß mein in den letzten 
                    Jahren mehr practisch thätiges Leben mir eine oberflächlichere Richtung 
                    gegeben hatte. Nachdem mir die allgemeinen Gesetze des Lebens klarer 
                    erschienen sind, habe ich allerdings versucht, das Leben in allen seinen 
                    Gestaltungen und Formen denselben zu unterordnen; meinen Sie hiemit
                    die allgemeinere Richtung, über welche der speciellern in der Tiefe 
                    Eintrag gethan, so gebe ich Ihnen im ersteren Recht, im letzteren nicht, 
                    weil ich mich immer mehr überzeuge, daß die leztere ohne die erste nur 
                    zur Einseitigkeit führt. In dieser Hinsicht, abgesehen von dem politischen 
                    Genuß, sind mir meine Feldzüge von unendlichem Nutzen gewesen, weil Sie 
                    mir und mit der größeren Sicherheit der Lebensansicht überhaupt auch
                    eine größere Sicherheit und Tiefe in der Ansicht des Einzelnen gegeben 
                    haben. – Jedoch, dieß möchte wohl eher in einem mündlichen Gespräche, 
                    als hier ausgemacht und verständigt werden.</p>
                <p>Schmerzlicher ergreift mich die entstehende Discrepanz unserer 
                    wissenschaftlichen Grundansichten, über die ich kaum reden kann, und 
                    die ich in Ihrer Bemerkung über <bibl n="Bd. 1. S. 732–736" sameas="8236" type="literature">§. 746</bibl> klar ausgesprochen 
                    finde. – Wenn dieser § von der ganzen im Persönlichen
                    befangenen glaubigen Welt widersprochen würde, so hätte ich gerade bei 
                    Ihnen volle Zustimmung erwartet, da er nur auf Ihre früheren Ansichten 
                    beruht, und nur in strenger Wissenschaftlichkeit begründet ist. Ich bin 
                    hierüber mit mir ganz im Reinen, nicht ohne große Opfer mancher früheren 
                    Ansichten, und ich verdanke dieß zur Erkenntniß kommen vorzüglich Ihnen. 
                    Es handelt sich hier nicht darum wer Recht hat, denn dieß kann meinem 
                    persönlichen Interesse ganz gleichgültig sein, sondern um die hiermit 
                    gestoerte Harmonie unserer Ansichten. Geben Sie mir einen streng 
                    wissenschaftlichen Beweis ihrer Ansicht, und ich gebe gerne die Meinige 
                    auf, da die Wahrheit mir heilig und kein Opfer mir für dieselbe zu groß 
                    ist.</p> 
                <p>Dieß führt mich nun auf mein <bibl sameas="55" type="literature">Archiv f˖<expan>ür</expan> den 
                    Thier˖<expan>ischen</expan> Magnet˖<expan>ismus</expan></bibl>. In Ihrer Bewertung <pb source="2r">
                    </pb>daß wir hätten sollen die Thatsachen mehr critisch sichten vor der 
                    Aufnahme, möchten Sie, was die <persname ref="4970">Nicksche</persname> <bibl n="Bd. 1. St. 2. S. 1–164" sameas="55" type="literature">Geschichte der Krämerin</bibl> betrifft, 
                    Recht haben. Da man indessen diese critische Sichtung nur an Ort und 
                    Stelle gründlich vornehmen kann, weil man dazu alle Nebenumstande genau 
                    kennen muß, so fällt dies vorzüglich <persname ref="111">Eschenmayer
                    </persname> anheim. Ich, als entfernter Mitherausgeber war <hi rend="u">hierbei</hi> 
                    passiv. – Habe ich <persname ref="571">Fr Baader</persname> Unrecht in 
                    meinem <bibl n="Bd. 1. St. 3. S. 113–119" sameas="55" type="literature">Urtheil</bibl> gethan, wenn er, was man nicht aus seiner <bibl sameas="16614" type="literature">Schrift</bibl> lernt, 
                    mehr wie diese aussagt, der doctrinellen Erkenntniß zugethan ist, so 
                    thut es mir leid, obgleich denn die Schuld, wie Sie selbst bemerken, an 
                    dem ihm mangelnden Talent der wissenschaftlichen Entwickelung liegen 
                    möchte. – Allein ich fürchte, daß auch hier die Motive Ihres Urtheils 
                    tiefer liegen dürften, nemlich in der Discrepanz unserer 
                    wissenschaftlichen Ansicht überhaupt. – Ueber meine Ansicht der 
                    Wissenschaft in der Anwendung derselben auf die Erscheinung des 
                    th˖<expan>ierischen</expan> M˖<expan>agnetismus</expan> habe ich mich in einer <bibl n="Bd. 2. St. 2. S. 63ff." sameas="55" type="literature">Abhandlung im 2 
                    B˖<expan>and</expan> 2 Stück</bibl> des Archivs unumwunden erklärt. Ich kann nunmahl 
                    meiner Natur nach, allen mysteriösen Ansichten nicht beistimmen,
                    sobald sie, anstatt aufs Wissen sich auf den Glauben stützend, jenes 
                    verdrängen wollen. Ich wünschte, daß Sie mir über Ihre Ansicht des 
                    th˖<expan>ierischen</expan> M˖<expan>agnetismus</expan> einen Aufsatz für mein Archiv zukommen 
                    ließen, – denn diese furchtbare Erscheinung verdiente wohl, daß auch 
                    Sie ihr einige thatige Aufmerksamkeit widmeten. Um ihren Untergang in 
                    der Meinung der Welt ist mir nicht mehr bange, denn was wahr ist, wird 
                    wohl bestehen, früh oder spät, wenn es an der Zeit ist, aber uns ist 
                    an der Art des Bestehens gelegen.</p>
                <p>Dieß Ihr langes Stillschweigen in Allen Sachen erinnert mich an unsere 
                    politischen Verhaltniße. Alle Ihre treuesten Freunde können es Ihnen 
                    kaum verzeihen, daß Sie vor großer Zeit so scheinbar müßig zugesehen 
                    haben. Man trug sich <date when="1814">vor 3 Jahren</date> mit einer Antwort von Ihnen auf eine 
                    Aufforderung auch zum allgemeinen <unclear cert="high">Zweck</unclear> mitzuwirken »<q>Mein 
                    Reich ist nicht von dieser Welt.<bibl resp="Joh 18,36" type="bibel"></bibl></q>« Hierauf gründete sich die Aeußerung 
                    meiner Freude in meinem letzten <bibl sameas="6584" type="document">Briefe</bibl>, daß Sie der öffentlichen Sache 
                    nicht fremd geworden seien, und ich freue mich <hi rend="u">recht herzlich</hi> 
                    auf die Erscheinung des Ganzen Ihres <bibl sameas="8404" type="document">Systemes</bibl> von <unclear cert="low">###</unclear>. Wohl ist ein 
                    blutiger <pb source="2v"></pb>Kampf auch im Reiche der Wissenschaft wie im 
                    Leben von nöthen, wenn etwas Besseres kommen, und man sich einem mehr denn an <placename ref="889">Deutschland</placename> verzweifelnden 
                    Glauben hingeben soll, daß in der Wanderung von Osten nach Westen 
                    Deutschland abzusterben beginne, und nur in <placename ref="891">
                    Amerika</placename> die Entwickelung der Gehirne zu erwarten 
                    sei, ein Glaube, dessen ich, wie ich offen gestehe, noch immer nicht 
                    ganz Meister werden kann.</p>
                <p>In unserer <placename ref="1807">Wartburg</placename>sangelegenheit hegen Sie 
                    noch immer den irrigen Glauben, als wie das Ganze nur ein abgekartetes 
                    Spiel gewesen sei, um unsern <persname ref="574">Grosherzog</persname> 
                    zu verherrlichen. Das ist es bestimmt nicht. Sie scheinen <persname ref="1165">Fries</persname> als das Hauptwerkzeug im Sinne zu haben. 
                    Ich stehe in keinen besonderen Verhältnißen mit demselben, aber hierin 
                    thut man ihm Unrecht. Einige seiner Schüler haben zwar das große Werk 
                    geführt, und ihm nachgesprochen, allein Fries selbst ist an der 
                    Anordnung des Festes ganz unschuldig. Er war selbst am <date when="1817-10-15">15 Oct<expan>ober</expan></date> noch unschlüßig, ob er nach <placename ref="452">Eisenach</placename> gehen sollte, und nur mein Beispiel bestimmte 
                    ihn. – Jedoch möchten alle diese Einzelheiten nicht hinreichen, das 
                    Gegentheil Ihrer Meinung zu beweisen, die nur durch die genaue 
                    Kenntniß aller Umstände wiederlegt werden kann, die aber leider 
                    selbst die Cabinette ergriffen hat. – Die Wartburgsversamml<expan>un</expan>g war 
                    eine herrliche, einzige Erscheinung, ganz im Sinne, wie Sie, daß sie 
                    hätte sein können, <unclear cert="high">andeuten</unclear>, aber man hat das Exoterische 
                    begriffen weil das Esoterische, Heiligere, weniger verstanden wird, 
                    es fehlte nicht viel, daß wir uns alle schämen müßten, bei einer der 
                    edelsten Erscheinungen der Zeit gegenwärtig gewesen zu seyn. – So 
                    hat man denn auch von Seiten der Höfe ein förmliches 
                    Vehmgericht über unsern Grosherzog gehalten, der im Ganzen bei der 
                    Wartburgsgsangelegenheit sich passiv verhalten hat; <persname ref="3928">Hardenberg</persname> und <persname ref="3150">Cichy</persname>
                    letzterer mit einem eigenhändigen Schreiben des <persname ref="2545">Kaisers</persname>, forderten 
                    vom Grosherzog Rechenschaft seines Benehmens, wegen Wartburg, 
                    Preßfreiheit p. – Indessen ist mir nicht bange, daß die 
                    Wartburgsfeierlichkeit ihren Werth auch noch bei dem gegenwärtigen 
                    Geschlechte sich vindiciren wird. – Zu diesem Zwecke habe ich eine 
                    <bibl sameas="17012" type="literature">Beschreibung</bibl> derselben zum Drucke ausgearbeitet, die aber noch bei 
                    Hardenberg liegt, der sie zur Durchsicht verlangte. Ob ich mich in 
                    derselben über die Allgemeinen Verhaltniße aus<pb source="3r"></pb>laße, wovon ich in meinen früheren Briefen sprach, weiß ich noch 
                    nicht, da unsere Preß-Freiheit sehr problematisch geworden ist.</p>
                <p><persname ref="131">Okens</persname> <bibl sameas="64" type="literature">Isis</bibl> war
                    inhibirt, bei Gelegenheit der Embleme, mit welchen er in 
                    seiner <bibl n="1817. Nr. XI u. XII. St. 195. Sp. 1553–1559" sameas="64" type="literature">Beschreibung von Wartburg</bibl> die Verbrannten geziert hatte. 
                    Man klagte ihn aber der <hi rend="u">Gotteslästerung</hi>, des <hi rend="u">Hochverraths
                    </hi>, des <hi rend="u">Majestätsverbrechen</hi>, alles in der Isis 
                    ausgesprochen, vorzüglich in seiner Critik unserer landständischen 
                    Verfassung an. Die Isis ist wieder freigegeben, der Proceß aber 
                    noch nicht entschieden, noch glaube ich daß er mit einer 
                    policierlichen Geldstrafe davon kommen wird. – Das <bibl sameas="17013" type="literature">weimarische Oppositionsblatt</bibl> ist nun inhibirt, auf 
                    Reklamation von <placename ref="1092">Oesterreich</placename>. So drücken 
                    in dem freien <placename ref="889">Deutschland</placename> die Großen auf die Kleinen, welche 
                    letztern glauben frei zu sein!</p>
                <p>Haben Sie bei der <placename ref="1807">Wartburg</placename>sfahrt genauere Indicia geheimer 
                    Umtriebe, so theilen Sie mir sie mit, da mir an Eruierung der 
                    Wahrheit unendlich viel liegt. Ich bin mit unseren Studenten, 
                    mit ihrem Geiste und Treiben sehr genau bekannt, genauer als 
                    vielleicht einer meiner Collegen, aber ich habe auch nicht den 
                    mindesten Verdacht solcher Vorgänge. <persname ref="1165">Fries</persname> handelt unvorsichtig, 
                    und die Welt nicht kennend, sich von seinen Gefühlen hinreissen 
                    lassend. Er hat gegen seine Erwartung hier ein großes Publikum 
                    gefunden, und sich hierauf verleiten laßen, als Sprecher 
                    aufzutreten. Aber er ist sicher nicht Werkzeug höherer Hand. Im 
                    Gegentheil hat ihn nur das hiesige Oberappellationsgericht von 
                    einer Anklage auf Majestätsverbrechen, wegen Mitwissen an der 
                    Verbrennungsgeschichte retten können, und in <placename ref="98">Weimar</placename> ist man auf 
                    ihn, wegen seiner Unvorsichtigkeit mit Recht sehr aufgebracht. 
                    Er kennt nicht manche frühere Vorgänge, sonst würde er nicht so 
                    offen von geheimen Bünden schwatzen.</p>
            </div>
            <div>
                <p>Somit schließe ich diesen langen Brief. Leben Sie 
                    wohl. Seien Sie überzeugt daß keine Ihrer Aeußerungen meiner 
                    unbedingten Ergebenheit Eintrag thun, noch meine 
                    freundschaftliche Verehrung vermindern können, mit derer ich 
                    Ihnen von ganzem Herzen zugethan bin.<lb></lb>
                    Ihr</p>
                <p>Dr. DG Kieser</p>
            </div>
		</body>
	</text>
</TEI>