<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 7516</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/7516">https://schelling.badw.de/text/7516</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/7516">https://schelling.badw.de/doc/7516</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
    <div>
        <index n="6"></index><pb facs="1r"></pb><p><note place="left">I)</note>Dem zufällig Seyenden scheint zunächst nur das
            nothwendig Seyende entgegenstehen zu können; daher, der vom Zufälligen sich abwendet, gleichsam blindlings
            und unwillkührlich diesen Begriff erfaßt, den Begriff des schlechterdings nicht nicht seyn könnenden seyn 
            Müssenden. Allein, was in andern Fällen wahrzunehmen ist, zeigt sich auch hier, nämlich daß es eine andre 
            Ordnung der Begriffe ist, nach welcher sie im Bewußtseyn hervortreten und eine andere, welche durch ihre 
            Natur bestimmt wird.</p>
        <p>Denn, wenn ich den Begriff des nothwendig Seyenden setze, so ist mir damit weiter nichts gegeben, als der 
            Begriff von einem, das nothwendig seyend ist; aber dieses ist eine bloße Bestimmung desselben in Ansehung 
            des Seyns, eine bloß objective, aber was es <hi rend="u">an sich</hi> ist, dieses nothwendig Seyende, oder was das 
            <hi rend="u">Subject</hi> ist dieses nothwendig Seyenden ist mir damit nicht gegeben.</p> 
        <p>Frage ich nun, nicht was das nothwendig seyende, sondern was <hi rend="u">das</hi> ist, <hi rend="u">welches</hi> das nothwendig
            Seyende <hi rend="u">ist</hi>, so leuchtet gleich ein, daß dieses nicht wieder das nothwendig Seyende, das seyn Müssende seyn 
            kann, und da ebensowenig das Zufällige, nur das <hi rend="u">seyn Könnende</hi> oder das eine lautere Freyheit ist 
            zu seyn.</p> 
        <p>Wenn aber das, was das nothwendig Seyende ist, nur das seyn Könnende seyn kann, so ist freylich klar, daß 
            dieses Subject des nothwendigen Seyns, <hi rend="u">wenn es erst seyend gesetzt wird</hi>, nicht als das zufällig seyende, 
            sondern nur als das an sich, wesentlich und nothwendig seyende gesetzt werden kann.</p>
        <p>Aber wie komme ich denn dazu, es seyend, d.h. objectiv zu setzen, da es an sich nur <hi rend="u">seyn könnend</hi> nur 
            <hi rend="u">Subject</hi> ist?</p>
        <p>Denn wenn gefragt wird, was das Subject des Seyns ist – nicht aber <pb facs="1v"></pb>des nothwendigen oder 
            zufälligen – sondern des Seyns schlechthin, so kann dieses, wie von selbst einleuchtet, als solches nicht 
            schon seyend <hi rend="u">seyn</hi> (in <hi rend="u">diesem</hi>, so eben bestimmten Sinn, werden wir es das nicht Seyende nennen können);
            es ist so zu sagen vorerst der bloße Titel zu einem Seyn, oder angemeßner ausgedrückt, die innre, 
            ursprüngliche, lebendige Möglichkeit des Seyns.</p>
        <p>Nun ist der schlechthin erste oder vielmehr absolute Begriff unstreitig eben der des lautern Subjects,
            nicht insofern es dieses oder jenes, (z.B. nothwendig seyend) sondern sofern es eben Subject d.h. lautres 
            Seyn-Können ist.</p>
        <p>Dieses Subject ist denn freylich – wir halten uns bis jetzt an den bloßen reinen Begriff desselben – das 
            Subject also ist, diesem <hi rend="u">Begriff</hi> nach, nicht nur das seyn Könnende, sondern eben dieses ist auch 
            das, was seyn <hi rend="u">muß</hi>, und damit wir gleich den dritten möglichen hinzusetzen, das was seyn <hi rend="u">soll</hi>. 
            Denn was in aller Welt kann seyn, das seyn <hi rend="u">soll</hi> oder dem gebührt zu seyn, als dieses lautre Subject 
            alles Seyns, das nicht sowohl seyend, als das <hi rend="u">Seyende selbst</hi> (<foreign xml:lang="el">αὐτὸ τὸ ὌΝ</foreign>) ist 
            und allein <hi rend="u">sofern</hi> es das seyn Könnende ist*)<note place="left">*) NB. eigenschaftlich</note>, kann es nicht 
            das Seyn Müssende seyn, wie von selbst einleuchtet und nicht das seyn Sollende. <note place="left">*)) Könnte man nicht 
            davon anfangen, daß das nicht nicht seyn Kön˖<expan>nende</expan> (von dem allerdings anzufangen) nur das <hi rend="u">seyn Kön˖<expan>nende</expan></hi> 
            seyn kann (incoercibler Geist, Freyheit zu seyn) – also: das seyn Müss<expan>ende</expan>. Aber nicht <hi rend="u">als so</hi> sondern 
            <hi rend="u">als</hi> das seyn K˖<expan>önnende</expan> – oder das seyn Müss˖<expan>ende</expan> das es ist und nicht ist. Damit es das seyn Müss˖<expan>ende</expan> 
            sey, muß es objectiv (<hi rend="aq"><foreign xml:lang="la"><unclear cert="low">###</unclear> loco</foreign></hi>) gesetzt werden.</note>Denn es sey – das seyn Könnende,
            so <hi rend="u">kann</hi> es also seyn (es versteht sich daß auch nichtseyn), Seyend aber, ist es nicht 
            mehr das seyn kann und nicht seyn <hi rend="u">kann</hi> d.h. das lautre Subject, sondern das seyn und nicht seyn <hi rend="u">konnte</hi>,
            d.h. das zufällig Seyende (ein größerer Umsturz läßt sich nicht denken). Nun ist es aber das seyn Sollende 
            nicht unbeschränkter Weise, sondern nur <hi rend="u">als</hi> das lautere Subject. Offenbar also, sofern das seyn Könnende,
            ist es vielmehr das nicht seyn Sollende. Und dasselbe <pb facs="2r"></pb>gilt auch umgekehrt; nämlich inwiefern 
            das seyn Müssende kann es nicht das seyn Könnende seyn und inwiefern das seyn Sollende, kann es weder das seyn 
            Könnende noch das seyn Müssende seyn.</p>
        <p>Wir können auch jenes Erste, was vom bloßen Begriff des Subjects gesagt worden umkehren; nämlich <hi rend="u">Alles</hi>,
            was es nur sey, seyn Könnendes oder seyn Müssendes oder seyn Sollendes, kann an sich nur das Seyn-Könnende seyn,
            denn es kann Einmal nichts Anderes <hi rend="u">seyn</hi>, als das Subject des Seyns, d.h. das Seynkönnende. Wenn nun alles 
            (auch das seyn Müssende und das seyn Sollende) <hi rend="u">an sich</hi> nur das Seynkönnende seyn kann, so folgt, daß 
            dieses <hi rend="u">an sich</hi>, d.h. gradezu, unbedingt, grundlos nur Eines seyn könne – nämlich eben das seyn Könnende;
            wie es aber das seyn Müssende sey, von welchem (dem seyn Müssenden) daß es <hi rend="u">sey</hi> gar keine Frage seyn kann, 
            da es seyn <hi rend="u">muß</hi>, sondern nur, wie das, was es allein seyn kann, das seyn Könnende es seyn könne – ferner 
            wie es das seyn Sollende sey, von dem, daß es sey ebenfalls unzweifelhaft ist und nur gefragt wird, wie das, 
            was es allein seyn kann, das Seynkönnende, es seyn könne – wie es also das seyn Müssende und das seyn Sollende 
            sey, davon ist einleuchtend, daß erst Grund gegeben werden muß.</p>
        <p>Hieraus folgt also auch, daß wir als schlechthin Erstes nicht das seyn Müssende noch das seyn Sollende sondern 
            eben nur das <hi rend="u">seyn Könnende</hi> setzen können. Denn es ist alles an sich = dem seyn Könnenden. Daß also 
            das Erste das bloß seyn Könnende, das in seiner Bloßheit gesetze Subject selbst sey bedarf keines Grundes,
            versteht sich von selbst, gilt unbedingt nach dem bloßen Gesetz der Identität (A=A), von dem <persname ref="154">Fichte</persname> schon, im 
            Anfang der Wis<pb facs="2v"></pb>senschaftslehre scharfsinnig bemerkt, daß <q>es kein <hi rend="u">Seyn</hi> 
            aussage<bibl n="S. 5" sameas="2575" type="literature"></bibl><bibl n="Bd. I,2. S. 256" sameas="6360" type="literature"></bibl></q>, 
            oder nach dem des Widerspruchs. Denn da das Subject des Seyns d.h. eben das Seynkönnende 
            <hi rend="u">an sich</hi> auch das seyn Müssende und das seyn Sollende ist,</p>
        <p>Und so geben wir denn im Anfang wohl das seyn Müssende auf, indem wir nur das Seynkönnende 
            d.h. das allgemeine Subject setzen, sofern es das seyn Könnende ist. Allein indem wir so das bloße nackte Seynkönnende 
            oder Subject setzen, machen wir <pb facs="3r"></pb><note place="left">II)</note>ebendadurch möglich, daß
            das seyn Müssende sey. Denn an sich freylich kann dieses auch nur das Seynkönnende seyn, aber gegen das Subject,
            das ihm vorgesetzt ist, wird es zum nothwendig Seyenden, weil nichts das <hi rend="u">nothwendig</hi> Seyende seyn kann, 
            als eben das lautre Subject des Seyns, d.h. das seyn Könnende selbst, nicht an sich, aber sofern es objectiv,
            d.h. seyend gesetzt ist. Denn das seyn Könnende kann <hi rend="u">als</hi> dieses nicht wieder bloß seyn Könnendes seyn, 
            es muß das schlechterdings nicht nicht seyn Könnende das seyn <hi rend="u">müßende</hi> seyn. Nun ist aber das seyn 
            Könnende, das vor allem, schlechthin, gesetzt wird zwar das seyn Könnende, aber nicht <hi rend="u">als</hi> das seyn 
            Könnende. Es ist das seyn Könnende, aber das ganz in sich selbst zurückfallende, es ist’s, aber nicht 
            übergehender Weise oder so daß es sich selbst wieder als solches zurückgestrahlt wird, sondern 
            schlechthin innerlich und irreflexiver Weise; in der Einfalt nicht in der Verdoppelung (<hi rend="aq"><foreign xml:lang="la">reduplicative</foreign></hi>)
            es ist das seyn Könnende <hi rend="u">an sich</hi> im eigentlichen Sinn (wie man von einem Menschen sagt, daß er 
            einen Fehler oder eine Untugend <hi rend="u">an sich</hi> hat, die er nicht los werden kann, die er nicht gewahr wird).
            Ebendarum weil nicht <hi rend="u">als</hi> das seyn Könnende ist es wahrhaft seyn Könnendes, Freyheit zu seyn; denn 
            wär’ es <hi rend="u">als</hi> das seyn Könnende, so wär’ ihm eben dadurch <unclear cert="low">###</unclear> seyend zu werden, da es als solches 
            seyend aufhörte das seyn Könnende zu seyn. Um also das Seynkönnende (<unclear cert="low">### ###</unclear> allgemeine Subject) als das seyn 
            Müssende zu setzen kommt es bloß darauf an, es völlig objectiv zu setzen, losgerissen und befreyt von aller 
            Subjectivität. Dieß kann es aber nicht an sich seyn, denn an sich ist es eben Subject, also nur Bejahung
            <unclear cert="low">###</unclear>, nur dadurch, daß es ausgeschlossen ist von dem Ort des Subjects d.h. des Seynkönnens, also dadurch 
            daß ein andres ihm vorgesetzt ist, das das seyn Könnende Seynkönnende ist.</p>
        <p>Eben dasselbe läßt sich auch daraus einsehen, daß das entschiedne <hi rend="u">Seyn</hi> nur im Gegensatz des <hi rend="u">Könnens</hi>
            <pb facs="3v"></pb>d.h. als Nichtkönnen gedacht werden kann. Wollte man nun aber das Seyn (das entschiedne 
            lautre) als Nichtkönnen erklären, so wäre damit nichts gesagt, wenn es nicht an sich Können wäre; der positive 
            Begriff des Seyns ist selbst nur Seyn-Können, es ist ein ruhendes Seyn-Können, Seyn-Können, das nicht Seynkönnen 
            ist. Wie kann es aber Nicht-seyn-Können seyn, da an sich Seynkönnen, wenn es nicht gehindert, wenn ihm nicht 
            unmöglich gemacht ist, dieß zu seyn. Wodurch aber unmöglich, wenn nicht durch ein ihm Vorausgesetztes.</p>
        <p>Fragt man also nach dem Grund des Unterschieds zwischen dem seyn Könnenden und dem seyn Müssenden, so ist klar, daß 
            dieser Unterschied bloß auf dem Verhältniß beruht, das <persname ref="701">Aristoteles</persname> schon als das der
            <bibl n="190b 27" sameas="16628" type="literature">bloßen Beraubung (<foreign xml:lang="el">στέρησις</foreign>)<bibl n="1061a 20-28" sameas="16569" type="literature"></bibl></bibl> 
            bezeichnet hat. Nämlich das seyn Könnende ist an sich das seyn Müssende und das seyn Müssende ist das das seyn Könnende, 
            und doch ist jenes nicht dieses und dieses nicht jenes, wodurch keine Verneinung oder Verminderung der Wesentlichkeit in 
            dem einen oder andern gesetzt wird, wohl aber macht dieses an sich unwesentliche, auf das Wesen keinen Bezug habende, Verhältniß den 
            in andrer Hinsicht himmelweiten Unterschied, daß das eine das seyn Könnende das andre das Seyn Müssende ist.</p>
        <p>Nun läßt sich auch wohl zum Voraus absehen daß das <unclear cert="low">###</unclear> dessen, was das seyn Könnende und dessen was das seyn 
            M˖<expan>üssende</expan> ist ein sehr verschiednes seyn werde. Man könnte daher versucht seyn zu fragen: warum, da jenes doch 
            eben das (dasselbe Subject) ist, was dieses, warum muß jenes das Erste und darum das bloß seyn Könnende seyn,
            dieses das Zweyte und dadurch das schlechthin Seyende? Allein hievon läßt sich wie leicht einzusehen kein Grund 
            angeben. Nämlich: nachdem das erste ist, so ist freylich Grund daß das zweyte das schlechthin Seyende sey; denn es 
            kann nur dieses seyn, daß aber das Erste das erste, und also das lautre seyn Könnende ist, davon läßt sich kein 
            Grund angeben, <hi rend="u">hier</hi> ist der Ungrund, das ist so weil es so ist, als ein ewig Vorausgesetztes, 
            gleichsam als eine ewige Vergangenheit, denn irgendwo muß der Grund aufhören, nämlich eben bey dem, was 
            der Grund von allem ist. Auch wäre ja damit nichts gewonnen, wenn das, was jetzt das Zweyte ist das Erste,
            und was das Erste, das Zweyte wäre, sondern es würde nur dieselbe Frage <pb facs="4r"></pb>wieder 
            zurückkehren. Es ist mit dem gegenwärtigen Verhältniß wie mit dem des Linken und Rechten im thierischen
            Körper. Denn nachdem das, was jetzt das Herz ist, Herz ist, kann das, was die Lunge ist nur Lunge seyn; aber 
            es wird niemand fragen, warum ist das, was das Herz ist, Herz und nicht Lunge und was die Lunge ist, Lunge und 
            nicht Herz? Hier, im Innern des Organismus zeigt sich
            das Linke und das Rechte wirklich verschieden; auf der Oberfläche ist auch diese Verschiedenheit wieder 
            ausgeglichen; das linke Auge ist was das rechte und das rechte was das linke, und doch ist jenes nicht 
            dieses und dieses nicht jenes, und beyde sind dadurch, daß sie sich wie das linke und rechte verhalten,
            entschiedner auseinandergehalten als selbst durch einen Unterschied des Wesens und der Form geschehen 
            könnte, da das rechte nie das linke, das linke nie das rechte werden kann; dennoch wird niemand fragen,
            warum ist dieses das linke jenes das rechte, denn würde dieß auch umgekehrt so wäre damit nichts gewonnen,
            und dieselbe Frage würde zurückkehren.</p>
        <p>Was für unsern Zweck das Wesentliche in diesem Verhältniß ist, ist daß das Erste Seynkönnende, welches 
            das bloße seyn Könnende ist, dem, welches das seyn Müssende ist, möglich macht, dieß zu seyn. Indem 
            es <unclear cert="high">nämlich</unclear> das Können auf sich nimmt, entbindet es das andere des Könnens, d.h. seiner selbst, hebt es über sich, 
            und setzt es aus sich selbst hinaus und macht es zum Gegensatz von s˖<expan>ich</expan> s˖<expan>elbst</expan>, als das nicht seyn Könnende 
            sondern schlechthin nicht nicht seyn Könnende, seyn Müssende. Das seyn Könnende also ist das Vorausgesetzte 
            (<hi rend="aq"><foreign xml:lang="la">suppositum</foreign></hi>) oder, da die Begriffe hier, wie die Worte völlig übereintreffen, das Subject (<hi rend="aq"><foreign xml:lang="la">subjectum</foreign></hi>)
            des seyn Müssenden, und inwiefern man von A, das Subject von B ist, sagt: A <hi rend="u">ist</hi> B, so müssen wir also auch sagen: 
            das seyn Könnende <hi rend="u">ist</hi> das seyn Müssende. Jenes ist das Innere, dieses das Äußere, jenes ist von diesem 
            gleichsam bedeckt und überkleidet, dieses kommt über jenes. Aber eben, weil das seyn Könnende dem seyn 
            Müssen<pb facs="4v"></pb>den das Seyn ist, ist dieses durch ein unzerstörliches und unauflösliches Band an 
            jenes geknüpft. Denn es kann das seyn Müssende nur in diesem Verhältniß seyn, nur sofern es das seyn Könnende 
            <hi rend="u">in</hi> sich, als Träger, Unterstand oder Subject voraussetzt.</p>
        <p>Nun wollten wir aber doch im Grunde keines von diesen beyden. Wir wollten nicht das Subject, die Freyheit zu 
            seyn, bloß als das Subject und insofern als das nicht <hi rend="u">Seyende</hi>. Wir wollten ebensowenig das Subject 
            als das bloß <hi rend="u">Seyende</hi>, da es aufhört Subject zu seyn, Object wird, Nichtfreyheit zu seyn. Sondern wir 
            wollten das <hi rend="u">als solches seyende</hi> Subject. Dieses ist was seyn soll, <unclear cert="high">denn</unclear> nur dem Subject als solchem 
            gebührt es zu seyn. Dadurch daß es das als solches <hi rend="u">seyende</hi>, nicht nicht seyende, ist unterscheidet sich 
            das seyn Sollende von dem Ersten, dadurch daß das <hi rend="u">als solches</hi> seyende von dem zweyten. Inwiefern das 
            <hi rend="u">als solches seyende</hi> Seynkönnende, ist es natürlich weder das seyn Könnende im Sinn des Ersten, d.h. 
            das seyn Könnende, das das nicht Seyende ist, noch das Seyende im Sinn des Zweyten, d.h. das Seyende, das <unclear cert="high">das</unclear> 
            das nicht seyn Könnende ist, sondern es ist das seyn Könnende nur als das seyn Müssende, und das seyn Müssende 
            nur als das seyn Könnende, d.h. es ist auf dieselbe Art <hi rend="u">seyend</hi> wie das seyn Müssende ist, aber so,
            daß es in diesem Seyn Freyheit zu seyn ist, und hinwiederum, es ist Freyheit zu <hi rend="u">seyn</hi>, wie das Erste, aber so, 
            daß es in diesem Seyn seyend ist wie das zweyte – nicht zufällig seyend, wie das bloße seyn Können, wenn 
            es zum Seyn übergeht, sondern wesentlich seyend und in diesem Wesen <unclear cert="low">###</unclear> Seyn, 
            doch nicht unfrey wie das Zweyte sondern frey wie das Erste, und umgekehrt frey, zu seyn, wie das Erste und doch 
            in der Unmöglichkeit in diesem Seyn das unwesentliche Seyende zu werden.</p>   
        <pb facs="5r"></pb><p><note place="left">III)</note>Nun ist für sich klar, daß das Subject dieses seyn 
            Sollenden auch wieder nur dasselbe seyn kann, das auch das seyn Könnende und das seyn Müssende ist, denn 
            wenn das seyn Sollende ist, so ist ja nur das <hi rend="u">als solches seyende</hi> Subject, die als solche 
            seyende Freyheit zu seyn. Es ist aber ferner klar, daß diese Freyheit zu seyn, wenn Freyheit zu seyn,
            nothwendig nicht das Seyende, wenn das Seyende nicht Freyheit zu seyn ist. Um also als diese auch jenes,
            als jenes diese zu seyn muß es weder diese noch jenes <hi rend="u">insbesondere</hi> seyn. Allein damit dieses 
            Weder-Noch kein leeres sey, oder anders ausgedrückt, daß es weder jenes noch diese, und doch nicht 
            <hi rend="u">Nichts</hi> sey, muß es an sich seyn, was diese sind; es muß ihm nicht an sich selbst 
            unmöglich seyn, sowohl das seyn Könnende, als das seyn Müssende zu seyn? Wodurch also kann es zu jener 
            Untrennlichkeit, zu der Unmöglichkeit das seyn Könnende anders zu seyn, denn als das seyn Müssende,
            und das seyn Müssende anders denn als das seyn Könnende, gebracht werden, als dadurch, daß es sowohl 
            vom Ort des seyn Könnenden als von dem des seyn Müssenden ausgeschlossen ist. Es muß also erstens 
            abgehalten seyn vom Ort des seyn Könnens, daß es nicht in die Tiefe kann. Könnt’ es in die Tiefe,
            so wär’ es das Seynkönnende aber das nur das seyn Könnende, d.h. das nicht als 
            solches <hi rend="u">ist</hi>, das ebendarum auch aufhören kann, es zu seyn und das zufällig Seyende werden.
            Daß es nun nicht in die Tiefe kann ist ihm nicht durch sich selbst unmöglich, denn es ist an sich 
            eben das, was das Erste und Innerste ist. Wäre, um so ungereimt zu reden, das Erste nicht, so wäre das 
            Zweyte an der Stelle da das Erste ist, und wäre das Erste und das Zweyte nicht, so wäre das Dritte 
            die Tiefe. Denn alles ist seiner Natur nach (<hi rend="aq"><foreign xml:lang="la">nisu suo</foreign></hi>) Tiefe, Abgrund, denn alles 
            ist an sich nur Es Selbst (A=A), <pb facs="5v"></pb>nichts ist an sich erhöht, über Sich (jene Tiefe)
            gehoben, es sey denn ein es Erhöhendes, es Emportragendes. Aber noch könnte es, vom Centro abgehalten,
            in der unmittelbaren Beziehung zu diesem, das schlechthin Seyende seyn, Nichtfreyheit zu seyn. Es muß
            also noch überdieß vor jeder unmittelbaren Beziehung zu dem Centro bewahrt seyn, d.h. es muß ihm
            ebenso unmöglich seyn, das Objective, als ihm unmöglich war, das Subjective zu seyn. Es muß daher 
            zwischen ihm und der Tiefe ein Mittleres seyn, das es sowohl von dieser abhalte als mit ihr verbinde.
            An den beyden Stellen, an der des seyn Könnenden, wie an der des seyn Müssenden, muß schon ein andres 
            seyn, d.h. eines das <hi rend="u">nicht Es</hi> ist, mit dem es in <unclear cert="high">jenem</unclear> ausschließenden Verhältniß 
            steht, das als Verhältniß der <bibl n="1055a" sameas="16569" type="literature">Beraubung</bibl> beschrieben worden. Da es weder an dieser noch an jener Stelle,
            also weder das seyn Könnende noch das seyn Müssende seyn kann, so bleibt ihm nichts, als das seyn Könnende 
            so zu seyn, daß es als dieses auch das seyn Müssende ist, und das seyn Müssende so, daß als dieses 
            auch das seyn Könnende, d.h. es ist genöthigt, <hi rend="u">als</hi> das Weder-Noch, als die Gleichgültigkeit 
            (Indifferenz) von beyden, <hi rend="u">als</hi> das <hi rend="u">lautre Subject</hi> oder seyn Könnende zu <hi rend="u">seyn</hi>,
            zugleich dasselbe mit sich und der Gegensatz von sich, dasselbe weil die lautre Freyheit zu seyn, den
            Gegensatz von sich weil als solches seyend.</p>
        <p>Es ist mit Einem Wort nur als das ausgeschloßne Dritte (<hi rend="aq"><foreign xml:lang="la">exclusum tertium</foreign></hi>)
            wie wir uns nach dem bekannten logischen Grundsatz ausdrücken können. Denn auch der weniger 
            Scharfsichtige sieht, wie die sogenannten drey Urgesetze alles Denkens in der That nichts anders 
            als die Gesetze <pb facs="6r"></pb>des Ur-Seyns des absoluten Subjects in seinen drey Gestalten sind.
            Denn das seyn Könnende ist es schlechthin, an sich, vermöge des bloßen Gesetzes absoluter 
            Sich-Selbst-Gleichheit (A=A), trägt aber eben als solches auch den Keim der höchsten Sich-Selbst-Ungleichheit,
            des Widerspruchs in sich. Daß es aber das seyn Müssende, Nichtfreyheit zu seyn ist, dieses fodert
            Grund, und es ist dieses nur nach dem Gesetze des Grundes (<hi lang="la" rend="aq">principium rationis 
            sufficientis</hi>), das man auch Gesetz des ausgeschlossenen Zweyten nennen könnte. Endlich das Dritte 
            das als solches <hi rend="u">seyende</hi> – seyn Könnende ist es nur zufolge des Gesetzes des ausgeschlossenen 
            Dritten (<hi rend="aq"><foreign xml:lang="la">principium excl˖<expan>usi</expan> tertii</foreign></hi>). Wenn übrigens demzufolge das Erste bloße seyn 
            Könnende das Vorausgesetzte und unmittelbare Subject des seyn Müssenden ist, das es sich anzieht und mit dem 
            es sich gleichsam selbst überkleidet, so sind wieder beyde zusammen das Subject des Dritten und so 
            das Seynkönnen (Nichts) das Subj˖<expan>ect</expan> der allg˖<expan>emeinen</expan> Magie</p>
    </div>
</body>
	</text>
</TEI>