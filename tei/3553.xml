<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 3553</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/3553">https://schelling.badw.de/text/3553</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/3553">https://schelling.badw.de/doc/3553</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p><placename ref="21">München</placename> den <date when="1813-12-18">18. Dec˖<expan>ember</expan> 1813</date>.</p>
                <p>Liebste Mutter!</p>
            </div>
            <div>
                <p>Dank sey Gott! Der kleine <persname ref="225">Enkel</persname> ist gebohren; gestern früh den <date when="1813-12-17">
                    17. Dec˖<expan>ember</expan></date> drey viertel auf 8 Uhr ist er an das Licht der Welt getreten, als eben die schönste Wintersonne 
                    am Himmel aufstieg. Alles ging gut, ob es gleich nicht ohne Schmerzen für uns alle und besonders die gute 
                    <persname ref="223">Pauline</persname> abgehen konnte. – Erwarten Sie, liebste Mutter, keine ausführliche Erzählung 
                    heute; ich bin noch zu bewegt, nur dieß daß Pauline zu unsrer Verwunderung wohl 
                    sich befindet und lange nicht so geschwächt ist, als ich nach ihrer zarten Körperlichkeit und der
                    letzten Erfahrung erwarten mußte. <date when="1813-12-16">Donnerstag</date> früh 
                    3 Uhr fing sie zuerst an, Schmerzen zu empfinden, die aber nur in großen Zwischenräumen wiederkamen, und jederzeit 
                    nur kurze Zeit währten. Den ganzen Morgen konnte sie noch abwechselnd liegen, sitzen, herumgehen und behielt auch 
                    da noch ihre Heiterkeit, als das Stündchen immer näher herbeyrückte. Tiefer dringende schmerzhafte Wehen kamen 
                    erst Nachmittags 3 Uhr. Sie hatte schon den ganzen <date when="1813-11">letzten Monat</date> viele Krämpfe; 
                    diese setzten sich nun, soviel ich verstehen kann, dem Geschäft der Geburt entgegen, bis sie durch eine Arzney, 
                    die <persname ref="4049">Fischer</persname> verschrieb gehoben wurden. Die schmerzhaftesten <pb source="1v"></pb>Stunden 
                    waren von 1–4 Uhr in der Nacht; wie sie überhaupt am meisten von den vorbereitenden Wehen litt. Ihre Gelassenheit, 
                    Standhaftigkeit und bewiesenen Verstand kann ich nie genug loben; von letzterem hatte sie mehr als wir alle und 
                    mehr selbst als die Hebamme, die sie nach Art solcher Personen wohl öfters antrieb mitzuwirken, indeß sie immer 
                    versicherte es sey noch nicht Zeit. Als die Zeit gekommen war, ließ sie es nicht an Anstrengung fehlen; und der 
                    letzte Theil der Geburt ging mit einer Leichtigkeit von Statten, die wir nicht erwartet hatten. Wir alle und 
                    Pauline selbst fürchteten uns vor diesem letzten Act; dieß allein machte sie bey den früheren Schmerzen bange, 
                    weil sie fürchtete, den noch größeren nicht aushalten zu können; aber Gott sey Dank, sobald es nur zur Geburt 
                    kommen konnte, war das Ärgste überstanden; sie zeigte auch dabey die größte Gelassenheit, alles ging auf die gute, 
                    stille Art, die ihr gewöhnlich ist. Ihre Natur hat auch bey dem Allem wieder mit der größten Regelmäßigkeit gewirkt; 
                    sie bedurfte Zeit, aber sie überwand allmälig alles, ohne Sturm, ohne heftige Bewegungen, und 3/4 auf 8 Uhr hörten 
                    wir, ohne noch daran zu denken, ohne daß sie es durch ein Wort verraten hätte, schon die Stimme des kleinen Ankömmlings. 
                    Pauline war ganz still; man konnte nicht unterscheiden, ob sie dem Schmerz oder der Wonne erlag, es war ein schöner, 
                    unvergeßlicher Augenblick. Könnte überhaupt mein Gefühl für sie einen Zuwachs bekommen, so hätte ihr Benehmen während 
                    des ganzen Vorgangs schon allein sie mir ewig werth gemacht.</p>
                    <pb source="2r"></pb><p>Nun werden Sie auch etwas von dem kleinen <persname ref="225">Enkel</persname> wissen wollen. Aber hier komme ich in Gefahr, in den Verdacht 
                    der väterlichen Eitelkeit schon jetzt zu gerathen, wenn ich Ihnen nur die Hälfte von dem schreibe, was die Leute 
                    zu Gunsten des Kleinen sagen. Es ist wahr, er ist ein vollkommnes Kind, nicht wie ich andre gesehen habe, die nur 
                    zur Noth dafür gelten konnten, ganz ausgebildet, runde, mit fest wohl ausgelegten Gliedmaßen, stark und sehr lang, 
                    daß man kaum begreift, wie der guten <persname ref="223">Pauline</persname> die Schwangerschaft so leicht, wohl aber, daß ihr der erste Theil 
                    der Geburt ziemlich schwer werden mußte. Was mir besonders an ihm gefällt, ist daß nichts Formloses, Ungebildetes 
                    an ihm ist, sondern alles wohl geformt, und daß er besonders im Gesicht schon so viel Bildung, soviel Gescheidtes 
                    und Vernünftiges zeigt, was ich freylich nicht sagen sollte, da alle Welt findet, daß er mir wie aus dem Gesicht 
                    geschnitten sey. Er hat den ganzen Kopf voll brauner Haare mitgebracht, große schöne Nägel an Zehen und Fingern, 
                    und ungemein viel Lebenskräftigkeit, die uns hoffen läßt, er werde, wenn Gott ihn uns erhalten will, durch alle 
                    Gefahren des zartesten Alters sich wohl durchschlagen. Pauline hat nichts an ihm verkürzt: er hat sein voll 
                    Gewicht und Maß, er ist mit Sorgfalt und Feinheit ausgeführt, nicht die bloße Ebauche eines künftigen Menschen,
                    sondern schon wirklich ein Mensch und noch dazu ein Knabe!</p>
                <p><persname ref="223">Mutter</persname> und <persname ref="225">Kind</persname> befinden sich so gut, daß es nach <persname ref="4049">Fischer</persname>’s 
                    Versicherung nicht besser seyn könnte. Nun muß ich Sie 
                    noch von etwas in Kenntniß setzen, was wir gethan und wovon ich nicht weiß, ob es Ihnen Pauline voraus geschrieben.
                    <pb source="2v"></pb>Ich habe bis in die letzte Zeit aus allen Kräften mich gegen das Selbst-Stillen gesetzt, weil 
                    es auch Ihre Meynung und die von P<expan>auline</expan>’s <persname ref="4377">Onkel</persname> war. Aber Fischer 
                    bestand mit einem Eigensinn darauf, der Paulinen, deren Wunsch es ohnedieß immer war, umstimmte. Ich habe nun 
                    nachgegeben, weil ich wenigstens gegen die Gründe, die von ihrem Gesundheitszustand hergenommen waren, nichts 
                    einwenden konnte. Sie haben freylich keinen Begriff von dem Grad der Stärke, zu dem sie gelangt ist, im Grund 
                    schon seitdem sie hier ist, besonders aber während der Schwangerschaft, wo es mit ihr bis ins Robuste ging. 
                    Das ungemein kräftige Aussehen des Kinds, dem gewiß nicht viele ähnliche geboren werden, legt die vollgültigste 
                    Probe ihrer inneren Gesundheit ab; und da ihr der erste Rapport, in dem sie 9 Monate lang mit dem Kind gestanden, 
                    so trefflich bekommen: so rechne ich auf eine ähnliche Rückwirkung auch für die Folge. So habe ich denn endlich 
                    nachgegeben und das Kind ist heute das erstemal angelegt worden, hat auch schon ziehen und trinken gelernt. 
                    Nur war es für die gute Pauline abermals mit Schmerzen verbunden; alles versichert jedoch, auch diese würden 
                    in wenigen Tagen vorüber seyn. Ich betrachte nun dieses Selbst-Stillen einstweilen als Probe, findet sich früher 
                    oder später, daß es dem Kind oder der Mutter nicht bekommt, so wird es eingestellt; denn der Knabe ist stark 
                    genug, um dieß auch nach den ersten 6 Wochen auszuhalten. Ängsten Sie sich also nicht darum, liebe Mutter, 
                    und rechnen Sie überhaupt darauf, daß alles geschieht, die gute Pauline zu erleichtern und ihre Kräfte zu schonen. 
                    Hände sind genug da, meine <persname ref="88">Mutter</persname>, dann hat sich die <persname ref="275">Köhler</persname> 
                    bey uns einquartiert und die ganze Zeit, auch der Geburt, bei P˖<expan>auline</expan> ausgehalten. Nochmals, Dank sey Gott! 
                    es ist bis jetzt alles über unser Denken und Erwarten gut gegangen; Er wird ferner helfen. Pauline grüßt die 
                    nunmehrige Grosmutter, Tanten, Grostanten etc. auf’s zärtlichste so wie ich; wegen der Notifications- und 
                    <bibl sameas="7466" type="document">Gevatterschreiben</bibl> müssen sie noch Geduld haben. –</p>
            </div>
            <div>
                <p>Leben Sie wohl, beste Mutter. Ihr<lb></lb>
                    guter Sohn</p>
                <p>S.</p>
            </div>
        </body>
	</text>
</TEI>