<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 2786</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/2786">https://schelling.badw.de/text/2786</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/2786">https://schelling.badw.de/doc/2786</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="2r"></pb><p>Den <date when="1816-12-22">
                    22. Dec˖<expan>ember</expan> 1816</date>.</p>
            </div>
            <div>
                <p>Ich schicke Ihnen, lieber Freund, das obige <bibl sameas="2602" type="document">
                    Brief-Fragment</bibl>, wie ich es im <date when="1816-05">May dieses 
                    Jahres</date> angefangen hatte zu schreiben, damit Sie Sich wenigstens 
                    von der damaligen guten Absicht überzeugen, Ihnen gleich wieder zu 
                    antworten. Immer wartete ich auf das fatale <bibl n="Nr. 215 v. 13.11.1815" sameas="28" type="literature">Stück vom Morgenblatt</bibl>, am Ende erhielt ich es nicht; – so kam mir 
                    zuletzt auch dieser Brief aus dem Gesicht. <date when="1816-08-16">Ende Augusts</date> <bibl sameas="6648" type="document">sandten</bibl> Seine Kön˖<expan>igliche</expan> <persname ref="444">Hoheit</persname> mir Ihren 
                    ganzen <bibl sameas="2314" type="literature">Aufsatz</bibl><bibl n="Bd. I 19. S. 315–404" sameas="6347" type="literature"></bibl> zu, mit dem Bemerken, daß Sie 
                    gewünscht haben, ich möchte die Herausgabe besorgen und daß auch 
                    S<expan>ein</expan>e˖ K˖<expan>önigliche</expan> H˖<expan>oheit</expan> damit einstimmen. Es wäre nicht 
                    anders als meine Schuldigkeit gewesen, Ihnen für das in mich gesetzte 
                    Vertrauen gleich herzlich zu danken. Glauben Sie mir, daß ich den 
                    Werth desselben innig fühlte; aber es war mir <hi rend="u">damals</hi> unmöglich zu 
                    bestimmen, wie bald ich die Herausgabe besorgen könnte. Ich dachte 
                    von Woche zu Woche dran zu gehen, aber stets neue Hindernisse 
                    <pb source="2v"></pb>setzten sich entgegen. Sie glauben nicht, wie traurig in dieser 
                    Hinsicht meine Lage in <placename ref="21">München</placename> ist; fast nicht 
                    ohne Thränen lese ich oft, was <persname ref="115">Winckelmann</persname> 
                    über seine goldne Freyheit, die Muße und Stille in <placename ref="99">
                    Rom</placename> schreibt. Endlich im <date when="1816-11">vorigen Monat
                    </date> (nachdem ich fast die Hälfte des <date when="1816-10">
                    Oct˖<expan>obers</expan></date> und <date when="1816-11">Nov˖<expan>embers</expan></date> durch den 
                    Umzug in eine neue Wohnung verloren hatte) wollte ich an die 
                    Redaction gehen; nun kam eine Krankheit, die mich an die 4 Wochen 
                    unthätig machte.</p>
                <p>Jetzt endlich ist es so weit, daß <date when="1816-12-23">morgen</date> der Druck beginnt und noch im Laufe des <date when="1817-01">
                    Januars</date> das Ganze (bey <placename ref="1182">Cotta</placename>) gedruckt 
                    erscheint.</p>
                <p>Nun habe ich Ihnen zunächst über mein Verfahren Rechenschaft zu geben. 
                    Ehe ich aber diese thue, kann ich nicht umhin, Ihnen über Ihren 
                    meisterhaften und trefflich gearbeiteten <bibl sameas="2314" type="literature">Aufsatz</bibl><bibl n="Bd. I 19. S. 315–404" sameas="6347" type="literature"></bibl>, den ich jetzt erst 
                    mit gehöriger Muße und so recht <hi rend="aq"><foreign xml:lang="it">con amore</foreign></hi> 
                    lesen und durchdenken konnte, meine Freude zu 
                    bezeugen. Es bleibt doch ewig wahr, daß wer in Einer Sache ein 
                    gescheidter Mann ist, auch in andern sich als solcher erweiset. Diese 
                    kleine Schrift trägt durchaus das Gepräge Ihres reifen Urtheils, 
                    kerngesunden Verstandes, einer recht taktfesten Dialektik und des Ihnen 
                    eigenen Humors. Ich will Sie hier nicht im Scherz, sondern in vollem 
                    Ernste loben. Mir war, als hörte ich Sie reden, und wieder kam mir 
                    in die Seele, was ich <pb source="3r"></pb>seit unserer persönlichen Bekanntschaft so oft 
                    gedacht und empfunden habe, daß ich Wenige oder Niemanden in der Welt 
                    gefunden, mit dem ich mich getraute, freyer, heiterer, geistreicher 
                    und nach völliger Herzens- und Geisteslust zu leben, als mit Ihnen, 
                    welches freylich wunderbar genug ist, da wir übrigens so ganz 
                    verschiedne Dinge betreiben, ich nämlich – – doch dieß will ich nicht sagen um nicht mich selbst zu loben – Sie aber – wir wissen ja, was Sie außer der Kunst noch treiben und wovon man besser schweigt.</p>
                <p>Nach diesem <hi rend="aq"><foreign xml:lang="fr">Preambule</foreign></hi> werden Sie nun von selbst 
                    vermuthen, daß ich bey der Bearbeitung Ihres <bibl sameas="2314" type="literature">Aufsatzes</bibl><bibl n="Bd. I 19. S. 315–404" sameas="6347" type="literature"></bibl> mit der 
                    größten Gewissenhaftigkeit zu Werk ging, nichts daran zu verderben, 
                    ihm seine Eigenthümlichkeit soviel möglich zu erhalten und nur 
                    diejenigen Verbesserungen des Styls vorzunehmen, welche 
                    schlechterdings vorgenommen werden mußten, wenn der Aufsatz gedruckt 
                    erscheinen sollte.</p>
                <p>Von selbst versteht sich, daß ich an dem Materiellen ihrer Erklärungen 
                    und Äußerungen um so weniger Etwas geändert, da es lächerlich seyn 
                    würde, als Gelehrter, der die Gegenstände nicht einmal gesehen hat, 
                    irgend etwas besser als Sie wissen zu wollen, der mit dem Auge des 
                    Künstlers anhaltend und mit Ernst diese merkwürdigen Werke betrachtet.</p>
                <p>Über einen Punct bin ich noch nicht mit mir <pb source="3v"></pb>selbst einig, was zu thun. 
                    Sie erklären sich so entschieden für die <placename ref="895">ägyptische</placename> 
                    Abstammung der <placename ref="924">griechischen</placename> Kunst, ja für die 
                    Identität des <placename ref="1585">äginetischen</placename> und des ägyptischen Styls! Dieses nun ist 
                    eine Sache, die mir mehr als zweifelhaft scheint, sobald man nämlich 
                    etwas <hi rend="u">Historisches</hi> darüber behaupten will. Denn es ist so 
                    natürlich, daß auch das Verschiedenartigste und von einander 
                    Unabhängigste in den ersten Anfängen sich ähnlich ist; z.B. die 
                    unvollkommensten Pflanzen und die unvollkommensten Thiere sind nicht 
                    zu unterscheiden (daher <hi rend="u">Pflanzenthiere</hi>), während die 
                    vollkommensten himmelweit auseinanderstehen. – In der Folgezeit aber 
                    unterscheidet z.B. <persname ref="3183">Pausanias</persname> den 
                    äginetischen Styl sehr bestimmt vom ägyptischen. Ich werde mir also vielleicht die Freyheit nehmen, über diesen 
                    Punct Ihre Äußerungen – nicht ins Gegentheil zu verändern, nur in 
                    etwas zu mäßigen.</p>
                <p>Einige Anmerkungen werde ich beyfügen, damit ich doch als Herausgeber 
                    auch etwas gethan habe; doch werden diese Anmerkungen nicht 
                    Widersprüche, sondern mehr Erläuterungen Ihrer Ansichten enthalten.</p>
                <p>Über den großen Punct (die Sonderbarkeit der Köpfe) wovon ich oben 
                    noch nach dem ersten Eindruck, da ich Ihren <bibl sameas="2314" type="literature">Aufsatz</bibl><bibl n="Bd. I 19. S. 315–404" sameas="6347" type="literature"></bibl> nur flüchtig 
                    hatte durchlesen können, geurtheilt habe, bin ich nun fast ganz 
                    Ihrer Meynung, oder vielmehr, ich habe sie jetzt erst in allen 
                    ihren Bestimmungen und einzelnen Theilen recht erwogen und 
                    verstehen gelernt. Ich glaube jetzt, Sie haben das rechte Maß 
                    getroffen, <pb source="4r"></pb>oder jene schmale Mittel-Linie, in welcher die Wahrheit 
                    liegt. Mir erscheint das Problem jetzt allgemeiner; nämlich es ist 
                    dasselbe Problem in Ansehung der ganzen Figuren: »Woher der 
                    Widerspruch zwischen dem <hi rend="u">Styl</hi> und der <hi rend="u">Ausführung</hi>?« 
                    Dieser Widerspruch ist nur natürlich bey den Köpfen und Gesichtern 
                    auffallender als bey den übrigen Körpertheilen. Es scheint mir 
                    aber nun auch gar nicht so unbegreiflich, daß jene Künstler sich 
                    vorerst in der Ausführung zu vervollkommnen suchten, und an die 
                    Ändrung des Styls erst später dachten. Daß indeß die 
                    gegenwärtigen Figuren Wiederholungen von früheren seyn könnten, 
                    und darin <hi rend="u">zum Theil</hi> der Grund jenes Widerspruchs liege, 
                    halte ich noch immer für möglich; nur müßte die Nachahmung nicht 
                    zu grob materiell sondern mehr geistig genommen werden.</p>
                <p>Ihre nachgeschickten Zusätze und Verbesserungen sind an den gehörigen Stellen 
                    eingeschaltet worden.</p> 
                <p>Ich hoffe überhaupt, Sie sollen zufrieden 
                    seyn. Mir macht es nicht wenig Freude, Sie auf diese Art in die 
                    Schriftstellerwelt eingeführt zu haben; ich zweifle nicht, daß 
                    Sie die beste Aufnahme finden.</p>
                <p>Sollten Sie mir noch etwas mitzutheilen haben, das während des 
                    Drucks noch benutzt werden könnte, so säumen Sie nicht damit.</p>
                <p>Schreiben Sie auch, wie viele Exemplare Sie nach <placename ref="99">Rom</placename> zu erhalten 
                    wünschen. Ich werde Ihnen diese, nach Angabe S<expan>eine</expan>r˖ 
                    K˖<expan>öniglichen</expan> <persname ref="444">H˖<expan>oheit</expan></persname> durch das Haus <placename ref="2132"><hi lang="la" rend="aq">Carli et 
                    Comp.</hi></placename> in <placename ref="366">Augsburg</placename> zukommen lassen.</p>
                <pb source="4v"></pb><p>Was Sie mir von der Aufnahme des neu-alt-<placename ref="889">deutschen</placename> Wesens in <placename ref="99">Rom</placename> <bibl sameas="1055" type="document">schreiben</bibl>, 
                    hat mich doch mehr noch belustigt als geärgert. Ich hoffte 
                    freylich, solche Jungen, wie der <persname ref="4944">R˖<expan>uhl</expan></persname> aus 
                    <placename ref="297">Cassel</placename> sollten mit einer tüchtigen Tracht 
                    Schläge von Rom nach Hause geschickt werden, die einzige Curart, 
                    die anschlagen möchte und deren sie werth sind. Es wird aber auch 
                    ohne das vorübergehen, zumal, <persname ref="519">Cornelius</persname> 
                    etwa ausgenommen, doch kein eigentliches Talent dieser Narrheit zu 
                    Hülfe kommt. Je nachtheiliger diese Wendung ist, desto mehr muß 
                    man wünschen, daß classische Werke wieder ein wahres Muster 
                    aufstellen. Jetzt werden Sie doch ruhig an Ihrem <persname ref="2707">Orfeus</persname> fortarbeiten? Wann hoffen Sie ihn zu vollenden? 
                    Hoffentlich bringen Sie ihn selbst nach Deutschland. Verlieren Sie 
                    über allen diesen Tollheiten Ihren guten Humor nicht, bleiben Sie 
                    Ihrer großen und rechten Gesinnung treu und lassen Sie, wenn jenes 
                    Gemälde vollendet ist, gleich eine Anzahl von Werken aufeinanderfolgen. Es kommt nur auf Ihren Willen an, um ein großes und rüstig 
                    wirkendes Beyspiel aufzustellen.</p>
                <p>Der Herr <persname ref="270">Subdiaconus</persname>, nach dem Sie so oft fragen, ist inzwischen ein vollkommner, nämlich hochmüthiger, Narr, und wie es bey solchen Leuten gewöhnlich am Ende geschieht ein schlechtes Subject geworden.</p>
                <p>Noch bitte ich, mir zu schreiben, ob ich das Honorar, das ich vom <persname ref="246">Verleger</persname> für Sie erhalte, Ihnen durch Wechsel aus <placename ref="366">Augsburg</placename> übermachen, oder <placename ref="21">hier</placename> an Herrn <persname ref="2994">von Gärtner</persname> auszahlen soll.</p>
                <p>Recht viele Grüße an <persname ref="589">Friz Gärtner</persname>; bitten Sie ihn nochmals, sich 
                    des obenerwähnten Monuments anzunehmen, danken Sie ihm vorläufig 
                    für seine schöne Zeichnung, ich bin mit dieser ganz einverstanden, 
                    ich bitte ihn sich mit <persname ref="3435">Thorwaldsen</persname> deshalb zu benehmen, denn was 
                    dieser billigt ist mir recht.</p>
            </div>
            <div>
                <p>Leben Sie wohl, geliebter
                    Giovanni. Ich bin Ihr alter getreuer</p>
                <p>Sch</p>
            </div>
		</body>
	</text>
</TEI>