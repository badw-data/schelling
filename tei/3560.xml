<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 3560</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/3560">https://schelling.badw.de/text/3560</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/3560">https://schelling.badw.de/doc/3560</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <p>A Madame</p>
                <p>Madame <hi rend="u"> Gotter née Stieler</hi></p>
                <p>à</p>
                <p><hi rend="u"><placename ref="57">Gotha</placename></hi></p>
            </div>
            <div>
                <pb source="1r"></pb><p> den <date when="1812-06-26">26. Juny 1812</date>.</p>
            </div>
            <div>
                <p>Weil Sie, geliebteste Mutter, mit der größten Gewißheit auf Briefe vom 
                    heutigen Datum zählen, so will ich trotz der Schwierigkeiten der Zeit 
                    und vieler dringender Geschäfte Ihnen nur kürzlich melden, wie es Ihren 
                    Kindern seitdem ergangen ist. Sie erinnern sich vielleicht, daß ich 
                    schon am Morgen Ihrer Abreise über ein Halsweh klagte, wovon der erste 
                    leichte Verdacht mich auf der Reise nach <placename ref="78">Lichtenfels</placename> 
                    zuerst angewandelt hatte, hernach aber den gewöhnlichen nächtlichen 
                    Erwärmungsmitteln immer wieder gewichen war. Die Anstrengungen, die 
                    abwechselnden Erhizungen und Erkältungen, des letzten Tags hatten es 
                    merklich verschlimmert, so daß ich schon 1 Stunde nach Ihrer Abreise 
                    selbst Mittel aus der Apotheke holen ließ, um ihr zu steuren. Auf 9 Uhr 
                    bat ich <persname ref="692">Marcus</persname> und <persname ref="4570">Weyer</persname>. Der liebenswürdige Leichtsinn unsrer Freunde fand keine 
                    nähere Untersuchung nöthig, und Marcus sagte: es ist unbedeutend, 
                    unterwegs werden Sie es verlieren. Sonach bestiegen wir um 10 Uhr den 
                    Wagen und fuhren ohne Sorgen per <placename ref="156">Nürnberg</placename>. 
                    Aber schon auf dem halben Wege nach <placename ref="558">Forchheim</placename> 
                    (6 Stunden von hier) hatte das Übel sich so verschlimmert, daß ich in 
                    Forchheim <pb source="1v"></pb>Halt mußte machen lassen und im Wirtshaus 
                    absteigen. Nach 1/2 Stunde Ruhe stellte sich das heftigste Fieber, und 
                    nach und nach alle beschwerliche Erscheinungen einer vollkommnen 
                    Halsentzündung ein. Ich ließ den dortigen (als Naturforscher nicht 
                    unbekannten Arzt) Dr. <persname ref="3645"><unclear cert="low">###</unclear></persname> 
                    rufen. Er verschrieb einiges. Unsre Lage will ich nicht schildern. 
                    Die Nacht wurde in den größten Beängstigungen zu gebracht. Nachdem 
                    am andern Morgen das Übel noch höher gestiegen, war <persname ref="223">Paulinens</persname> guter Rath, nach <placename ref="74">Bamberg</placename> 
                    zurückzukehren, den ich gern annahm. So fuhren wir Freytags den <date when="1812-06-19">19ten</date> Nachm˖<expan>ittags</expan> 2 Uhr wieder in den 
                    wohlbekannten Bamberger Hof ein.</p>
                <p>Von nun an bot <persname ref="692">M˖<expan>arcus</expan></persname> alle seine Kräfte auf, nicht allein mich vor 
                    Gefahr zu schützen, sondern auch die Zeit abzukürzen. Alle die 
                    leidigen und oft schmerzlichen Scenen dieser schlimmen Krankheit sind 
                    durchgespielt worden. – Und wie verhielt sich bey dem Allen unsre 
                    liebe <persname ref="223">Pauline</persname>? werden Sie fragen. Kann ich es aussprechen? 
                    Unmöglich. Mein Schmerz ging bloß auf sie, die holde Seele, die so 
                    früh den Ehstand von der Seite seiner Leiden, und zwar der 
                    beängstigendsten, kenn lernen sollte. Aber ein höherer Geist gab 
                    ihr Kraft; sie stand mir wie ein Engel zur Seite. Nie kann, nie werde 
                    ich ihr diese Tage vergessen; auch hiedurch habe ich sie als ein 
                    Geschenk des Himmels erkennen gelernt – und zu den Empfindungen der 
                    reinsten Liebe für <pb source="2r"></pb>sie gesellt sich seitdem das 
                    einer unbeschreiblichen Rührung.</p>
                <p>Liebste Mutter, Sie fangen bald an, wegen meiner in Sorgen gestürzt 
                    zu werden. Werfen Sie Ihre Sorgen auf den <hi rend="u">Herrn</hi>, wir vermögen 
                    ja doch nichts. Sie haben uns freylich, wie unsre Schutzgeister 
                    verlassen; denn seit dem Augenblick Ihrer Abreise hatten wir keine 
                    gute Stunde.</p>
                <p>Rechnen Sie darauf, daß nicht bloß in Ansehung meiner, auch in 
                    Ansehung <persname ref="223">Paulinens</persname> alles geschieht, was zu ihrer GesundErhaltung 
                    erforderlich ist.</p>
                <p>In diesen Tagen sehe ich meiner völligen Erlösung entgegen; wenigstens 
                    verspricht es der <persname ref="692">Freund</persname>. In Zeit von 5–6 
                    Tagen hoffe ich nach <placename ref="21">M˖<expan>ünchen</expan></placename> abreisen zu 
                    können. Der Himmel hat mich durch ein großes Feuer der Trübsal gehen 
                    lassen; unmittelbar aus dem Himmel in die Hölle gestürzt habe ich mich 
                    doch geistig nicht niederbeugen lassen. Beten Sie für uns; und seyn 
                    Sie übrigens was den Ausgang betrifft ganz ruhig.</p>
            </div>
            <div>
                    <p>Die besten Grüße an unser theures <persname ref="1149">Julchen</persname>, an <persname ref="1154">Cäc˖<expan>ilie</expan></persname> an 
                        alle die sich unsrer erinnern mögen.<lb></lb>
                        Der Himmel erhalte Sie gesund und beruhige Ihr Herz.<lb></lb>
                        Ihr<lb></lb>
                        treuester und ergeb<expan>en</expan>ster Sohn</p>
                    <p>Fr.</p> 
            </div>
        </body>
	</text>
</TEI>