<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 5544</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/5544">https://schelling.badw.de/text/5544</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/5544">https://schelling.badw.de/doc/5544</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
				<pb source="1r"></pb><p>Hochwohlgeborner<lb></lb>Hochzuverehrender Herr Direktor!</p>
            </div>
            <div>
                <p>Nicht ohne große Schüchternheit und nicht ohne einiges Selbstvertrauen ergreife ich zum 
                    <bibl sameas="6238" type="document">zweitenmal</bibl> diese Feder. Wohl weiß ich, wahrlich ich weiß es, an wem ich 
                    schreibe! Aber wie jene Schüchternheit die Kraft mir benehmen will, so stärkt dieses Vertrauen 
                    mir den Muth, aber eben nur dadurch, daß in ihm wie ein ewiges Christusbild Ihre erlauchte, mir 
                    <pb source="1v"></pb>ewig allgegenwärtige Gestalt tröstend lebt, Seegen <hi rend="u">auch mir</hi> zuwinkend 
                    und Gedeihen. Nur diese giebt mir <hi rend="u">mit der reinsten Verehrung zugleich den Muth</hi> des 
                    Vertrauens, der die Kraft der Dehmuth sich selbst erschafft. Aber offen muß ich reden, ganz 
                    offen und unbefangen, wenn ich mich unterstehen will, an Sie zu schreiben.</p>
                <p>Die Schrift lehrt uns: <q>Jesus bedurfte nicht, daß jemand Zeugniß gäbe von einem 
                    Menschen, denn <hi rend="u">er kannte sie alle</hi> und wußte wohl, was im Menschen war<bibl resp="Joh 2,25" type="bibel"></bibl></q>. (Joh.2,25) Mir ward das Glück, worauf ich so lange gehofft hatte, zu 
                    Theil, Euer Hochwohlgeboren zu sehen, zu sprechen. Sie kennen mich, durchschauen mich, wenn 
                    auf der ganzen Erde irgendwer. <hi rend="u">Diese</hi> Gewißheit giebt mir auch die unbefangene 
                    Sicherheit, fortzufahren in diesem Briefe, fast <pb source="2r"></pb>ohne zu erröthen, wenn 
                    dieses möglich ist bei Gedanken an Ihre Wirklichkeit, ja fast ohne zu erstaunen über die 
                    eigene Kühnheit vor mir selber. Schwankend ist meines Erdverhältnißes äußere Lage, ich muß 
                    jede Stunde den Grund, worauf ich trete, mir wie von vorne herein wiedererschaffen. Seit 
                    <date when="1817">5 Jahren</date> ist mein <persname ref="4188">Vater</persname> todt, meine 
                    <persname ref="4189">Mutter</persname>, immer kränklich, lebt kümmerlich von ihrer Pension und 
                    schwankt immer mehr dem Grabe zu. Unbekannt dem Staate bin ich ohne Unterstüzung und lebe 
                    blos durch die Thätigkeit des Selbstbewußtseins meines Rechtes, das ich habe, von der 
                    Wirklichkeit zu verlangen, daß sie meine Existenz erhalte. Aber um meine Existenz ist mir nur 
                    zu thun, weil mir um meine wahre und geistige Wirklichkeit zu thun ist und meines Lebens 
                    einziger Gedanke in dieser Beziehung war von jeher der, mich zum <hi rend="u">akademischen Lehrer der 
                    Philosophie</hi> zu bilden. Euer Hochwohlgeboren werden <pb source="2v"></pb>darin gewiß weder die 
                    Sprache der Klage, noch der Anmaßung finden. Seit ich mit dem wirklichen Gotte, mit der 
                    Freiheit wieder versöhnet bin durch – die Wissenschaft, habe ich noch nichts gewollt, was 
                    ich nicht vollbracht hätte. Die Worte: &quot;der Mensch wisse nur, was er ist und er wird 
                    sein, was er solle&quot; gelten auch hier. Euer Hochwohlgeboren durchschauen meine etwanige 
                    Fähigkeit oder Unfähigkeit, Würdigkeit oder Unwürdigkeit zu diesem Amte. Darf ich es nicht 
                    wagen, in Form einer Frage die Bitte auszusprechen, ob Sie in Ihrer Gnade auch die 
                    Gewogenheit haben wollten, auch mich in Beziehung auf jenes Amt zu empfehlen oder ein 
                    Zeugniß mir auszustellen über jene Fähigkeit. Mögte dieses Briefes vielleicht zudringliche 
                    Offenheit den <hi rend="u">versöhnenden</hi>, wie <pb source="3r"></pb>Ihre Erkenntniß den begeisternden 
                    Zug haben, der mich in Ihrer Gewogenheit wenigstens erhält. <placename ref="128">Erlangen</placename> 
                    wäre mir schon wegen dieser Nähe der erwünschteste Ort. Von seiner Excellenz, dem Herrn 
                    Minister <persname ref="3692">von Stainlein</persname> an Herrn Minister <persname ref="409">
                    von Zentner</persname> gnädigst empfohlen – <hi rend="u">daß ich ganz unverholen spreche</hi> – fehlt mir 
                    nichts als ein Wink aus Ihrer vielvermögenden Hand. Wohl weiß ich, was ich bitte und von 
                    wem ich bitte, aber die Verehrung giebt mir diesen Muth. Schon liegt mehrere Tage jene 
                    Empfehlung an Herrn von Zentner, ja leider schon mehrere Wochen in meiner Verwahrung, 
                    bevor ich den Muth mir geben wollte zu <hi rend="uu">dieser</hi> Bitte. Sei dem aber, wie 
                    ihm werde: ich kann immer Ihre Gerechtigkeit nur für Gnade, Ihre Gnade nur als seegnende 
                    Gerechtigkeit betrachten, sie sei nun <pb source="3v"></pb>verneinend oder bejahend diese 
                    <hi rend="u">Bitte</hi> als <hi rend="u">die Kühnste meines ganzen Lebens</hi>. Hieher leitete mich der Gedanke 
                    <persname ref="1627">Homers</persname>:</p>
                <lg><q>Kühn und zage du nicht! Dem muthigen Manne gelinget<lb></lb>Jegliches 
                    Werk am besten – und ob er auch anderswo herkommt!<bibl n="V. 51f." sameas="14749" type="literature"></bibl></q></lg>
            </div>
            <div>
                <p>In ewig unveränderlich gleicher Hochachtung, jeder Zukunft gewärtig, 
                    habe ich die Ehre, mit dem heißesten Seegenswunsche für Ihre Gesundheit,<lb></lb>erfurchtsvoll 
                    zu verharren<lb></lb>Euer Hochwohlgeboren<lb></lb>unterthänigster Diener</p>
                <p>Joh. Georg Christian Kapp, D.</p>
                <p><placename ref="159">Baireuth</placename> den <date when="1822-11-10">
                    10ten Novem˖<expan>ber</expan> 1822</date>.</p>
                </div>
            </body>
	</text>
</TEI>