<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 5891</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/5891">https://schelling.badw.de/text/5891</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/5891">https://schelling.badw.de/doc/5891</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <pb source="1r"></pb><p><placename ref="21">München</placename> den <date when="1813-10-08">
                    8. Oct˖<expan>ober</expan> 1813</date>.</p>
            </div>
            <div>
                <p>Es ist mir unmöglich, Herrn Hofrath <persname ref="213">von Breyer</persname> reisen zu lassen, ohne 
                    sein Anerbieten zu benutzen und mich in Ihr gütiges Andenken, verehrtester 
                    Freund, wieder einmal zurückzurufen. Welchen Werth ich darauf setze, einen Platz in Ihrer 
                    Erinnerung zu haben, wissen Sie längst; wie erfreulich mir darum das <bibl sameas="8180" type="document">Schreiben</bibl> war, das Sie mir durch meine gute <persname ref="88">Mutter</persname> schickten, brauche ich nicht 
                    zu sagen. Ich habe <date from="1813-06" to="1813-08">diesen Sommer</date> über manches Glück genossen; keinen kleinen Theil daran hatte die 
                    Anwesenheit meiner Mutter, die wie ein guter Geist manche Sorgen von mir und meiner 
                    <persname ref="223">Frau</persname> hinweggenommen, in deren Gegenwart ich freylich den Verlust meines 
                    unvergeßlichen <persname ref="191">Vaters</persname> erst ganz fühlen, aber doch auch wieder mehr 
                    verschmerzen gelernt als zuvor. Unser Zusammenseyn war bis jetzt durch nichts gestört; zum Verwundern 
                    hat sich die alte Frau an das hiesige Clima gewöhnt und an Kräften merklich zugenommen. Dieß hat 
                    mir auch den Muth gegeben, sie um einen längeren Aufenthalt anzusprechen, eine Bitte, die sie mit der 
                    größten Bereitwilligkeit zugestanden. Ich erkenne in dieser Fügung eine für mich gütig sorgende 
                    Vorsehung, welche mir diese treue Mutter zur Hülfe und zum Trost in einer Zeit gönnt, wo ich jene 
                    gewiß, diesen <pb source="1v"></pb>vielleicht doch öfters, nöthig haben werde, indem die erste 
                    <date when="1813-12-17">Entbindung</date> meiner Frau im kommenden Winter bevorsteht. – Dieß von 
                    mir und dem was mich angeht, weil ich Ihre gütige Theilnahme auch an meinem Ergehen kenne.</p>
                <p>Alle Geister und Herzen sind jetzt voll von der großen wunderähnlichen <hi lang="la" rend="aq">Conversio 
                    rerum</hi>, die sich in den letzten Monaten ereignet. Es ist ein Gefühl an das man sich noch gar 
                    nicht recht gewöhnen kann. Seit dem Unglück <placename ref="889">Deutschlands</placename> habe ich erst die 
                    Propheten recht verstehen lernen; jetzt lerne ich fühlen, was es heißt, aus der Gefangenschaft und 
                    mehr als <placename ref="963">babylonischen</placename> Knechtschaft erlöst zu werden. Die eingetretene 
                    Zerstörung der <placename ref="627">feindlichen Macht</placename>, die Auflösung, deren vollständige Resultate wir noch nicht einmal 
                    kennen, scheint in gar keinem Verhältniß mit den Niederlagen; diese Zerstörung kommt von innen durch 
                    einen eigentlichen Verwesungs- und Putrefactionsproceß. <persname ref="448">Moll</persname>’s Zeitrechnung 
                    wird jetzt wohl einige Modification erleiden müssen, ob ich gleich immer glaube, daß <persname ref="1201"><hi rend="u">sein</hi></persname> 
                    Ende noch nicht so nah ist; verstehe ich etwas von dem wunderbaren Gang der Entwicklung, so wird 
                    <hi rend="u">er</hi> noch aufgespart; wenn alle seine Helfershelfer abgegangen sind, wird er noch leben, um 
                    den Kelch der Demüthigung bis auf die Hefen auszuleeren. – Das Benehmen im gegenwärtigen Krieg 
                    scheint auf eine noch tiefere Depravation zu deuten; ich glaube seine ganze Energie hat nicht wie 
                    man ihm zutraute, in einem blinden Fatalism, der doch immer noch etwas in gewiss<pb source="2r">
                    </pb>er Art Erhabenes und Vernunftartiges hat, sondern in bloßem Casualismus, einer Vergötterung 
                    des Zufalls bestanden; seine Überzeugung scheint mir jetzt die gewesen zu seyn, daß selbst nicht 
                    Verstand, und Kunst, noch weniger freylich Moralität und ein höherer Wille über das Gelingen der 
                    Unternehmungen entscheiden, sondern reiner Zufall, der das Tollste gelingen macht, wenn er günstig 
                    ist. – Ein guter Geist scheint bis jetzt auch über den politischen Verhandlungen zu walten; möge 
                    er bleiben! Deutschland hat es hoch nöthig; ja ein Gesetzgeber der vom Himmel käme, wäre zu wünschen, 
                    um den Deutschen (da das Alte einmal nicht wohl wiederkommen kann) die Verfassung zu geben, die zu 
                    ihrem daurenden Glücke nothwendig ist. – Wie es bey uns steht, wird Ihnen Herr H<expan>of</expan>R˖<expan>ath</expan> <persname ref="213">von Br˖<expan>eyer</expan></persname>
                    mündlich sagen können.</p>
                <p>Die reinsten und innigsten Wünsche für mein besondres, hochgeliebtes <placename ref="1009">Vaterland</placename> 
                    erwachen bey dieser Veranlassung in meiner Brust. Doch gewiß, es wird gerettet werden, mit all’ den 
                    Keimen des Guten, die selbst in der bösen Zeit noch erhalten, und wenigstens von einzelnen treuen 
                    Herzen gepflegt worden sind. Die Trefflichkeit wohlhergebrachter Anstalten, die Tüchtigkeit und der 
                    herrliche Sinn einzelner Patrioten, worunter ich Sie mit ganz besondrer Empfindung nenne, ist Ursache, 
                    daß wenigstens das Volk, die Nation, viel wenigere Schritte zurückzunehmen hat, um sich wieder ganz 
                    im Rechten und Guten zu befinden, als andere Völker.</p>
                <pb source="2v"></pb><p>Wegen der theol˖<expan>ogischen</expan> Lehrstelle in <placename ref="16">Tübingen</placename> scheint es 
                    noch immer anzustehen; ich schließe daraus, daß der gute <persname ref="728">Köstlin</persname> keine 
                    entschiednen Aussichten hat. Ich glaube zwar, wie Sie bemerken, daß er während seiner Hofmeisterstelle 
                    die Theologie ziemlich <unclear cert="high">an den</unclear> Nagel gehängt haben mag, indeß er ist ein empfänglicher 
                    Kopf, der leicht auf’s Rechte zu lenken sein würde. Aber warum denkt man denn auch jetzt nicht an den 
                    trefflichen Helfer <persname ref="145">Renz</persname> in <placename ref="227">
                    Laufen</placename>? Mag er auf dem Lande auch von den Studien abgekommen seyn; <hi rend="u">er</hi> ist der Mann, sich bald 
                    wieder hineinzufinden, alles nachzuhohlen, und die ziemlich veraltete, den Besseren schon lang 
                    widerwärtig gewordene Lehrart mit ganz neuem Geiste zu beleben. Fast muß ich glauben, eine herrschende 
                    Partei fürchtet sich vor ihm; ich weiß wohl, daß er nie in ihren Wegen gewandelt, nie mit ihrer 
                    unnatürlichen Theologie zufrieden gewesen; aber wie seine Theologie beschaffen, kann man, dünkt mich, 
                    aus der Wirkung seiner Predigten schließen, dergleichen meines Wissens nie weder die Gelehrsamkeit 
                    des sonst so achtungswerthen <persname ref="249">Storr</persname>, noch die <persname ref="550">Süßkindische</persname> 
                    Mikroskopie hervorgebracht, oder hervorzubringen gelehrt hat. Daß er auch jetzt noch, wie ehmals in 
                    Tübingen, an Talenten und gründlichem Geist weit über alle hervorragt, die etwa mit ihm rivalisiren 
                    können, glaube ich ebenso gut annehmen zu dürfen, als daß sein vortrefflicher Charakter unverändert 
                    derselbe geblieben ist. Wenn auf einem Ausländer bestanden werden sollte, so gebe nur der Himmel, 
                    daß es kein <persname ref="163">Paulus</persname> oder ähnlicher sey, der, wie <date when="1811">vor 2 Jahren</date> ein gewisser 
                    <persname ref="4081"><hi rend="u">Stephanie</hi></persname> (ehmaliger Zuhörer von Paulus, und <placename ref="1061">baierscher</placename> <hi rend="u">Kirchen</hi>-Rath in <placename ref="366">Augsburg</placename>) in einer <pb source="3r"></pb>dem <hi rend="u">katholischen</hi> Clerus 
                    seines Sprengels zugeeigneten <bibl sameas="16767" type="literature">Brochure</bibl> gethan hat, die Einsetzung des Nachtmahls durch den Blutbecher 
                    zu erläutern fähig ist, der nach <persname ref="4082">Sallust</persname> bei der <bibl sameas="16768" type="literature">
                    Catilinarischen Verschwörung</bibl> umhergegangen. <hi rend="aq"><foreign xml:lang="la">Horresco referens</foreign></hi>! 
                    Unter den Theologen, die ich im Ausland kennen gelernt, ist unser hiesiger Kirchenrath und Akademikus 
                    Dr. <persname ref="222">Martini</persname> noch einer der besten; nicht daß er in 
                    die wahre Tiefe gedrungen, aber wenigstens verdreht und verfälscht er die Schrift 
                    nicht durch Paulusische Auslegungen, die er verabscheut, und hat an profunde und ausgebreitete 
                    theologische, historische und allgemein literarische Gelehrsamkeit in <placename ref="1009">Würtemberg</placename> 
                    jetzt schwerlich seines Gleichen. Sollten Sie einmal einen Fremden haben, so wollte 
                    ich Würtemberg diesen gönnen.</p>
                <p>Was meine literarischen Arbeiten betrifft, (denn ich weiß, daß Sie daran einigen Theil nehmen), so 
                    warten die <bibl sameas="8196" type="document">Weltalter</bibl><bibl sameas="8404" type="document"></bibl> auch auf die bessere Zeit. In diesem Jahr voll Krieg, 
                    Sturm und Unruhe, wollte ich sie nicht dem offnen Meer preisgeben; im Jahr <date when="1814">1814</date> wird man empfänglicher für diese Ideen sein. Dann werden sie aber auch gewiß nicht länger 
                    zurückgehalten.</p>
                <p>Ich kann nicht schließen, ohne Sie zu bitten, daß Sie mich Herrn Superint˖<expan>endenten</expan> <persname ref="306">Riegern</persname> auf’s Angelegentlichste empfehlen und mich entschuldigen, daß ich durch diese Gelegenheit nicht 
                    gleichfalls <bibl sameas="4889" type="document">danke</bibl> für die mir sehr erfreulich gewesene Lektüre seiner Christologischen 
                    <bibl sameas="16636" type="literature">Abhandlung</bibl>; 
                    die Zeit will es jetzt nicht erlauben, aber ich werde meine 
                    Schuldigkeit nachholen. Ich nehme den innigsten Antheil an der Freude die ihm durch die, so viel ich 
                    urtheilen kann gewiß glückliche und <hi rend="aq"><foreign xml:lang="la">non sine numine</foreign></hi> geschlossene 
                    Verbindung seiner J<expan>un</expan>gf<expan>e</expan>r˖ <persname ref="4436">Tochter</persname> zutheil werden wird; auch meine 
                    schwachen Wünsche begleiten sie in dieß neue Verhältniß, in dem ihr die Vorsehung all’ das Glück 
                    gewähren wird, dessen sie durch Geist und Herz so würdig ist.</p>
                <p>Vielleicht daß es mir im kommenden <date from="1814-03" to="1814-05">Frühling</date> oder <date from="1814-06" to="1814-08">Sommer</date> 
                    so gut wird, meine gute <persname ref="88">Mutter</persname> hinaus zu begleiten. Dann freue ich mich insbesondre auch mit Ihnen wieder reden zu können 
                    von Angesicht zu Angesicht. Inzwischen erhalte Sie Gott der guten und gerechten Sache, der treu geblieben 
                    zu seyn in allen Umständen jetzt doppelte Freude bringt, da die Phantome der falschen Weisheit, wie 
                    die Ausgeburten der Hölle, zerstieben. <hi rend="aq"><foreign xml:lang="la"><hi rend="u">Fuit</hi></foreign></hi>!</p>
            </div>
            <div>
                <p>Ich bin und bleibe mit der treuesten Verehrung unverändert<lb></lb>
                    Ihr<lb></lb>
                    G<expan>an</expan>z ergebner</p>
                <p>Schelling.</p>
            </div>
		</body>
	</text>
</TEI>