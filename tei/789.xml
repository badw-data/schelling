<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 789</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/789">https://schelling.badw.de/text/789</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/789">https://schelling.badw.de/doc/789</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <p>Seiner Hochgebohren</p>
                <p>dem Herrn geheimen JustizRath</p>
                <p>und Ritter <hi rend="u">Schelling</hi></p>
                <p>in <placename ref="128">Erlangen</placename> (<placename ref="1061">Bayern</placename>)</p>
                <p><hi rend="u">franco.</hi></p>
            </div>
            <div>
                <pb source="1r"></pb><p><placename ref="22">Dresden</placename>, den <date when="1826-11-11">11 November. 1826</date>.</p>
            </div>
            <div>
                <p>Erlauben Sie mir, hochzuverehrender und herzlich geschäzter Herr geheimer JustizRath, frey von allen 
                    persönlichen Regeln und Umschweifungen des Briefstils, Ihnen aus der Ferne einen Mann in Erinnerung 
                    zu bringen, der es als ein wahres Glück schäzt Ihre Bekanntschaft gemacht zu haben und obgleich nicht 
                    vermögend Ihren Geist gehörig zu schätzen, doch durch eine gewiße Herzensahndung zu Ihnen eine Neigung
                    fühlt, die Ihm auch von Ihm selbst seitdem eine beßere Meinung beygebracht hat. Ich war schon längst 
                    gesonnen Sie, verehrungswürdiger Mann, für Ihre herzliche Aufnahme in <placename ref="187">Karlsbad</placename> zu 
                    danken, aber mein Gemüthszustand und die Unruhe über die Krankheit meines <persname ref="3776">Bruders</persname>,
                    der jezt, Gott sey Dank! sich viel beßer befindet, entzogen mich von Allem wozu ich mich sonst hingerißen 
                    fühlte. Jezt, überzeugt juristisch (obgleich ohne Nutzen für Ihn), so wie ich es vorher moralisch war, 
                    von der gänzlichen Unschuld deßen, der unser Unglück, aber zugleich meine jetzige hohe Seelenstimmung 
                    verursachte, und etwas beruhigt über die Gemüthskrankheit meines hiesigen Bruders, fange ich an selbst
                    aufzuleben und denke meine hiesige Muße einer wißenschaftlichen Thätigkeit zu widmen, darüber meine 
                    Zeit und Alles was mich betroffen so viel als möglich, vergeßen und mich selbst moralisch und
                    wißenschaftlich, den jetzigen Zeiten etwas entsprechend, auszubilden, so viel wenigstens als es in meinen 
                    Kräften seyn wird, die, wenn nicht ganz erschlafft, doch stark gelähmt sind. – Obgleich ich fern 
                    von allen ehrgeitzigen Entwürfen bin und keinen andern Regungen in mir zu entstehen erlauben möchte, 
                    als denen des allgemeinen Wohlwollens und einer wahren, herzlichen, aber zugleich aufgeklährten 
                    Vaterlandsliebe, so wäre es doch Undank und Egoismus von meiner Seite, wenn ich nicht zugleich der 
                    Hofnung Raum ließe meinen jetzigen Aufenthalt in dem geistigthätigen <placename ref="889">Deutschland</placename>
                    auch für meine in der Zukunft möglichen Verhältniße in <placename ref="1102">Rußland</placename> zu benutzen. 
                    Auch jezt, indem Ich an Sie schreibe und im Sinne habe Sie um Aufmunterung zu meinen Studien und um 
                    Aufklährungen zu ersuchen, denke ich nur an mein Vaterland <pb source="1v"></pb>und an mich selbst, blos
                    in Rücksicht auf Ihn, und wahrlich, ungeachtet alles deßen, was mit Uns geschehen, Ich</p>
                <lg><q>erköhre mir kein andres Land<lb></lb>zum Vaterland,<lb></lb>Wäre mir auch frey die große 
                    Wahl!<bibl n="S. 274" sameas="8531" type="literature"></bibl></q></lg>
                <p>Ich wünschte nicht umsonst jezt in Deutschland leben und mich wieder, wie vor <date when="1801">25 
                    Jahren</date> in <placename ref="10">Göttingen</placename>, für eine administrative Wirksamkeit, ganz im Stillen 
                    vorzubereiten. Mein Lieblingsfach, außer der Welt- und besonders Religionsgeschichte, bleibt immer 
                    Schulwesen und Verbreitung der hohen akademischen Studien. Meine Ansichten darüber, was Rußland 
                    anbetrifft, sind von der Art, daß sie nicht einem solchen Plane zuwider wären, der von dem Allgemeinen 
                    und Reinwißenschaftlichen ausgehend, in alle Zweigen des menschlichen Wißens hinunterläuft. Freylich 
                    möchte ich gern meinen Studien eine mehr praktische Richtung geben, aber von dem Allgemeinen müßen 
                    sie doch ausgehen. Meine systematische Lectüre möchte ich gern von einem strengphilosophischen, aber 
                    auch geistvollen Werke anfangen, und daran das Studium der Geschichte anknüpfen, welches in alle 
                    Fächer der Administration, so wie auch der Legislation eingreift. Ich habe mich etwas hier in den 
                    verschiedenen Litteraturen umgesehen, aber noch zu keiner systematischen und strenggeordneten Lectüre 
                    mich entschließen können. Ich erwarte das Erscheinen Ihres neuen vollständigen Werks mit einer Ungeduld,
                    als ob ich wirklich fähig wäre es zu faßen. Ich werde es doch versuchen! Indeßen würden Sie, hochverehrter 
                    Mann, mir wirklich eine wahre Wohlthat erweisen, wenn Sie mir Etwas, was Sie selbst für das Beßte im 
                    Fache der Geschichte und der Philosophie halten, anzeigen könnten, so wohl in Deutscher, als in 
                    <placename ref="362">Englischer</placename> oder <placename ref="627">Französischer</placename> Litteratur. Apropos: Haben 
                    Sie schon <persname ref="1147">Cousin</persname>'s »<bibl sameas="16683" type="literature">fragmens philosophiques</bibl>« gelesen. 
                    Darin ist Neues nur sein <foreign xml:lang="fr">Compte-rendu</foreign> über die neuesten philosophischen Bestrebungen 
                    in Frankreich, aber so eine Sprache führen, dürften wohl, aber würden selbst <persname ref="157">Kant</persname> 
                    oder Schelling nicht. Übrigens ist jezt in Frankreich, durch die Annäherung an die Deutsche Litteratur, 
                    ein Geist der Forschung aufgeweckt worden, welchen <pb source="2r"></pb>Sie beßer als ich zu würdigen werden. 
                    Auch in Cousin sind in einigen Stellen Ausstrahlungen, die Ihres Geistes nicht ganz unwürdig sind. 
                    <persname ref="316">Guizot</persname> hat eine merkwürdige <bibl n="S. 1-40" sameas="16704" type="literature">Einleitung</bibl> zu einer 
                    &quot;Encyclopédie progressive&quot; geschreiben, nur, obgleich selbst Schüler und Verehrer der Deutschen, spricht er 
                    <unclear cert="low">###</unclear> als ein wahrer Franzose von den Fortschritten der Philosophie im 19ten Jahrhundert und 
                    sieht Licht nur unter seinen Fenstern. – Ich habe eine Auseinandersetzung eines neuen Werks von Herrn 
                    <persname ref="487">Thiersch</persname> gelesen: <bibl n="Bd. 1" sameas="7118" type="literature">über gelehrte Schulen in Bayern</bibl>. 
                    Es ist wohl dasselbe, für das ich von Ihnen einen Recommendationsbrief erhalten, aber noch nicht abgegeben 
                    habe. Das Werk scheint vortreflich, und obgleich für Bayern geschrieben, doch auch für Andre belehrend zu 
                    seyn. <placename ref="21">Munich</placename> scheint wirklich aufblühen zu wollen und es macht dem neuen 
                    <persname ref="444">König</persname>, an den ich nie denke, ohne zugleich mich an seinen Aufenthalt in Göttingen 
                    zu erinnern, <unclear cert="high">wirklich</unclear> Ehre, daß Er in Seiner Residenz die Wissenschaften mit der Kunst 
                    vereinigen will. Künftiges Jahr wird dort auch die Zusammenkunft der Deutschen Naturforscher statt haben. 
                    Vor einigen Wochen habe ich hier diesen Versammlungen mit wahrem Interesse beygewohnt. Es ist ein Leben, 
                    was sich in Deutschland regt, das die Retrogradation unmöglich macht und zugleich den 
                    <bibl n="S. 5" sameas="16705" type="literature">Brodtgelehrten</bibl>, wie <persname ref="329">Schiller</persname> sie nannte, den 
                    Umsturz Ihres friedlichen Compendienhüten drohet, auf deren Eingang das: <hi lang="la" rend="aq">noli
                    me tangere</hi>, nicht lange stehen bleiben wird. Der <unclear cert="low">allm##tige</unclear> <unclear cert="low">###</unclear> 
                    der wissenschaftlichen Forschung wird sie umstürzen und neue Paläste werden emporsteigen, die wieder andern 
                    den Platz räumen und so bis an's Ende der Weltgeschichte, wo alle Keime des menschlichen Erkenntnißes sich 
                    entwickeln und den Plan der Vorsehung rechtfertigen werden. – Jetzt erlauben Sie mir eine Hauptfrage: Ist 
                    es wahr, daß Sie als Professor an der neuen <placename ref="1071">Universität</placename> in Mun˖<expan>ich</expan> angestellt sind; 
                    daß Sie aber nicht eher als in <date when="1829-03"><unclear cert="high">28</unclear> Monaten</date> Ihr Lehramt daselbst 
                    antreten werden? Es wäre vieleicht nicht unmöglich und gewiß ein Glück für mich, wenn ich zu dieser Zeit 
                    noch mit der meinigen disponiren könnte. Mir liegt sehr daran es vorläufig zu wißen, so wie auch was für 
                    Vorlesungen Sie halten werden? – Ich werde hier in Dresden, bis zum <date when="1827-05-01">Anfang May</date> 
                    (oder Ende April) bleiben und dann warscheinlich eine Reise mit meinem Bruder, und mit meinem Jugendfreunde, 
                    dem berühmten <pb source="2v"></pb>Dichter und jezt Erzieher unseres <persname ref="3775">Grossfürsten</persname> und 
                    ThronErben, <persname ref="3774">Zoukoffsky</persname>, nach <placename ref="25">Paris</placename> machen, wo ich nur 3 bis 
                    4 Wochen, wegen <unclear cert="low">###</unclear> bleiben werde und dann mit meinem Bruder in die <placename ref="622">Schweitz</placename>
                    reisen und dort wieder meine Studien fortsetzen. Wenn Sie mich mit einigen Zeilen beehren wollen, so ist meine 
                    Adresse: à Mr A.T. im <unclear cert="low">### ###</unclear>. H9. oder <unclear cert="low">### ###</unclear>. Ich ersuche Sie Ihrer Frau 
                    <persname ref="223">Gemahlin</persname> meine wahre Verehrung zu bezeugen und zugleich den Wunsch, daß Ihre Gesundheit 
                    sich <unclear cert="low">###</unclear> beßere und daß sie auch über den Ihrige keine Ursache habe sich zu beunruhigen.</p>                    
            </div>
            <div>
                <p>Empfangen Sie, hochgeschäzter Herr Geheimer Rath, die Versicherung der aufrichtigsten Verehrung,
                    mit der ich verbleibe<lb></lb>Ihr ganz gehorsamster Diener</p>
                <p>Alexander Turgeneff</p>
            </div>
      </body>
	</text>
</TEI>