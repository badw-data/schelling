<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription 1023</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/1023">https://schelling.badw.de/text/1023</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/1023">https://schelling.badw.de/doc/1023</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		<body>
            <div>
                <p>Sr. Wohlgebohrn</p>
                <p>Herrn Professor <hi rend="u">Welcker</hi></p>
                <p>in</p>
                <p><hi rend="u"><placename ref="10">Göttingen</placename></hi></p>
                <p>frey nur bis <placename ref="452">Eisenach</placename> vergütet</p>
            </div>
            <div>
                <pb source="1r"></pb><p><placename ref="21">München</placename> den 
                    <date when="1819-03-08">8. März 1819</date>.</p>
            </div>
            <div>
                <p>Es war gleich nach Erscheinung der Recension des <persname ref="169">
                    Wagner</persname>’schen <bibl sameas="2314" type="literature">Berichts</bibl><bibl n="Bd. I 19. S. 315–404" sameas="6347" type="literature"></bibl> in den <bibl n="St. 115 v. 18.07.1818. S. 1137–1152. St. 116 v. 20.07.1818. S. 1153–1156" sameas="123" type="literature">Gött˖<expan>ingischen</expan> gel˖<expan>ehrten</expan> Anz˖<expan>eigen</expan></bibl> meine Absicht 
                    gewesen, Ew. Wohlgebohrn für die billige Beurtheilung zu danken, welche 
                    Sie meinen Zusätzen zu demselben zu theil werden ließen. Aber meine Zeit 
                    wird von so verschiednen Seiten in Anspruch genommen und das vielfach 
                    bewegte Leben der <placename ref="21">hiesigen Stadt</placename> wirkt so 
                    zerstreuend ein, daß auch die einfachsten Vorsätze oft nicht zur 
                    Ausführung kommen. Inzwischen kommt Ihre Gefälligkeit meinem Danke mit 
                    Übersendung eines eignen Exemplars jener Rec˖<expan>ension</expan> und eines neuen 
                    Heftes Ihrer <bibl sameas="97" type="literature">Zeitschrift</bibl> zuvor. Bemerken will ich 
                    im Vorbeygeh’n, daß das Paket, welches Ihrem <bibl sameas="8513" type="document">Schreiben
                    </bibl> zufolge am <date when="1818-12-03">3. Dec˖<expan>ember</expan> v˖<expan>origen</expan> 
                    J˖<expan>ahres</expan></date> von <placename ref="10">Göttingen</placename> abging, erst vor 
                    wenig Tagen in meine Hände gekommen ist.</p>
                <p>Es mußte mich natürlich nicht wenig freuen, in Ansehung des Alters der 
                    <placename ref="1585">aeginäischen</placename> Statuen meine Meynung gegen den 
                    schnellen <bibl n="S. 184–186" sameas="392" type="literature">Schluß</bibl> auf <persname ref="4881">Onatas</persname>, den sich Herr 
                    <persname ref="1409">Hirt</persname> erlaubt, und den allerdings schwachen, 
                    von den Reichthümern nach dem <placename ref="899">Persischen</placename> Krieg 
                    hergenommenen Grund von einem Mann Ihrer Art in Schutz genommen zu sehen.
                    Wenn wir von der Kleinheit der Bilder auf die Dimensionen des Tempels
                    schließen, so scheint die Verschwendung an denselben die Kräfte nicht 
                    überstiegen zu haben, welche wir den Aegineten eine gute Zeit vorher 
                    zutrauen dürfen; immer muß dieß Tempelchen gegen den <placename ref="1948">
                    Parthenon</placename> eine sehr bescheidne Figur gemacht haben. Über den 
                    besondern Sinn, welcher von <persname ref="3183">Pausanias</persname> und 
                    nach ihm <hi rend="u">von den <placename ref="924">Griechen</placename></hi> mit <hi rend="u">
                    äginäischen Werken</hi> verbunden worden, muß ich eine weitere Untersuchung, 
                    besonders in Bezug auf die Stelle des <bibl sameas="14685" type="literature">Hesychius</bibl><persname ref="3484"></persname> vorbe<pb source="1v"></pb>halten, wenn man anders hierüber <hi rend="aq"><foreign xml:lang="la">ad majorem certitudinis gradum</foreign></hi> (im <persname ref="1057">
                    Böckh</persname>’schen Latein zu reden) gelangen kann. Vor jetzt halten mich die 
                    Stellen über Onatas ab, jenen Ausdruck auf die allerältesten Werke zu 
                    beschränken. So wahrscheinlich indeß, als <hi rend="u">Hirt’s</hi> Erklärung, ließe 
                    sich, meines Erachtens, sogar dieses machen, daß jenes Unterscheidende
                    sich bloß auf die <hi rend="u">Dimensionen</hi> bezogen, da ich Spuren nachweisen 
                    wollte, daß die aegin˖<expan>äische</expan> Kunst sich bis auf die späteren Zeiten 
                    unter Naturgröße zu halten gewohnt gewesen. Nicht eher, als die 
                    besprochenen Werke <placename ref="21">hieher</placename> kommen, werde ich zu diesen Untersuchungen 
                    zurückkehren; <hi rend="u">ganz</hi> aufgeben kann ich die (vielleicht zu <hi rend="u">weit</hi> getriebne) Meynung noch nicht, weil ich von manchen andern Seiten, 
                    selbst durch Urtheile von Künstlern, darinn bestärkt werde. Doch die 
                    <hi rend="u">Anschauung</hi> ist die Hauptsache, wie mich neuerdings die Abgüsse 
                    von den Figuren des Parthenon belehrt; was ich immer glaubte, mir davon 
                    vorstellen zu können, scheint es mir, allgemein behaupten zu können, es 
                    sey unmöglich, sich von dieser Verbindung des Großartigen mit der 
                    reinsten Naturerfahrung ohne eignen Anblick einen Begriff zu machen. – 
                    Auch Ihr Urtheil über <hi rend="u">Hirt’s Deutung</hi> der Figuren wird von <placename ref="99">Rom</placename> aus 
                    mehr und mehr bestätigt, <persname ref="169">Wagner</persname>, auf den ich hierinn großes Vertrauen 
                    setze, spricht von derselben als einer völlig unmöglichen.</p>
                <p>Wie wir <placename ref="21">hier</placename> durch Anträge, die unsrem Prof. <persname ref="487">Thiersch</persname> für <placename ref="10">Göttingen</placename> gesch<expan>eh</expan>en, in Erfahrung bringen, werden Ew. 
                    Hochwohlgeb<expan>ohrn</expan> nach <placename ref="146">Bonn</placename> gehen. Ich wünsche 
                    dieser neuen <placename ref="1783">Universität</placename> Glück zu solchen 
                    Erwerbungen, durch die sie allein die bis jetzt erregten Erwartungen und 
                    Hoffnungen erfüllen kann.</p>
            </div>
            <div>
                <pb source="2r"></pb><p>Empfangen Sie nochmals meinen herzlichen Dank für die 
                    Beweise wahrhaft freundschaftlicher Gesinnung, die Sie mir fortwährend 
                    geben. Ich bitte sie, ganz von dem Werth überzeugt zu seyn, den ich auf 
                    dieselbe setze, und auch in den neuen Verhältnissen mich in wohlwollendem
                    Andenken zu behalten.<lb></lb>
                    Mit wahrester Hochachtung<lb></lb>
                    Ew. Wohlgebohrn<lb></lb>
                    Ganz ergebenster</p>
                <p>Schelling.</p>
            </div>
		</body>
	</text>
</TEI>